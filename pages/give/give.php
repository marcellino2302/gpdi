<div class="content">
                        <!--  section --> 
                        <section class="parallax-section header-section" data-scrollax-parent="true" id="sec1">
                            <div class="bg"  data-bg="assets/images/bg/gpdi.png" data-scrollax="properties: { translateY: '200px' }"></div>
                           
                            <div class="container big-container">
                                <div class="section-title">
                                    <h3>GPdI Bukit Hermon</h3>
                                    <div class="separator trsp-separator"></div>
                                    <h2>GIVE</h2>
                                    <!-- <p>Curabitur bibendum mi sed rhoncus aliquet. Nulla blandit porttitor justo, at posuere sem accumsan nec.</p> -->
                                    <a href="#sec2" class="custom-scroll-link sect-scroll-link"><i class="fa fa-long-arrow-down"></i> <span>scroll down</span></a>
                                </div>
                            </div>
                        </section>
                        <!--  section end--> 
                        <!--  section  --> 
                        <section id="sec2" data-scrollax-parent="true" >
                            <div class="container">
                                <div class="section-container fl-wrap">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="content-wrap about-wrap">
                                            <h3 class="bold-title">Give</h3>
                                                <?php
                                                    $result_head = mysqli_query($koneksi,"select * from `wb_give`
                                                    where deleted_at is null order by urutan ASC");
                                                    while($d_head = mysqli_fetch_array($result_head)){
                                                        ?>
                                                        <div class="mySlides fade">
                                                        <img src="admin/image/give/<?php echo $d_head['photos_give'] ?>" style="width:100%"><br><br><br>
                                                        </div>
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <!-- <div class="box-item">
                                                <img  src="assets/images/team/6.jpg"  class="respimg" alt="">
                                               
                                                <a href="assets/images/team/6.jpg" class="image-popup popup-image"><i class="fa fa-search"  ></i></a>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- <div class="bg dec-bg left-pos-dec"  data-bg="assets/images/bg/14.jpg"></div> -->
                        </section>
                        <!--  section end--> 
                       
                            <div class="partcile-dec"></div>
                            <div class="border-section-wrap">
                                <div class="border-section"></div>
                            </div>
                        </section>
                        
                        <!--  section end--> 
                        <div class="limit-box fl-wrap"></div>
                    </div>