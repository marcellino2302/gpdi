<style>
	.description-section{
		font-size: 3em;
	}

	.description-section > p{
		font-size: 20px !important;
	}

	.description-section > ul,
	.description-seciont > ol{
		font-size: 20px !important;
		list-style-type: circle;
		margin-left: 20px;
	}

	.gallery-item img, .grid-sizer img {
    width: 300px;
    height: 300px;
    position: relative;
    z-index: 1;
		object-fit: cover !important;
		object-position: center;
	}
		
	.list-inline{
		display: flex;
		flex-direction: row;
		column-gap: 20px;
		justify-content: center;
		align-items: center;
	}

	.social-media-icons ul {
    list-style: none;
    padding: 0;
    text-align: center;
	}

	.social-media-icons a {	
		display: inline-block;
		text-decoration: none;
		padding: 7px;
	}

</style>

<?php

$lwg_data = mysqli_query($koneksi, "SELECT * FROM `wb_lwg` ORDER BY urutan ASC");

?>

<div class="content">
			<!--  section --> 
			<?php $row = mysqli_fetch_array($lwg_data); ?>
			<section class="parallax-section header-section" data-scrollax-parent="true" id="sec1">
					<div class="bg"  data-bg="admin/image/lwg/<?= $row['data_value'] ?>" data-scrollax="properties: { translateY: '200px' }"></div>

					<div class="container big-container">
							<div class="section-title">
									<h3 style='color:#000'>&nbsp;</h3>
									<div class="separator trsp-separator" style='background:rgba(0,0,0,0.0);'></div>
									<h2 style='color:#000'>&nbsp;</h2>
									<!-- <p>Curabitur bibendum mi sed rhoncus aliquet. Nulla blandit porttitor justo, at posuere sem accumsan nec.</p> -->
									<a href="#sec2" class="custom-scroll-link sect-scroll-link"><i class="fa fa-long-arrow-down"></i> <span>scroll down</span></a>
							</div>
					</div>
			</section>
			<!--  section end--> 
			<!--  section  --> 
			<?php $row = mysqli_fetch_array($lwg_data); ?>
			<section id="sec2" data-scrollax-parent="true">
					<div class="container">
							<div class="section-container fl-wrap">
								<div class="col-md-12">
									<div class="content-wrap about-wrap">
									<h3 class="bold-title"><?= $row['data_value'] ?></h3>
								</div>
							</div>
						<div class="row">
							<div class="col-md-3">
								<div class="content-wrap about-wrap">
									<div class="box-item">
									<?php $row = mysqli_fetch_array($lwg_data); ?>
										<img  src="admin/image/lwg/<?= $row['data_value'] ?>"  class="respimg" alt="">
										<div class="overlay"></div>
										<a href="admin/image/lwg/<?= $row['data_value'] ?>" class="image-popup popup-image"><i class="fa fa-search"  ></i></a>
									</div>
								</div>
							</div>
						<?php $row = mysqli_fetch_array($lwg_data); ?>
						<div class="col-md-9">
							<div class="description-section">
								<?= $row['data_value'] ?>
							</div>
						</div>
					</div>

				<div class="row" style="text-align: left; margin-top: 30px;">
					<div class="description-section">
						<?php $row = mysqli_fetch_array($lwg_data); ?>
						<?= $row['data_value'] ?>
					</div>
				</div>

				<?php $row = mysqli_fetch_array($lwg_data); ?>
				<div class="row">
					<div class="col-md-12">
						<br><br><br>
						<div class="gallery-items border-folio-conteainer vis-por-info four-coulms">
						<?php
							$gallery_row = explode(',', $row['data_value']);
							$last_key = array_key_last($gallery_row);

							foreach ($gallery_row as $key => $line) {
									if ($key === $last_key) {
											// Skip the last element in the loop
											continue;
									}
									?>
									<div class="gallery-item">
											<div class="grid-item-holder">
													<div class="box-item">
															<img class="image-in-box" src="admin/image/lwg/<?= $line ?>" alt="">
															<div class="overlay"></div>
															<a href="admin/image/lwg/<?= $line ?>" class="image-popup popup-image"><i class="fa fa-search"></i></a>
													</div>
											</div>
									</div>
									<?php
							}
						?>
						</div>
					</div>
				</div>

				<?php $row = mysqli_fetch_array($lwg_data); ?>
				<div class="row">
					<div class="col-md-12">
						<br><br><br>
						<div class="content-wrap about-wrap">
							<iframe style='width:100%;' height=690 src="<?= $row['data_value'] ?>"></iframe></p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<ul class="list-inline col-xs-12">
							<a href="<?= $row['data_value'] ?>"><i class="fa fa-youtube-square fa-5x"></i></a>
							<?php $row = mysqli_fetch_array($lwg_data); ?>
              <a href="<?= $row['data_value'] ?>"><i class="fa fa-instagram fa-5x"></i></a>
							<?php $row = mysqli_fetch_array($lwg_data); ?>
              <a href="<?= $row['data_value'] ?>"><i class="fa fa-facebook-square fa-5x"></i></a>
							<?php $row = mysqli_fetch_array($lwg_data); ?>
              <a href="<?= $row['data_value'] ?>"><i class="fa fa-twitter-square fa-5x"></i></a>        
            </ul>
					</div>
				</div>
			</div>
		</div>
	</section>
			<!--  section end--> 
			
	<div class="partcile-dec"></div>
	<div class="border-section-wrap">
	<div class="border-section"></div>
</div>
			<!--  section end--> 
<div class="limit-box fl-wrap"></div>
</div>