<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CommunityEvent extends MX_Controller {
    
    public function __construct(){
        parent::__construct();

        $this->page->use_directory();
        $this->load->model('ModelCommunityEvent');
    }
    
    public function index(){
        $this->page->view('communityEvent_index', array(
            'grid'  => $this->ModelCommunityEvent->get_index(),
            'kebun' => $this->ModelCommunityEvent->get_komsel_name(),
            'jemaat'=> $this->ModelCommunityEvent->get_jemaat(),
        ));
    }

    public function jemaatAbsen(){
		$jemaatKehadiranStatus = $this->input->post('statusKehadiran');
		$jemaatKehadiranId = $this->input->post('idKehadiranJemaat');

		$this->ModelCommunityEvent->jemaat_kehadiran($jemaatKehadiranStatus, $jemaatKehadiranId);
	}

    public function upload_form(){
        
        $data['id_event']       = $this->input->post('id_event_up');
        $data['id_user']        = $this->input->post('id_user');
        if($_FILES['form_add']['name'] == '' ||  $_FILES['form_add']['name'] == null){
		}else{
			$data['form'] 		= $this->input->post('id_user')."-".str_replace(" ","",$_FILES['form_add']['name']);
			$foto				= $this->input->post('id_user')."-".str_replace(" ","",$_FILES['form_add']['name']);
		}

        $this->ModelCommunityEvent->upload_form($data);


        $config = array(
            'file_name'         => $foto,
            'upload_path'       => "./image/communityEvent/",
            'allowed_types'     => "gif|jpg|png|jpeg|pdf|xls|xlsx",
			'overwrite'         => TRUE,
        );

        $this->load->library('upload', $config);
		if($_FILES['form_add']['name'] == '' ||  $_FILES['form_add']['name'] == null){
		}else{
			$this->upload->do_upload('form_add');
		}

        redirect($this->agent->referrer());
    }

    public function add_event(){
        $data['komsel']         = $this->input->post('komsel_add');
        $data['event']          = strtoupper($this->input->post('event_add'));
        $data['tempat']         = strtoupper($this->input->post('tempat_add'));
        $data['leader']         = $this->input->post('leader_add');
        $data['lektor']         = $this->input->post('lektor_add');
        $data['ayat']           = $this->input->post('ayat_add');
        $data['kolekte']        = $this->input->post('kolekte_add');
        $data['deskripsi']      = $this->input->post('deskripsi_add');
        $data['waktu_mulai']    = ($this->input->post('waktu_mulai_add')) != "" ? date("Y/m/d H:i:s", strtotime($this->input->post('waktu_mulai_add'))) : "";
        $data['id_user']        = $this->input->post('id_user');

        $this->ModelCommunityEvent->add_event($data);
        redirect($this->agent->referrer());
    }

    public function edit_event(){
        $data['id_event']       = $this->input->post('id_event_edit');
        $data['komsel']         = $this->input->post('komsel_edit');
        $data['event']          = strtoupper($this->input->post('event_edit'));
        $data['tempat']         = strtoupper($this->input->post('tempat_edit'));
        $data['leader']         = $this->input->post('leader_edit');
        $data['lektor']         = $this->input->post('lektor_edit');
        $data['ayat']           = $this->input->post('ayat_edit');
        $data['kolekte']        = $this->input->post('kolekte_edit');
        $data['deskripsi']      = $this->input->post('deskripsi_edit');
        $data['waktu_mulai']    = ($this->input->post('waktu_mulai_edit')) != "" ? date("Y/m/d H:i:s", strtotime($this->input->post('waktu_mulai_edit'))) : "";
        $data['id_user']        = $this->input->post('id_user');

        $this->ModelCommunityEvent->edit_event($data);
        redirect($this->agent->referrer());
    }

    public function del_event(){
        $data['id_event']  = $this->input->post('id_event_del');
        $data['event']      = $this->input->post('nama_event_del');
        $data['id_user']    = $this->input->post('id_user');

        $this->ModelCommunityEvent->del_event($data);
        redirect($this->agent->referrer());
    }

    public function community_participant($idEvent){
        $this->page->view('communityEvent_member', array(
            'event'  => $this->ModelCommunityEvent->get_event($idEvent),
            'member' => $this->ModelCommunityEvent->get_event_member($idEvent),
            'kebun' => $this->ModelCommunityEvent->get_komsel_name(),
            'jemaat'=> $this->ModelCommunityEvent->get_jemaat(),
        ));
    }
}

?>