<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Roommanagement extends MX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modelroommanagement');
	}

    public function index(){
		//load view with get data
		$this->page->view('roommanagement_index', array (
			'grid'		=> $this->Modelroommanagement->get_index(),
		));
    }

	public function dataLoad(){
		echo json_encode($this->Modelroommanagement->data_reload_all());
	}

	public function dataReloadAll(){
		echo json_encode($this->Modelroommanagement->data_reload_all());
	}
}