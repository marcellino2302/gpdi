<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class SendEmail extends CI_Controller {

 public function __construct()
 {
   parent::__construct(); 

   require APPPATH.'libraries/phpmailer/src/Exception.php';
   require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
   require APPPATH.'libraries/phpmailer/src/SMTP.php';
 }

 public function index()
 {
   $this->load->view('email');
 }

 public function send()
 {
  $mail = new PHPMailer();
  $email_pengirim = 'admin@gpdibukithermon.org'; // Isikan dengan email pengirim
  $nama_pengirim = 'GPDI BUKIT HERMON'; // Isikan dengan nama pengirim

  //Server settings
  $mail->SMTPDebug = 2;                      //Enable verbose debug output
  $mail->isSMTP();                                            //Send using SMTP
  $mail->Host       = 'mail.gpdibukithermon.org';                     //Set the SMTP server to send through
  $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
  $mail->Username   = $email_pengirim;                     //SMTP username
  $mail->Password   = '!Leuwigajah103';                               //SMTP password
  $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;           //Enable implicit TLS encryption
  //$mail->SMTPAutoTLS = false;
  $mail->Port       = 587;                                    //TCP port to connect to; use 587 if you have set SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS
  $mail->SMTPOptions = array(
              'ssl' => array(
                  'verify_peer' => false,
                  'verify_peer_name' => false,
                  'allow_self_signed' => true
              )
          );

  //Recipients
  $mail->setFrom($email_pengirim, $nama_pengirim);
  $mail->addAddress($this->input->post('email'));    //Add a recipient

  //Content
  $mail->isHTML(true);                                  //Set email format to HTML
  $mail->Subject = 'Hallo Rendy';
  $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
  $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

  // $mail->send();
  
  //  // PHPMailer object
  //  $response = false;
  //  $mail = new PHPMailer();


  // $mail->SMTPOptions = array(
  //   'ssl' => array(
  //   'verify_peer' => false,
  //   'verify_peer_name' => false,
  //   'allow_self_signed' => true
  //   )
  // );

  // $mail = new PHPMailer;
  // $mail->isSMTP();
  // $mail->Host = 'mail.gpdibukithermon.org';

  // $mail->Username = $email_pengirim;
  // $mail->Password = '!Leuwigajah103';

  // $mail->Port = 25;
  // $mail->SMTPAuth = true;
  // $mail->SMTPSecure = 'TLS';

  // // Aktifkan untuk melakukan debugging
  // $mail->SMTPDebug = 2; 



  

  // $mail->isHTML(true);
  // ob_start();


  // // Jika ingin mengambil isi pesan dari page lain conth disini menggunakn content.php
  // // include "content.php";
  // // $content = ob_get_contents(); 
  // // ob_end_clean();



  // // Isi Email
  // $mailContent = "<p>Hallo <b>".$this->input->post('nama')."</b> berikut ini adalah komentar Anda:</p>
  // <table>
  //   <tr>
  //     <td>Nama</td>
  //     <td>:</td>
  //     <td>".$this->input->post('nama')."</td>
  //   </tr>
  //   <tr>
  //     <td>Pesan</td>
  //     <td>:</td>
  //     <td>".$this->input->post('pesan')."</td>
  //   </tr>
  // </table>
  // <p>Terimakasih <b>".$this->input->post('nama')."</b> telah memberi pesan.</p>";
  // $mail->Body = $mailContent;
  
  // // Aktifkan jika ingin menampilkan gambar dalam email
  // //$mail->AddEmbeddedImage('image/logo.png', 'logo_mynotescode', 'logo.png'); 

  // // Email subject & Alamat
  // $mail->Subject = "".$this->input->post('subject'); 
  // $mail->addAddress($this->input->post('email'));
  // $mail->isHTML(true);

   if(!$mail->send()){
     echo 'Message could not be sent.';
     echo 'Mailer Error: ' . $mail->ErrorInfo;
   }else{
     echo 'Message has been sent';
   }
 }
}