<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Calendar extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modelcalendar');
	}

    public function index(){
		//load view with get data
		$this->page->view('calendar_index', array (
			'grid'		=> $this->Modelcalendar->get_index(),
			'room'		=> $this->Modelcalendar->get_rooms(),
		));
    }

	public function check(){
		// room data : lantai_ruang|id|nama_ruang
		$room_data				= explode("|",$this->input->post('ruang_add_calendar'));

		$event_check = [$room_data[1], date("Y/m/d H:i:s", strtotime($this->input->post('waktu_mulai_add_calendar'))),date("Y/m/d H:i:s", strtotime($this->input->post('waktu_selesai_add_calendar')))];

		echo json_encode($this->Modelcalendar->event_check($event_check));
	}

	public function checkEdit(){
		// room data : lantai_ruang|id|nama_ruang
		$room_data				= explode("|",$this->input->post('ruang_add_calendar'));

		$event_check = [$room_data[1], date("Y/m/d H:i:s", strtotime($this->input->post('waktu_mulai_add_calendar'))),date("Y/m/d H:i:s", strtotime($this->input->post('waktu_selesai_add_calendar'))),$this->input->post('id_calendar_edit')];

		echo json_encode($this->Modelcalendar->event_edit($event_check));
	}

	public function save(){
		// room data : lantai_ruang|id|nama_ruang
		$room_data				= explode("|",$this->input->post('ruang_add_calendar'));

		//array data from input post
		$data['nama'] 			= $this->input->post('nama_add_calendar');
		$data['deskripsi'] 		= $this->input->post('deskripsi_add_calendar');
		$data['id_room']		= (int)$room_data[1];
		$data['waktu_mulai'] 	= date("Y/m/d H:i:s", strtotime($this->input->post('waktu_mulai_add_calendar')));
		$data['waktu_selesai'] 	= date("Y/m/d H:i:s", strtotime($this->input->post('waktu_selesai_add_calendar')));
		$data['hex_colour'] 	= substr($this->input->post('hex_colour_add_calendar'),1);
		$data['id_user'] 		= $this->input->post('id_user');
		$this->Modelcalendar->simpan($data);

		//redirect pages
		redirect($this->agent->referrer());
    }

	public function save_edit(){
		//array data from input post
		$data['id'] 			= $this->input->post('id_calendar_edit');
		$data['nama'] 			= $this->input->post('nama_edit_calendar');
		$data['deskripsi'] 		= $this->input->post('deskripsi_edit_calendar');

		// room data : lantai_ruang|id|nama_ruang
		$room_data				= explode("|",$this->input->post('ruang_edit_calendar'));
		$data['id_room']		= (int)$room_data[1];

		$data['waktu_mulai'] 	= date("Y/m/d H:i:s", strtotime($this->input->post('waktu_mulai_edit_calendar')));
		$data['waktu_selesai'] 	= date("Y/m/d H:i:s", strtotime($this->input->post('waktu_selesai_edit_calendar')));
		$data['hex_colour'] 	= substr($this->input->post('hex_colour_edit_calendar'),1);
		$data['id_user'] 		= $this->input->post('id_user');

		//update data
		$this->Modelcalendar->simpan_edit($data);

		//redirect page
		redirect($this->agent->referrer());
    }

	public function delete(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_calendar_delete');
		$data['nama'] 		= $this->input->post('nama_calendar_delete');
		$data['id_user'] 	= $this->input->post('id_user');

		//delete data
		$this->Modelcalendar->hapus($data);

		//redirect page
		redirect($this->agent->referrer());
    }
}