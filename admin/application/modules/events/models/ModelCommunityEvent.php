<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelCommunityEvent extends CI_Model {
    
    public function get_index(){
        $result = $this->db->query("SELECT x.*, y.total_kehadiran FROM (SELECT kmsl.nama_komsel, ent.`id`, ent.`event`, ent.`waktu_mulai`, ent.`deskripsi`, ent.`tempat`, ent.`leader`, ent.`kolekte`, ent.`ayat`, ent.`lektor`, ent.`form` FROM `wb_komsel_event` ent LEFT JOIN `wb_komsel` kmsl ON kmsl.id=ent.komsel WHERE ent.deleted_at IS NULL) as x JOIN (SELECT ent.id, SUM(`kehadiran`) as total_kehadiran FROM `wb_komsel_eventdetail` dtl LEFT JOIN `wb_komsel_event` ent ON ent.id=dtl.id_event GROUP BY dtl.id_event) as y ON x.id=y.id")->result();

        return $result;
    }

    public function get_jemaat(){
        $result = $this->db->query("SELECT * FROM `wb_jemaat_fix` WHERE deleted_at IS NULL")->result();

        return $result;
    }

    public function get_komsel_name(){
        $result = $this->db->query("SELECT * FROM `wb_komsel` WHERE deleted_at IS NULL")->result();

        return $result;
    }

    public function get_event($idEvent){
        $result = $this->db->query("SELECT kmsl.nama_komsel, ent.`id`, ent.`event`, ent.`waktu_mulai`, ent.`deskripsi`, ent.`tempat`, ent.`leader`, ent.`kolekte`, ent.`ayat`, ent.`lektor`, ent.`form` FROM `wb_komsel_event` ent LEFT JOIN `wb_komsel` kmsl ON kmsl.id=ent.komsel WHERE ent.`id`=$idEvent AND ent.deleted_at IS NULL")->result();

        return $result;
    }

    public function get_event_member($idEvent){
        $result = $this->db->query("SELECT ent.`id`, ent.kehadiran, jmt.nama, jmt.`status` FROM `wb_komsel_eventdetail` ent LEFT JOIN `wb_jemaat_fix` jmt ON jmt.`id`=ent.`id_jemaat` WHERE ent.`id_event`=$idEvent AND (jmt.`status`= 0 OR jmt.`status`=4 ) AND ent.deleted_at IS NULL")->result();

        return $result;
    }

    public function jemaat_kehadiran($jemaatKehadiranStatus, $jemaatKehadiranId){
		$result = $this->db->query("UPDATE `wb_komsel_eventdetail` SET `kehadiran`=$jemaatKehadiranStatus WHERE `id` = $jemaatKehadiranId;");

		return $result;
	}

    public function upload_form($data){
        $array = array(
            'form'              => $data['form'],
            'update_at' 	    => date("Y-m-d h:i:s"),
			'update_by' 	    => $data['id_user'],
        );

        $this->db->set($array);
        $this->db->where('id', $data['id_event']);
        return $this->db->update('wb_komsel_event');
    }

    public function add_event($data){
        $namaKebun = $data['komsel'];
		$result = $this->db->query("SELECT `id` FROM wb_komsel WHERE `nama_komsel`= '$namaKebun'")->result();

        $komsel = 0;
		foreach($result as $idKomsel){
			$komsel = $idKomsel->id;
		}

        $namaLeader = explode(" - ",$data['leader'])[0];
        $namaLektor = explode(" - ",$data['lektor'])[0];

        $array = array(
            'komsel'          => $komsel,
            'event'           => $data['event'],
            'tempat'          => $data['tempat'],
            'leader'          => $namaLeader,
            'lektor'          => $namaLektor,
            'ayat'            => $data['ayat'],
            'kolekte'         => $data['kolekte'],
            'deskripsi'       => $data['deskripsi'],
            'waktu_mulai'     => $data['waktu_mulai'],
            'created_at'      => date("Y-m-d h:i:s"),
            'created_by'      => $data['id_user']
        );

        $returnVal = $this->db->insert('wb_komsel_event', $array);

        $namaEvent = $data['event'];
        $waktuMulai = date($data['waktu_mulai']);

        $result = $this->db->query("SELECT `id` FROM `wb_komsel_event` WHERE `event` = '$namaEvent' AND `komsel` = $komsel AND `waktu_mulai` = '$waktuMulai' AND `deleted_by` IS NULL")->result();

        $eventId = 0;
		foreach($result as $result){
			$eventId = $result->id;
		}

        $idUser = $data['id_user'];
        $dateNow = date("Y-m-d h:i:s");

        $this->db->query("INSERT INTO `wb_komsel_eventdetail` (id_jemaat, id_event, created_at, created_by) SELECT id, $eventId, '$dateNow', $idUser FROM `wb_jemaat_fix` WHERE `wb_jemaat_fix`.komsel = $komsel;");

        return $returnVal;
    }

    public function edit_event($data){
        $namaKebun = $data['komsel'];
		$result = $this->db->query("SELECT `id` FROM wb_komsel WHERE `nama_komsel`= '$namaKebun'")->result();

        $komsel = 0;
		foreach($result as $idKomsel){
			$komsel = $idKomsel->id;
		}

        $namaLeader = explode(" - ",$data['leader'])[0];
        $namaLektor = explode(" - ",$data['lektor'])[0];

        $array = array(
            'komsel'          => $komsel,
            'event'           => $data['event'],
            'tempat'          => $data['tempat'],
            'leader'          => $namaLeader,
            'lektor'          => $namaLektor,
            'ayat'            => $data['ayat'],
            'kolekte'         => $data['kolekte'],
            'deskripsi'       => $data['deskripsi'],
            'waktu_mulai'     => $data['waktu_mulai'],
            'update_at'       => date("Y-m-d h:i:s"),
            'update_by'       => $data['id_user']
        );

        $this->db->set($array);
        $this->db->where('id',$data['id_event']);

        return $this->db->update('wb_komsel_event');
    }

    public function del_event($data){
        $array = array(
			'deleted_at' 		=> date("Y-m-d h:i:s")	,
			'deleted_by' 		=> $data['id_user'],
		);

        $this->db->set($array);
        $this->db->where('id',$data['id_event']);

        return $this->db->update('wb_komsel_event');
    }
}

?>