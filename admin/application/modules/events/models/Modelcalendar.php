<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelcalendar extends CI_Model {

	public function get_index(){
		//build query index
		$result = $this->db->query("SELECT * FROM `wb_calendars` cal LEFT JOIN `ad_rooms` rooms ON rooms.id_rooms=cal.room_id_event WHERE cal.deleted_at IS NULL ORDER BY `id_calendars` ASC")->result();
		//return query result
		return $result;
	}

	public function get_rooms(){

		$result = $this->db->query("SELECT * FROM `ad_rooms` WHERE `deleted_at` IS NULL")->result();

		return $result;
	}

	public function event_check($event_check){
		//build query index

		$result = $this->db->query("SELECT cal.*,rooms.nama_rooms, rooms.lantai FROM `wb_calendars` cal LEFT JOIN `ad_rooms` rooms ON rooms.id_rooms=cal.room_id_event WHERE `room_id_event`=".$event_check[0]." AND (((CAST('$event_check[1]' AS DATETIME) >= `waktu_mulai` AND CAST('$event_check[1]' AS DATETIME) <= `waktu_selesai`) OR ((CAST('$event_check[2]' AS DATETIME) >= `waktu_mulai`) AND CAST('$event_check[2]' AS DATETIME) <= `waktu_selesai`)) OR ((CAST('$event_check[1]' AS DATETIME) <= `waktu_mulai`) AND CAST('$event_check[2]' AS DATETIME) >= `waktu_mulai`)) AND rooms.`lantai`!='Luar Gereja' AND cal.`deleted_at` IS NULL")->result();

		//return query result
		return $result;
	}

	public function event_edit($event_check){
		//build query index

		$result = $this->db->query("SELECT cal.*,rooms.nama_rooms, rooms.lantai FROM `wb_calendars` cal LEFT JOIN `ad_rooms` rooms ON rooms.id_rooms=cal.room_id_event WHERE `room_id_event`=".$event_check[0]." AND (((CAST('$event_check[1]' AS DATETIME) >= `waktu_mulai` AND CAST('$event_check[1]' AS DATETIME) <= `waktu_selesai`) OR ((CAST('$event_check[2]' AS DATETIME) >= `waktu_mulai`) AND CAST('$event_check[2]' AS DATETIME) <= `waktu_selesai`)) OR ((CAST('$event_check[1]' AS DATETIME) <= `waktu_mulai`) AND CAST('$event_check[2]' AS DATETIME) >= `waktu_mulai`)) AND `id_calendars` != '$event_check[3]' AND rooms.`lantai`!='Luar Gereja' AND cal.`deleted_at` IS NULL")->result();

		//return query result
		return $result;
	}

	public function simpan($data){
		//array data
		$array = array(
			'nama_event' 		=> $data['nama'],
			'deskripsi'			=> $data['deskripsi'],
			'room_id_event'		=> $data['id_room'],
			'waktu_mulai' 		=> $data['waktu_mulai'],
			'waktu_selesai' 	=> $data['waktu_selesai'],
			'hex_colour'		=> $data['hex_colour'],
			'created_at' 		=> date("Y-m-d h:i:s"),
			'created_by' 		=> $data['id_user'],
		);
		//insert
		return $this->db->insert('wb_calendars', $array);
	}

	public function simpan_edit($data){
		//array data
		$array = array(
			'nama_event' 		=> $data['nama'],
			'deskripsi' 		=> $data['deskripsi'],
			'room_id_event'		=> $data['id_room'],
			'waktu_mulai' 		=> $data['waktu_mulai'],
			'waktu_selesai' 	=> $data['waktu_selesai'],
			'hex_colour'		=> $data['hex_colour'],
			'updated_at' 		=> date("Y-m-d h:i:s"),
			'updated_by' 		=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_calendars', $data['id']);
		//update	
		return $this->db->update('wb_calendars');
	}

	public function hapus($data){
		//array data
		$array = array(
			'deleted_at' 		=> date("Y-m-d h:i:s")	,
			'deleted_by' 		=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_calendars', $data['id']);
		//update
		return $this->db->update('wb_calendars');
	}
}