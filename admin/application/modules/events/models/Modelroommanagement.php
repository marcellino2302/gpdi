<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelroommanagement extends CI_Model {

	public function get_index(){
		//build query index
		$result = $this->db->query("SELECT * FROM `wb_calendars` cal LEFT JOIN `ad_rooms` rooms ON rooms.id_rooms=cal.room_id_event WHERE cal.deleted_at IS NULL ORDER BY `waktu_mulai` ASC")->result();
		// $result = $this->db->query("SELECT * FROM `wb_calendars` cal LEFT JOIN `ad_rooms` rooms ON rooms.id_rooms=cal.room_id_event WHERE CURRENT_TIMESTAMP >= `waktu_mulai` AND CURRENT_TIMESTAMP <= `waktu_selesai` AND cal.deleted_at IS NULL ORDER BY `id_calendars` ASC")->result();
		//return query result
		return $result;
	}

	public function data_reload_all(){
		$result = $this->db->query("SELECT cal.nama_event, rooms.lantai, rooms.nama_rooms, cal.waktu_mulai, cal.waktu_selesai FROM `wb_calendars` cal LEFT JOIN `ad_rooms` rooms ON rooms.id_rooms=cal.room_id_event WHERE cal.deleted_at IS NULL ORDER BY `waktu_mulai` DESC")->result();

		return $result;
	}
}