<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <div style="display: flex; align-items: center; text-align: center; justify-content: center;">
          <h1 class="m-0">Community Event</h1>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="content">
  <div class="containter-fuid">
    <div class="card card-primary">

      <div class="card-header">
        <h3 class="card-title" style="font-size: 1.8em;">Event list</h3>
      </div>

      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">

          <thead>
            <tr>
              <th></th>
              <th style="width : 5%">No</th>
              <th>Event</th>
              <th>Komsel</th>
              <th>Tempat</th>
              <th>Waktu Mulai</th>
              <th>Kehadiran</th>
              <th style="text-align: center;">Actions</th>
              <th class="none"></th>
            </tr>
          </thead>

          <tbody>
            <?php 
              $no=1;
              foreach($grid as $data){
            ?>
            <tr>
              <td></td>
              <td><?= number_format($no++, 0)?></td>
              <td><?= $data->event; ?></td>
              <td><?= $data->nama_komsel; ?></td>
              <td><?= $data->tempat; ?></td>
              <td><?= date("D, d F Y H:i:s", strtotime(str_replace("-","/",$data->waktu_mulai))); ?></td>
              <td><?= ($data->total_kehadiran== null) ? '-' : $data->total_kehadiran.' Jemaat'?></td>
              <td style="text-align: center;">

                <button class="btn btn-info btn-sm" style="margin-right: 15px;"
                  data-a="<?= $data->id?>"
                  data-b="<?= $data->nama_komsel?>"
                  data-c="<?= $data->event ?>"
                  data-d="<?= $data->deskripsi ?>"
                  data-e="<?= $data->waktu_mulai ?>"
                  data-f="<?= $data->tempat ?>"
                  data-g="<?= $data->leader ?>"
                  data-h="<?= $data->kolekte ?>"
                  data-i="<?= $data->ayat ?>"
                  data-j="<?= $data->lektor ?>"
                  data-toggle="modal" data-target="#modal-edit-communityEvent" data-backdrop="static" data-keyboard="false"
                >
                <i class="fas fa-pencil-alt"></i> Edit</button>

                <button class="btn btn-info btn-sm" style="margin-right: 15px;"
                data-c="<?= $data->id; ?>"
                data-v="<?= $data->event; ?>"
                data-x="<?= ($data->form == null) ? '' : $data->form; ?>"
                data-toggle="modal" data-target="#modal-upload-communityEvent" data-backdrop="static" data-keyboard="false"><i class="fa fa-file" aria-hidden="true"></i> Upload</button>

                <button class="btn btn-primary btn-sm" style="margin-right: 15px;"
                onclick="javascript:location.href = '<?= BASE_URL()?>events/communityEvent/community_participant/'+'<?= $data->id; ?>'"
                >
                <i class="fa fa-users" aria-hidden="true"></i> Participant</button>

                <button class="btn btn-danger btn-sm" style="margin-right: 15px;"
                  data-c="<?= $data->id; ?>"
                  data-v="<?= $data->event; ?>"
                  data-toggle="modal" data-target="#modal-delete-communityEvent"
                >
                <i class="fas fa-trash"></i> Delete</button>

              </td>
              <td class="none">
                <table class="table table-bordered table-striped">
                  <tr>
                    <th>Leader</th>
                    <th>Pembawa Firman</th>
                    <th>Thema dan Ayat FA</th>
                    <th>Kolekte</th>
                    <th>Deskripsi</th>
                  </tr>
                  <tr>
                    <td><?= ($data->leader == null) ? '-' : $data->leader ?></td>
                    <td><?= ($data->lektor == null) ? '-' : $data->lektor ?></td>
                    <td><?= ($data->ayat == null) ? '-' : $data->ayat ?></td>
                    <td><?= ($data->kolekte == null) ? '-' : $data->kolekte ?></td>
                    <td><?= ($data->deskripsi == null) ? '-' : $data->deskripsi ?></td>
                  </tr>
                </table>
              </td>
            </tr>
            <?php } ?>
          </tbody>

        </table>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="modal-add-communityEvent">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content card card-primary">

    <div class="card-header">
      <h3 class="card-title" style="font-size: 1.8em;">Add Event</h3>
    </div>

    <form action="<?= site_url('events/communityEvent/add_event');?>" method="post" enctype="multipart/form-data">
      <div class="modal-body">

      <div class="row">
        <div class="col-md-6">

          <div class="form-group">
            <label for="event_add" class="form-label">Nama Event (*)</label>
            <div>
              <input type="text" class="form-control" id="event_add" name="event_add" placeholder="Masukan Nama Event"
                value="" required>
            </div>
          </div>

          <div class="form-group">  
            <label for="komsel_add" class="form-label">KeBun (*)</label>
            <div>
              <input type="text" class="form-control" id="komsel_add" name="komsel_add" placeholder="Masukan KeBun" value="" required>
            </div>
          </div>

          <div class="form-group">
            <label for="tempat_add" class="form-label">Tempat</label>
            <div>
              <input type="text" class="form-control" id="tempat_add" name="tempat_add" placeholder="Masukan Tempat" value="" required>
            </div>
          </div>

          <div class="form-group">
            <label for="leader_add" class="form-label">Leader</label>
            <div>
              <input type="text" class="form-control" id="leader_add" name="leader_add" placeholder="Masukan Leader" value="" required>
            </div>
          </div>

          <div class="form-group">
            <label for="ayat_add" class="form-label">Thema dan Ayat FA</label>
            <div>
              <input type="text" class="form-control" id="ayat_add" name="ayat_add" placeholder="Masukan Thema dan Ayat FA" value="" required>
            </div>
          </div>

        </div>

        <div class="col-md-6">

          <div class="form-group">
            <label for="deskripsi_add">Deskripsi Event (*)</label>
            <div>
              <textarea class="form-control" id="deskripsi_add" name="deskripsi_add" placeholder="Masukan Deskripsi Event" value="" rows=5 style="height: 7.75em;" required></textarea>
            </div>
          </div>

          <div class="form-group">
            <label for="waktu_mulai_add" class="form-label">Waktu Mulai (*)</label>
            <div>
              <input type="datetime-local" class="form-control" style="background-color: inherit;" id="waktu_mulai_add" name="waktu_mulai_add" required></textarea>
            </div>
          </div>

          <div class="form-group">
            <label for="lektor_add" class="form-label">Pembawa Firman</label>
            <div>
              <input type="text" class="form-control" id="lektor_add" name="lektor_add" placeholder="Masukan Pembawa Firman" value="" required>
            </div>
          </div>

          <div class="form-group">
            <label for="kolekte_add" class="form-label">Persembahan</label>
            <div>
              <input type="text" class="form-control" id="kolekte_add" name="kolekte_add" placeholder="Masukan Jumlah Persembahan" value="" required>
            </div>
          </div>

        </div>
      </div>

        <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </form>

    </div>
  </div>
</div>

<div class="modal fade" id="modal-edit-communityEvent">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content card card-primary">

    <div class="card-header">
      <h3 class="card-title" style="font-size: 1.8em;">Edit Event</h3>
    </div>

    <form action="<?= site_url('events/communityEvent/edit_event');?>" method="post" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">

            <div class="form-group">
              <label for="event_edit" class="form-label">Nama Event (*)</label>
              <div>
                <input type="text" class="form-control" id="event_edit" name="event_edit" placeholder="Masukan Nama Event"
                  value="" required>
              </div>
            </div>

            <div class="form-group">  
              <label for="komsel_edit" class="form-label">KeBun (*)</label>
              <div>
                <input type="text" class="form-control" id="komsel_edit" name="komsel_edit" placeholder="Masukan KeBun" value="" required readonly>
              </div>
            </div>

            <div class="form-group">
              <label for="tempat_edit" class="form-label">Tempat</label>
              <div>
                <input type="text" class="form-control" id="tempat_edit" name="tempat_edit" placeholder="Masukan Tempat" value="" required>
              </div>
            </div>

            <div class="form-group">
              <label for="leader_edit" class="form-label">Leader</label>
              <div>
                <input type="text" class="form-control" id="leader_edit" name="leader_edit" placeholder="Masukan Leader" value="" required>
              </div>
            </div>

            <div class="form-group">
              <label for="ayat_edit" class="form-label">Thema dan Ayat FA</label>
              <div>
                <input type="text" class="form-control" id="ayat_edit" name="ayat_edit" placeholder="Masukan Thema dan Ayat FA" value="" required>
              </div>
            </div>

          </div>

          <div class="col-md-6">

            <div class="form-group">
              <label for="deskripsi_edit">Deskripsi Event (*)</label>
              <div>
                <textarea class="form-control" id="deskripsi_edit" name="deskripsi_edit" placeholder="Masukan Deskripsi Event" value="" rows=5 style="height: 7.75em;" required></textarea>
              </div>
            </div>

            <div class="form-group">
              <label for="waktu_mulai_edit" class="form-label">Waktu Mulai (*)</label>
              <div>
                <input type="datetime-local" class="form-control" style="background-color: inherit;" id="waktu_mulai_edit" name="waktu_mulai_edit" required></textarea>
              </div>
            </div>

            <div class="form-group">
              <label for="lektor_edit" class="form-label">Pembawa Firman</label>
              <div>
                <input type="text" class="form-control" id="lektor_edit" name="lektor_edit" placeholder="Masukan Pembawa Firman" value="" required>
              </div>
            </div>

            <div class="form-group">
              <label for="kolekte_edit" class="form-label">Persembahan</label>
              <div>
                <input type="text" class="form-control" id="kolekte_edit" name="kolekte_edit" placeholder="Masukan Jumlah Persembahan" value="" required>
              </div>
            </div>

          </div>
        </div>

        <input type="hidden" name="id_event_edit" id="id_event_edit" value="">
        <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </form>

    </div>
  </div>
</div>

<div class="modal fade" id="modal-upload-communityEvent">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content card card-info">

      <div class="card-header">
        <h3 class="card-title" style="font-size: 1.8 em;">Upload Form</h3>
      </div>

      <form action="<?= site_url('events/communityEvent/upload_form');?>" method="post" enctype="multipart/form-data">
        <div class="card-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <div class="file-area"><input type="file" accept="image/gif, image/jpeg, image/png, image/jpg" name="form_add" id="form_add" required multiple="multiple"/><div id="uktpPlaceholder" class="file-dummy flex" style="font-size: 1em;">Upload Form</div><img id="foto_up" class="file-dummy" style="border:0; display:none;"/></div>
              </div>
            </div>
          </div>
          <input type="hidden" name="id_event_up" id="id_event_up">
          <input type="hidden" name="id_user" id="id_user" value="<?= $this->session->userdata('pengguna')->id_user; ?>">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger">Upload</button>
        </div>
      </form>

    </div>
  </div>
</div>

<div class="modal fade" id="modal-delete-communityEvent">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content card card-danger">

      <div class="card-header">
        <h3 class="card-title" style="font-size: 1.8 em;">Delete Event</h3>
      </div>

      <form action="<?= site_url('events/communityEvent/del_event');?>" method="post" enctype="multipart/form-data">
        <div class="card-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <p>Apakah anda yakin ingin menghapus event ini ?<br>
                  Nama Event &nbsp; : <b name="nama_event_del" id="nama_event_del"></b></p>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" name="id_event_del" id="id_event_del">
        <input type="hidden" name="id_user" id="id_user" value="<?=$this->session->userdata('pengguna')->id_user;?>">
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger">Yes</button>
        </div>
      </form>

    </div>
  </div>
</div>

<style>
  .ui-autocomplete {
    border-radius: 0.5em;
    max-height: 200px;
    overflow-y: auto;
    overflow-x: hidden;
    padding-right: 20px;
    position: absolute;
    z-index: 99999 !important;
    padding: 0;
    margin-top: 2px;
  }

  .ui-autocomplete>li {
    padding: 3px 20px;
  }

  .ui-autocomplete>li.ui-state-focus {
    background-color: #DDD;
  }

  .ui-helper-hidden-accessible {
    display: none;
  }

  .flex { 
    display: flex; justify-content: center; align-items: center; text-align: center;
  }

  /* IMAGE UPLOAD ----------------------- */
  .file-area {
    position: relative; display: flex;
    justify-content: center;
    align-items: center;
    overflow: hidden;
    border-radius : 10px;
  }

  .file-area img {
    object-fit: cover;
  }

  input * :focus{
    outline : none;
  }

  input[type=file] {
    position: absolute;
    width: 100%;
    height: 100%;
    opacity: 0;
    cursor: pointer;
    border : 0;
  }

  .file-dummy {
    min-width : 400px;
    min-height : 533.33px;
    max-width : 400px;
    max-height : 533.33px;
    width : 400px;
    height : 533.33px;
    background: rgba(23, 162, 184,0.1);
    border: 2px dashed transparent;
    text-align: center;
    transition: background 0.3s ease-in-out;
    border-radius : 10px;
  }

  input[type=file]:hover + .file-dummy {
    background: rgba(23, 162, 184,0.2);
    border: 2px dashed rgba(23, 162, 184,1);
  }

</style>

<script>
  $(document).ready(function(){

    // ----- filter -----
    minDateFilter = "";
    maxDateFilter = "";

    // data table ---------
    var table;
    $(function () {
      table = $("#example1").DataTable({
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        "paging": true,
        "order": [[1, 'desc']],
      });
    });

    // ------ filter holder & button -----

    var button_add = $("<div style='height: 2.5em; margin-right: 1em;'></div>").addClass("dt-buttons btn-group flex-wrap");

    var add_button = $("<button></button>").text("+ Add Event");
    $(add_button).addClass("btn btn-secondary btn-sm");
    $(add_button).on('click', function(){$('#modal-add-communityEvent').modal('show');});

    var button_holder = $("<div></div>").addClass("dt-buttons btn-group flex-wrap");

    var filter_button_1 = $("<button></button>").text("All");
    $(filter_button_1).addClass("btn btn-secondary btn-sm");
    $(filter_button_1).on('click', function(){minDateFilter = ""; maxDateFilter = ""; table.draw();});

    var filter_button_3 = $("<input type='button'></input>").text("Range");
    $(filter_button_3).addClass("btn btn-secondary btn-sm daterange");
    $(filter_button_3).attr('id','timeFilter');

    var label_button_3 = $("<button class='btn btn-secondary btn-sm' for='timeFilter' style='justify-items : center !important; align-items : center !important; margin : auto !important; display : flex !important; text-align : center;'></button>").html("<label for='timeFilter' style='font-weight:inherit; padding : 0; margin : 0;'>Range : </label>");

    $(label_button_3).append(filter_button_3);

    $(document).ready(function() {

      $('#example1_wrapper .col-md-6:eq(0)').append(button_add.append(add_button));

      $('#example1_wrapper .col-md-6:eq(0)').append(button_holder.append(filter_button_1, label_button_3));

      $('#timeFilter').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

      $('.reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
          use24hours: true,
          format: 'YYYY/MM/DD HH:mm'
        }
      })

      $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex) {
          if (typeof aData._date == 'undefined') {
            aData._date = new Date(aData[5]).getTime();
          }

          if (minDateFilter && !isNaN(minDateFilter)) {
            if (aData._date < minDateFilter) {
              return false;
            }
          }

          if (maxDateFilter && !isNaN(maxDateFilter)) {
            if (aData._date > maxDateFilter) {
              return false;
            }
          }
          return true;
        }
      );

      // Refilter the table
      $('#timeFilter').on('change', function () {
        const timeFilter = $('#timeFilter').val().split(" - ");
        // Create date inputs
        minDateFilter = new Date(timeFilter[0]);
        minDateFilter = minDateFilter.getTime();

        maxDateFilter = new Date(timeFilter[1]);
        maxDateFilter = maxDateFilter.getTime();
        table.draw();
      });
    });

    // auto complete ----------
    $(function () {
      var kebunList = [<?php foreach($kebun as $komsel){
        echo '"'.($komsel->nama_komsel).'",';
      } ;?>];

    var jemaatList = [<?php foreach($jemaat as $jemaat){
        echo '"'.($jemaat->nama).' - '.($jemaat->nomor_hp).' - '.($jemaat->alamat).'",';
      } ;?>];

      $('#modal-add-communityEvent').on('shown.bs.modal', function() {
        $("#komsel_add").autocomplete({
          source: kebunList
        });

        $("#leader_add").autocomplete({
          source: function(request, response) {
            var results = $.ui.autocomplete.filter(jemaatList, request.term);

            response(results.slice(0, 10));
          }
        });

        $("#lektor_add").autocomplete({
          source: function(request, response) {
            var results = $.ui.autocomplete.filter(jemaatList, request.term);

            response(results.slice(0, 10));
          }
        });
      });

      $('#modal-edit-communityEvent').on('shown.bs.modal', function() {
        $("#komsel_edit").autocomplete({
          source: kebunList
        });

        $("#leader_edit").autocomplete({
          source: function(request, response) {
            var results = $.ui.autocomplete.filter(jemaatList, request.term);

            response(results.slice(0, 10));
          }
        });

        $("#lektor_edit").autocomplete({
          source: function(request, response) {
            var results = $.ui.autocomplete.filter(jemaatList, request.term);

            response(results.slice(0, 10));
          }
        });
      });
    });

  // edit community event ----------
  $('#modal-edit-communityEvent').on('show.bs.modal', function(event){
    var button = $(event.relatedTarget);

    var id_event        = button.data('a');
    var nama_komsel     = button.data('b');
    var event           = button.data('c');
    var deskripsi       = button.data('d');
    var waktu_mulai     = button.data('e');
    var tempat          = button.data('f');
    var leader          = button.data('g');
    var kolekte         = button.data('h');
    var ayat            = button.data('i');
    var lektor          = button.data('j');

    $('#event_edit').val(event);
    $('#deskripsi_edit').val(deskripsi);
    $('#komsel_edit').val(nama_komsel);
    $('#waktu_mulai_edit').val(waktu_mulai);
    $('#tempat_edit').val(tempat);
    $('#leader_edit').val(leader);
    $('#ayat_edit').val(ayat);
    $('#lektor_edit').val(lektor);
    $('#kolekte_edit').val(kolekte);
    $('#id_event_edit').val(id_event);
  });

  // delete community event ----------
  $('#modal-delete-communityEvent').on('show.bs.modal', function(event){
    var button = $(event.relatedTarget);

    var id_event        = button.data('c');
    var nama_event      = button.data('v');

    $('#nama_event_del').html(nama_event);
    $('#id_event_del').val(id_event);
  })

  // upload form onchange ----------
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#foto_up').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  $("#form_add").change(function(event){
    $("#uktpPlaceholder").css("display","none");
    $("#foto_up").css("display","block");

    readURL(this);
  });

  $('#modal-upload-communityEvent').on('show.bs.modal', function(event){
    var button = $(event.relatedTarget);

    var form        = button.data('x');
    var id_event    = button.data('c');
    var nama_event  = button.data('v');

    if (form != ""){
      $("#uktpPlaceholder").css("display","none");
      $("#foto_up").css("display","block");

      $('#foto_up').attr("src","<?= base_url('image/communityEvent'); ?>/"+form);
    }
    else{
      $("#uktpPlaceholder").css("display","flex");
      $("#foto_up").css("display","none");

      $('#foto_up').attr("src","");
    }

    $('#id_event_up').val(id_event);
  })

  });
  
</script>