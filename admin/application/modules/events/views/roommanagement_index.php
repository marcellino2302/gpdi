<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Room Management
        </h1>
      </div><!-- /.col -->
      <div class="col-sm-6">

      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">

          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th></th>
                  <th style ="width: 5%;">No</th>
                  <th>Event</th>
                  <th>Lantai</th>
                  <th>Ruangs</th>
                  <th>Waktu Mulai</th>
                  <th>Waktu Selesai</th>
                  <th class="none">Deskripsi</th>
                </tr>
              </thead>
              <tbody id="calendarContent">
                <?php 
                  $no = 1;
                  foreach($grid as $data){
                  ?>
                <tr <?= strtotime(str_replace("-","/",$data->waktu_selesai)) < time() ? 'class="table-danger"' : '' ?> >
                  <td></td>
                  <td><?= number_format($no++,0)?></td>
                  <td><?= $data->nama_event; ?></td>
                  <td><?= $data->lantai; ?></td>
                  <td><?= $data->nama_rooms; ?></td>
                  <td><?= date("D, d F Y H:i:s", strtotime(str_replace("-","/",$data->waktu_mulai))) ?></td>
                  <td><?= date("D, d F Y H:i:s", strtotime(str_replace("-","/",$data->waktu_selesai))); ?></td>
                  <td><?= $data->deskripsi; ?></td>
                </tr>

                <?php } ?>

              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

      </div>
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>

<script>

  // ----- filter -----
  minDateFilter = "";
  maxDateFilter = "";

  // ------ data table -----
  var table;
  $(function () {
    table = $("#example1").DataTable({
      "responsive": true,
      "lengthChange": false,
      "autoWidth": false,
      "paging": true,
      "order": [[1, 'desc']],
    })
  });

  // ------ filter holder & button -----

  var button_holder = $("<div></div>").addClass("dt-buttons btn-group flex-wrap");

  var filter_button_1 = $("<button></button>").text("All");
  $(filter_button_1).addClass("btn btn-secondary btn-sm");
  $(filter_button_1).on('click', function(){minDateFilter = ""; maxDateFilter = ""; table.draw();});

  var filter_button_2 = $("<button></button>").text("Current");
  $(filter_button_2).addClass("btn btn-secondary btn-sm");
  $(filter_button_2).on('click', function(){

    minDateFilter = new Date();
    maxDateFilter = minDateFilter;
    
    minDateFilter = minDateFilter.getTime();
    maxDateFilter = maxDateFilter.getTime();
    table.draw();
  });

  var filter_button_3 = $("<input type='button'></input>").text("Range");
  $(filter_button_3).addClass("btn btn-secondary btn-sm daterange");
  $(filter_button_3).attr('id','timeFilter');

  var label_button_3 = $("<button class='btn btn-secondary btn-sm' for='timeFilter' style='justify-items : center !important; align-items : center !important; margin : auto !important; display : flex !important; text-align : center;'></button>").html("<label for='timeFilter' style='font-weight:inherit; padding : 0; margin : 0;'>Range : </label>");

  $(label_button_3).append(filter_button_3);

  $(document).ready(function() {

    $('#example1_wrapper .col-md-6:eq(0)').append(button_holder.append(filter_button_1, filter_button_2, label_button_3));

    $('.reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        use24hours: true,
        format: 'YYYY/MM/DD HH:mm'
      }
    })

    $.fn.dataTableExt.afnFiltering.push(
      function(oSettings, aData, iDataIndex) {
        if (typeof aData._date == 'undefined') {
          aData._date = new Date(aData[5]).getTime();

          aData._date1 = new Date(aData[5]);
          aData._date1 = aData._date1.getTime();

          aData._date2 = new Date(aData[6]);
          aData._date2 = aData._date2.getTime();
        }

        if (minDateFilter && !isNaN(minDateFilter)) {
          if (aData._date2 < minDateFilter) {
            return false;
          }
        }

        if (maxDateFilter && !isNaN(maxDateFilter)) {
          if (aData._date > maxDateFilter) {
            return false;
          }
        }

        return true;
      }
    );

    // Refilter the table
    $('#timeFilter').on('change', function () {
      const timeFilter = $('#timeFilter').val().split(" - ");
      // Create date inputs
      minDateFilter = new Date(timeFilter[0]);
      minDateFilter = minDateFilter.getTime();

      maxDateFilter = new Date(timeFilter[1]);
      maxDateFilter = maxDateFilter.getTime();
      table.draw();
    });
  });

</script>