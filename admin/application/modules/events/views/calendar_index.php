<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Calendar &nbsp;&nbsp;&nbsp;
        <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#modal-add" data-backdrop="static" data-keyboard="false"><span>+ Add Event</span></button></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <div class="sticky-top mb-3 none">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Draggable Events</h4>
            </div>
            <div class="card-body">
              <!-- the events -->
              <div id="external-events">
                <div class="external-event bg-success">Lunch</div>
                <div class="external-event bg-warning">Go home</div>
                <div class="external-event bg-info">Do homework</div>
                <div class="external-event bg-primary">Work on UI design</div>
                <div class="external-event bg-danger">Sleep tight</div>
                <div class="checkbox">
                  <label for="drop-remove">
                    <input type="checkbox" id="drop-remove">
                    remove after drop
                  </label>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Create Event</h3>
            </div>
            <div class="card-body">
              <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                <ul class="fc-color-picker" id="color-chooser">
                  <li><a class="text-primary" href="#"><i class="fas fa-square"></i></a></li>
                  <li><a class="text-warning" href="#"><i class="fas fa-square"></i></a></li>
                  <li><a class="text-success" href="#"><i class="fas fa-square"></i></a></li>
                  <li><a class="text-danger" href="#"><i class="fas fa-square"></i></a></li>
                  <li><a class="text-muted" href="#"><i class="fas fa-square"></i></a></li>
                </ul>
              </div>
              <!-- /btn-group -->
              <div class="input-group">
                <input id="new-event" type="text" class="form-control" placeholder="Event Title">

                <div class="input-group-append">
                  <button id="add-new-event" type="button" class="btn btn-primary">Add</button>
                </div>
                <!-- /btn-group -->
              </div>
              <!-- /input-group -->
            </div>
          </div>
        </div>
      </div>
      <!-- /.col -->
      <div class="col-12">
        <div class="card card-primary">
          <div class="card-body p-0">
            <!-- THE CALENDAR -->
            <div id="calendar"></div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
    <!-- /.content -->

<div class="modal fade" id="modal-add">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel2">Tambahkan Event</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('events/calendar/save');?>" id="form_add_calendar" method="post" enctype="multipart/form-data">
        <div class="modal-body">

          <div class="form-group row">
            <label for="nama_add_calendar" class="col-sm-12 col-form-label">Nama Event (*)</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="nama_add_calendar" name="nama_add_calendar" placeholder="Ketikan Nama Event" value="" required>
            </div>
          </div>
          
          <div class="form-group row">
            <label for="deskripsi_add_calendar" class="col-sm-12 col-form-label">Deskripsi Event (*)</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="deskripsi_add_calendar" name="deskripsi_add_calendar" placeholder="Ketikan Deskripsi Event" value="" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="ruang_add_calendar" class="col-sm-12 col-form-label">Ruang Event (*)</label>
            <div class="col-sm-12 input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id='lantai_add_calendar' style="min-width : 2.7em;"></span>
              </div>
              <select class="form-control" id="ruang_add_calendar" name="ruang_add_calendar" required>
                <option hidden selected>Pilih Ruangan Event</option>
                <?php 
                  foreach($room as $roomOpt){
                    ?>
                    <option value='<?= $roomOpt->lantai.'|'.$roomOpt->id_rooms.'|'.$roomOpt->nama_rooms ?>'><?= $roomOpt->nama_rooms ?></option>
                    <?php
                  }
                ?>
              </select>
            </div>
          </div>

          <div class="form-group row">
            <label for="waktu_mulai_add_calendar" class="col-sm-12 col-form-label">Waktu Mulai Event (*)</label>
            <div class="col-sm-12 input-group">
              <input type="datetime-local" class="form-control float-right" id="waktu_mulai_add_calendar" name="waktu_mulai_add_calendar" value="" style="background-color : inherit;" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="waktu_mulai_add_calendar" class="col-sm-12 col-form-label">Waktu Selesai Event (*)</label>
            <div class="col-sm-12 input-group">
              <input type="datetime-local" class="form-control float-right" id="waktu_selesai_add_calendar" name="waktu_selesai_add_calendar" value="" style="background-color : inherit;" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="hex-colour_add_calendar" class="col-sm-12 col-form-label">Warna Event (*)</label>
            <div class="col-sm-12 input-group my-colorpicker1 colorpicker-element" data-colorpicker-id="2">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-square"></i></span>
              </div>
              <input type="text" class="form-control" data-original-title="" title="" value="#7373ED" id="hex_colour_add_calendar" name="hex_colour_add_calendar" style="background-color : inherit;" required readonly>
            </div>
          </div>

          <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          <button type="button" id="add_submit_button" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
</div>


<div class="modal fade" id="modal-edit-calendar">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Edit Data Calendar</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="form-edit-calendar" action="<?= site_url('events/calendar/save_edit'); ?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="form-group row">
            <label for="nama_edit_calendar" class="col-sm-12 col-form-label">Nama Event (*)</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="nama_edit_calendar" name="nama_edit_calendar" placeholder="Ketikan Nama Event"
                value="" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="deskripsi_edit_calendar" class="col-sm-12 col-form-label">Deskripsi Event (*)</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="deskripsi_edit_calendar" name="deskripsi_edit_calendar" placeholder="Ketikan Deskripsi Event" value="" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="ruang_edit_calendar" class="col-sm-12 col-form-label">Ruang Event (*)</label>
            <div class="col-sm-12 input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id='lantai_edit_calendar' style="min-width : 2.7em;"></span>
              </div>
              <select class="form-control" id="ruang_edit_calendar" name="ruang_edit_calendar" required>
                <option id="selected_ruang" value hidden selected></option>
                <?php 
                  foreach($room as $roomOpt){
                    ?>
                    <option value='<?= $roomOpt->lantai.'|'.$roomOpt->id_rooms.'|'.$roomOpt->nama_rooms ?>'><?= $roomOpt->nama_rooms ?></option>
                    <?php
                  }
                ?>
              </select>
            </div>
          </div>

          <div class="form-group row">
            <label for="waktu_mulai_edit_calendar" class="col-sm-12 col-form-label">Waktu Mulai Event (*)</label>
            <div class="col-sm-12 input-group">
              <input type="datetime-local" class="form-control float-right" id="waktu_mulai_edit_calendar" name="waktu_mulai_edit_calendar" value="" style="background-color : inherit;" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="waktu_mulai_edit_calendar" class="col-sm-12 col-form-label">Waktu Selesai Event (*)</label>
            <div class="col-sm-12 input-group">
              <input type="datetime-local" class="form-control float-right" id="waktu_selesai_edit_calendar" name="waktu_selesai_edit_calendar" value="" style="background-color : inherit;" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="hex_colour_edit_calendar" class="col-sm-12 col-form-label">Warna Event (*)</label>
            <div class="input-group my-colorpicker1 colorpicker-element" data-colorpicker-id="2">
              <div class="input-group-prepend">
                <span class="input-group-text" id="boxcolor_edit_calendar"><i class="fas fa-square"></i></span>
              </div>
              <input type="text" class="form-control" data-original-title="" title="" value="#7373ED" id="hex_colour_edit_calendar" name="hex_colour_edit_calendar" style="background-color : inherit;" readonly required>
            </div>
          </div>

          <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
          <input type="hidden" name="id_calendar_edit" id="id_calendar_edit" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modal-delete-calendar" onlclick="javascript:void(0);">
            <i class="fas fa-trash"></i> Delete Event
          </button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          <button id="edit_button" type="button" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-delete-calendar">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin menghapus Event ini ?<br>
          Nama Event &nbsp; : <b id="nama_delete_calendar"></b><br>
      </div>
      <form action="<?= site_url('/events/calendar/delete'); ?>" method="post">
      <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
        <input type="hidden" name="id_calendar_delete" id="id_calendar_delete" value="">

        <div class="modal-footer">
          <button type="submit" class="btn btn-danger">Yes</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-info-calendar">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Event Tidak Dapat Ditambahkan!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Sudah ada event terencana pada ruangan dan waktu tersebut.<br>
        <table>
          <tr>
            <td>Nama Event</td><td>&nbsp;:&nbsp;<b id="nama_info_calendar"></b></td>
          </tr><tr>
            <td>Lantai</td><td>&nbsp;:&nbsp;<b id="lantai_info_calendar"></b></td>
          </tr><tr>
            <td>Ruang Event</td><td>&nbsp;:&nbsp;<b id="ruangan_info_calendar"></b></td>
          </tr><tr>
            <td>Waktu Mulai</td><td>&nbsp;:&nbsp;<b id="waktumulai_info_calendar"></b></td>
          </tr><tr>
            <td>Waktu Selesai</td><td>&nbsp;:&nbsp;<b id="waktuselesai_info_calendar"></b></td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Page specific script -->
<script>
  $(function () {

    /* initialize the external events
    -----------------------------------------------------------------*/
    function ini_events(ele) {
      ele.each(function () {

        // create an Event Object (https://fullcalendar.io/docs/event-object)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        }

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject)

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex        : 1070,
          revert        : true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        })

      })
    }

    ini_events($('#external-events div.external-event'))

    /* initialize the calendar
    -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendar.Draggable;

    var containerEl = document.getElementById('external-events');
    var checkbox = document.getElementById('drop-remove');
    var calendarEl = document.getElementById('calendar');

    // initialize the external events
    // -----------------------------------------------------------------

    new Draggable(containerEl, {
      itemSelector: '.external-event',
      eventData: function(eventEl) {
        return {
          title: eventEl.innerText,
          backgroundColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
          borderColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
          textColor: window.getComputedStyle( eventEl ,null).getPropertyValue('color'),
        };
      }
    });

    var calendar = new Calendar(calendarEl, {
      headerToolbar: {
        left  : 'prev,next today',
        center: 'title',
        right : 'dayGridMonth,timeGridWeek,timeGridDay',
      },
      timeZone: 'UTC',
      locale: 'id-ID',
      scrollTime: 0,
      themeSystem: 'bootstrap',
      events: [
        <?php
          $no = 1;
          foreach($grid as $data){
        ?>
        {
          id             : "<?= $data->id_calendars; ?>|<?= $data->nama_event; ?>|<?= $data->deskripsi; ?>|<?= $data->waktu_mulai; ?>|<?= $data->waktu_selesai; ?>|<?= $data->hex_colour; ?>|<?= $data->nama_rooms; ?>|<?= $data->lantai; ?>|<?= $data->id_rooms; ?>",
          title          : " | <?= $data->nama_event; ?>",
          start          : "<?= date_format(date_create($data->waktu_mulai),"Y-m-d H:i:s"); ?>",
          end            : "<?= date_format(date_create($data->waktu_selesai),"Y-m-d H:i:s"); ?>",
          backgroundColor: "#<?= $data->hex_colour; ?>",
          borderColor    : "#<?= $data->hex_colour; ?>",
        },  
        <?php } ?>
        {
          title          : "",
          start          : new Date(y, m, d - 5),
          end            : new Date(y, m, d - 2),
          backgroundColor: "#ffffff", 
          borderColor    : "#ffffff" 
        }
      ],
      eventClick     : function(info){
        var list = (info.event.id).split('|');

        document.getElementById("id_calendar_edit").value = list[0];
        document.getElementById("nama_edit_calendar").value = list[1];
        document.getElementById("deskripsi_edit_calendar").value = list[2];

        document.getElementById("waktu_mulai_edit_calendar").value = list[3];

        document.getElementById("waktu_selesai_edit_calendar").value = list[4];

        document.getElementById("hex_colour_edit_calendar").value = '#'+list[5];
        document.getElementById("boxcolor_edit_calendar").style.color = '#'+list[5];
        document.getElementById("lantai_edit_calendar").innerHTML = list[7];
        document.getElementById("selected_ruang").innerHTML = list[6];
        document.getElementById("ruang_edit_calendar").value = list[7]+"|"+list[8]+"|"+list[6];
        document.getElementById("id_calendar_delete").value = list[0];
        document.getElementById("nama_delete_calendar").innerHTML = list[1];

        $('#modal-edit-calendar').modal('show');
      },
      editable  : false,
      droppable : false
    });

    calendar.render();
    // $('#calendar').fullCalendar()

    // Display room level on change
    $('#ruang_add_calendar').change(function (){
      $('#lantai_add_calendar').html($('#ruang_add_calendar').val().split("|",1))
    });

    $('#ruang_edit_calendar').change(function (){
      $('#lantai_edit_calendar').html($('#ruang_edit_calendar').val().split("|",1))
    });

    // TIME VALIDATION FUNCTION -----
    $('#waktu_mulai_add_calendar').change(function(){

      var dateStart = new Date($('#waktu_mulai_add_calendar').val());
      var dateEnd = new Date($('#waktu_selesai_add_calendar').val());

      if($('#waktu_selesai_add_calendar').val() == '' || $('#waktu_selesai_add_calendar').val() == null || dateEnd.getTime() < dateStart.getTime()){

        $('#waktu_selesai_add_calendar').val($('#waktu_mulai_add_calendar').val());

      }
    });

    $('#waktu_selesai_add_calendar').change(function(){

      var dateStart = new Date($('#waktu_mulai_add_calendar').val());
      var dateEnd = new Date($('#waktu_selesai_add_calendar').val());

      if(dateEnd.getTime() < dateStart.getTime()){

        Swal.fire({
          title : 'Invalid Date',
          text: 'Tanggal selesai harus setelah tanggal mulai!',
          icon: 'error',
          showCancelButton: true,
          cancelButtonColor: '#3085d6',
          showConfirmButton : false,
          })

        $('#waktu_selesai_add_calendar').val($('#waktu_mulai_add_calendar').val());

      }
    })

    $('#waktu_selesai_edit_calendar').change(function(){

    var dateStart = new Date($('#waktu_mulai_edit_calendar').val());
    var dateEnd = new Date($('#waktu_selesai_edit_calendar').val());

    if(dateEnd.getTime() < dateStart.getTime()){

      Swal.fire({
        title : 'Invalid Date',
        text: 'Tanggal selesai harus setelah tanggal mulai!',
        icon: 'error',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        showConfirmButton : false,
        })

      $('#waktu_selesai_edit_calendar').val($('#waktu_mulai_edit_calendar').val());

    }
    })

    /* ADDING EVENTS */
    var currColor = '#3c8dbc' //Red by default
    // Color chooser button
    $('#color-chooser > li > a').click(function (e) {
      e.preventDefault()
      // Save color
      currColor = $(this).css('color')
      // Add color effect to button
      $('#add-new-event').css({
        'background-color': currColor,
        'border-color'    : currColor
      })
    })

    $('#add-new-event').click(function (e) {
      e.preventDefault()
      // Get value and make sure it is not null
      var val = $('#new-event').val()
      if (val.length == 0) {
        return
      }

      // Create events
      var event = $('<div />')
      event.css({
        'background-color': currColor,
        'border-color'    : currColor,
        'color'           : '#fff'
      }).addClass('external-event')
      event.text(val)
      $('#external-events').prepend(event)

      // Add draggable funtionality
      ini_events(event)

      // Remove event from text input
      $('#new-event').val('')
    })

    //Date range picker with time picker
    $('.reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        use24hours: true,
        format: 'YYYY/MM/DD HH:mm'
      }
    })

    //color picker with addon
    $('.my-colorpicker1').colorpicker();

    $('.my-colorpicker1').on('colorpickerChange', function(event) {
      $('.my-colorpicker1 .fa-square').css('color', event.color.toString());
    })

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })

    $('#modal-add').on('show.bs.modal', function (calendar) {
      var button = $(calendar.relatedTarget); // Button that triggered the modal
      var modal = $(this);
    })

    function convertTZ(date, tzString) {
        return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {timeZone: tzString}));
    }

    var currentDate = convertTZ(new Date(), "Asia/Jakarta");

    var months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    var days = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];

    var noon = [11, 12, 13, 14, 15]; var morning = [5, 6, 7, 8, 9 , 10]; var dusk = [16, 17, 18]; var night = [19, 20, 21, 22, 23, 24, 0, 1, 2, 3,4];

    var salamTime = "";
    if(dusk.includes(currentDate.getHours())){
        salamTime = " sore ";
    }else if(morning.includes(currentDate.getHours())){
      salamTime = " pagi ";
    }else if(night.includes(currentDate.getHours())){
      salamTime = " malam ";
    }else if(noon.includes(currentDate.getHours())){
      salamTime = " siang ";
    }

    $('#edit_button').click(function(result){
      $.ajax({
        type: "POST",
        url: "<?= site_url('/events/Calendar/checkEdit'); ?>",
        data: {
            'id_calendar_edit':$('#id_calendar_edit').val(),
            'ruang_add_calendar':$('#ruang_edit_calendar').val(),
            'waktu_mulai_add_calendar':$('#waktu_mulai_edit_calendar').val(),
            'waktu_selesai_add_calendar':$('#waktu_selesai_edit_calendar').val(),
        },
        success: function (response) {
          var return_arr = JSON.parse(response);

          var startEdit = new Date($('#waktu_mulai_edit_calendar').val());
          var endEdit = new Date($('#waktu_selesai_edit_calendar').val());

          var startTimeEdit = days[startEdit.getDay()]+', '+startEdit.getDate()+' '+months[startEdit.getMonth()]+' '+startEdit.getFullYear()+' - '+(startEdit.getHours() < 10 ? "0"+startEdit.getHours() : startEdit.getHours())+':'+(startEdit.getMinutes() < 10 ? "0"+startEdit.getMinutes() : startEdit.getMinutes());

          var endTimeEdit = days[endEdit.getDay()]+', '+endEdit.getDate()+' '+months[endEdit.getMonth()]+' '+endEdit.getFullYear()+' - '+(endEdit.getHours() < 10 ? "0"+endEdit.getHours() : endEdit.getHours())+':'+(endEdit.getMinutes() < 10 ? "0"+endEdit.getMinutes() : endEdit.getMinutes());

          if(return_arr.length==0){
            var waText = "Selamat"+ salamTime +"bapak / ibu di tempat. Mohon maaf mengganggu waktunya. Diinfokan terdapat perubahan perencanaan event yaitu event " + $("#nama_edit_calendar").val() + " pada tanggal " + startTimeEdit + " hingga " + endTimeEdit +" menggunakan ruang " + $("#ruang_edit_calendar").val().split("|")[0] + " - " + $("#ruang_edit_calendar").val().split("|")[2] + ". Demikian info ini disampaikan. Terima kasih atas perhatiannya."

            window.open("https://wa.me/6281212000103?text="+waText.replace(" ","%20"));

            $('#form-edit-calendar').submit();
          }
          else{
            var startClash = new Date(return_arr[0]['waktu_mulai']);
            var endClash = new Date(return_arr[0]['waktu_selesai']);

            var startTimeClash = days[startClash.getDay()]+', '+startClash.getDate()+' '+months[startClash.getMonth()]+' '+startClash.getFullYear()+' pada pukul '+(startClash.getHours() < 10 ? "0"+startClash.getHours() : startClash.getHours())+':'+(startClash.getMinutes() < 10 ? "0"+startClash.getMinutes() : startClash.getMinutes());

            var endTimeClash = days[endClash.getDay()]+', '+endClash.getDate()+' '+months[endClash.getMonth()]+' '+endClash.getFullYear()+' pada pukul '+(endClash.getHours() < 10 ? "0"+endClash.getHours() : endClash.getHours())+':'+(endClash.getMinutes() < 10 ? "0"+endClash.getMinutes() : endClash.getMinutes());

            $('#modal-info-calendar').modal('show');
            $('#nama_info_calendar').html(return_arr[0]['nama_event']);
            $('#lantai_info_calendar').html(return_arr[0]['lantai']);
            $('#ruangan_info_calendar').html(return_arr[0]['nama_rooms']);
            $('#waktumulai_info_calendar').html(startTimeClash);
            $('#waktuselesai_info_calendar').html(endTimeClash);
          }
        }
      });
    });

    $('#add_submit_button').click(function(resutl){
      $.ajax({
        type: "POST",
        url: "<?= site_url('/events/Calendar/check'); ?>",
        data: {
            'ruang_add_calendar':$('#ruang_add_calendar').val(),
            'waktu_mulai_add_calendar':$('#waktu_mulai_add_calendar').val(),
            'waktu_selesai_add_calendar':$('#waktu_selesai_add_calendar').val(),
        },
        success: function (response) {
          var return_arr = JSON.parse(response);

          var startAdd = new Date($('#waktu_mulai_add_calendar').val());
          var endAdd = new Date($('#waktu_selesai_add_calendar').val());

          var startTimeAdd = days[startAdd.getDay()]+', '+startAdd.getDate()+' '+months[startAdd.getMonth()]+' '+startAdd.getFullYear()+' pada pukul '+(startAdd.getHours() < 10 ? "0"+startAdd.getHours() : startAdd.getHours())+':'+(startAdd.getMinutes() < 10 ? "0"+startAdd.getMinutes() : startAdd.getMinutes());

          var endTimeAdd = days[endAdd.getDay()]+', '+endAdd.getDate()+' '+months[endAdd.getMonth()]+' '+endAdd.getFullYear()+' pada pukul '+(endAdd.getHours() < 10 ? "0"+endAdd.getHours() : endAdd.getHours())+':'+(endAdd.getMinutes() < 10 ? "0"+endAdd.getMinutes() : endAdd.getMinutes());

          if(return_arr.length==0){
            var waText = "Selamat"+ salamTime +"bapak / ibu di tempat. Mohon maaf mengganggu waktunya. Diinfokan terdapat perencanaan event baru yaitu event " + $("#nama_add_calendar").val() + " pada tanggal " + startTimeAdd + " hingga " + endTimeAdd + " menggunakan ruang " + $("#ruang_add_calendar").val().split("|")[0] + " -" + $("#ruang_add_calendar").val().split("|")[2] + ". Demikian info ini disampaikan. Terima kasih atas perhatiannya."

            window.open("https://wa.me/6281212000103?text="+waText.replace(" ","%20"));

            $("#form_add_calendar").submit();
          }
          else{
            var startClash = new Date(return_arr[0]['waktu_mulai']);
            var endClash = new Date(return_arr[0]['waktu_selesai']);

            var startTimeClash = days[startClash.getDay()]+', '+startClash.getDate()+' '+months[startClash.getMonth()]+' '+startClash.getFullYear()+' pada pukul '+(startClash.getHours() < 10 ? "0"+startClash.getHours() : startClash.getHours())+':'+(startClash.getMinutes() < 10 ? "0"+startClash.getMinutes() : startClash.getMinutes());

            var endTimeClash = days[endClash.getDay()]+', '+endClash.getDate()+' '+months[endClash.getMonth()]+' '+endClash.getFullYear()+' pada pukul '+(endClash.getHours() < 10 ? "0"+endClash.getHours() : endClash.getHours())+':'+(endClash.getMinutes() < 10 ? "0"+endClash.getMinutes() : endClash.getMinutes());

            $('#modal-info-calendar').modal('show');
            $('#nama_info_calendar').html(return_arr[0]['nama_event']);
            $('#lantai_info_calendar').html(return_arr[0]['lantai']);
            $('#ruangan_info_calendar').html(return_arr[0]['nama_rooms']);
            $('#waktumulai_info_calendar').html(startTimeClash);
            $('#waktuselesai_info_calendar').html(endTimeClash);
          }
        }
      });
    });
  });

</script>