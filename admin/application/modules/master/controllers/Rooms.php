<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Rooms extends MX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modelrooms');
	}

    public function index(){
		//load view with get data
		$this->page->view('rooms_index', array (
			'grid'		=> $this->Modelrooms->get_index(),
		));
    }

	public function save(){
		//array data from input post
		$data['room'] 		= $this->input->post('nama_add_rooms');
		$data['lantai'] 	= $this->input->post('lantai_add_rooms');
		$data['id_user'] 	= $this->input->post('id_user');

		//save data
		$this->Modelrooms->simpan($data);

		//redirect pages
		redirect($this->agent->referrer());
    }

	public function save_edit(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_rooms_edit');
		$data['room'] 		= $this->input->post('nama_edit_rooms');
		$data['lantai'] 	= $this->input->post('lantai_edit_rooms');
		$data['id_user'] 	= $this->input->post('id_user');

		//update data
		$this->Modelrooms->simpan_edit($data);

		//redirect page
		redirect($this->agent->referrer());
    }

	public function delete(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_rooms_delete');
		$data['room'] 		= $this->input->post('nama_rooms_delete');
		$data['id_user'] 	= $this->input->post('id_user');

		//delete data
		$this->Modelrooms->hapus($data);

		//redirect page
		redirect($this->agent->referrer());
    }
}