<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Community extends MX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modelcommunity');
	}

	// Community Home View ----------
    public function index(){
		//load view with get data
		$this->page->view('community_index', array (
			'grid'		=> $this->Modelcommunity->get_index(),
			'jemaat'	=> $this->Modelcommunity->get_jemaat(),
		));
    }

	public function save(){
		$data['nama'] 		= $this->input->post('nama_add_community');
		$data['pks']		= $this->input->post('pks_add_community');
		$data['hp_pks']		= $this->input->post('hp_pks_add_community');
		$data['wilayah'] 	= $this->input->post('wilayah_add_community');
		$data['id_user'] 	= $this->input->post('id_user');

		$this->Modelcommunity->simpan($data);

		redirect($this->agent->referrer());
    }

	public function save_edit(){
		$data['id']			= $this->input->post('id_community_edit');
		$data['nama'] 		= $this->input->post('nama_edit_community');
		$data['pks']		= $this->input->post('pks_edit_community');
		$data['hp_pks']		= $this->input->post('hp_pks_edit_community');
		$data['wilayah'] 	= $this->input->post('wilayah_edit_community');
		$data['id_user'] 	= $this->input->post('id_user');

		$this->Modelcommunity->simpan_edit($data);

		redirect($this->agent->referrer());
    }

	public function delete(){
		$data['id'] 		= $this->input->post('id_community_delete');
		$data['nama'] 		= $this->input->post('nama_community_delete');
		$data['id_user'] 	= $this->input->post('id_user');

		$this->Modelcommunity->hapus($data);

		redirect($this->agent->referrer());
    }

	public function aktif(){
		$data['id'] 		= $this->input->post('id_community_aktif');
		$data['nama'] 		= $this->input->post('nama_community_aktif');
		$data['id_user'] 	= $this->input->post('id_user');

		$this->Modelcommunity->aktif($data);

		redirect($this->agent->referrer());
    }

	public function nonaktif(){
		$data['id'] 		= $this->input->post('id_community_nonaktif');
		$data['nama'] 		= $this->input->post('nama_community_nonaktif');
		$data['id_user'] 	= $this->input->post('id_user');

		$this->Modelcommunity->nonaktif($data);

		redirect($this->agent->referrer());
    }
	// ----------

	// Community Member View ----------
	public function community_list($idKomsel){
		$this->page->view('community_member', array(
			'komsel'	=> $this->Modelcommunity->get_komsel($idKomsel),
			'member'	=> $this->Modelcommunity->get_member($idKomsel),
			'kebun'		=> $this->Modelcommunity->get_index(),
			'nonmember' => $this->Modelcommunity->get_nonmember(),
		));
	}

	public function add_member_gpdi(){
		$memberData			= explode(" - ", $this->input->post('jemaat_add'));

		$data['nama'] 		= $memberData[0];
		$data['nomor']		= $memberData[1];
		$data['alamat'] 	= $memberData[2];
		$data['id_user'] 	= $this->input->post('id_user');
		$data['id_komsel']  = $this->input->post('id_komsel_gpdi');

		$this->Modelcommunity->add_gpdi($data);

		redirect($this->agent->referrer());
	}

	public function add_member_nongpdi(){
		$data['group'] 		  			= $this->input->post('id_keluarga_nongpdi');
		$data['nama'] 		  			= strtoupper($this->input->post('nama_nongpdi'));
		$data['tempat_lahir']  			= strtoupper($this->input->post('tempat_lahir_nongpdi'));
        $data['tanggal_lahir'] 			= ($this->input->post('tanggal_lahir_nongpdi')) != "" ? date("Y/m/d", strtotime($this->input->post('tanggal_lahir_nongpdi'))) : "";
		$data['lp']		      			= $this->input->post('jenis_kelamin_nongpdi');
		$data['pendidikan_terakhir'] 	= $this->input->post('pendidikan_nongpdi');
		$data['pekerjaan'] 	  			= strtoupper($this->input->post('pekerjaan_nongpdi'));
		$data['alamat']					= strtoupper($this->input->post('alamat_nongpdi'));
        $data['nomor_hp'] 	        	= $this->input->post('hp_nongpdi');
        $data['email']        			= $this->input->post('email_nongpdi');
        $data['diserahkan_anak']    	= ($this->input->post('diserahkan_anak_nongpdi')) != "" ? date("Y/m/d", strtotime($this->input->post('diserahkan_anak_nongpdi'))) : '';
        $data['baptis_selam']  			= ($this->input->post('baptis_selam_nongpdi')) != "" ? date("Y/m/d", strtotime($this->input->post('baptis_selam_nongpdi'))) : '';
        $data['nikah'] 	      			= ($this->input->post('tanggal_nikah_nongpdi')) != "" ? date("Y/m/d", strtotime($this->input->post('tanggal_nikah_nongpdi'))) : '';
        $data['smk'] 	      			= $this->input->post('smk_nongpdi');
        $data['kebun'] 	      			= $this->input->post('kebun_nongpdi');
        $data['hubungan_kk'] 	      	= $this->input->post('hubungan_kk__nongpdi');

		$this->Modelcommunity->add_nongpdi($data);

		redirect($this->agent->referrer());
	}
	// ----------
}