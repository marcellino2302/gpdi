<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Lwg extends MX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modellwg');
	}

    public function index(){
		//load view with get data
		$this->page->view('lwg_index', array (
			'grid'		=> $this->Modellwg->get_index(),
		));
    }

	public function save(){
		//redirect pages
		redirect($this->agent->referrer());
    }

	// function update hanya menyimpan data pada data base dan tidak melakukan reload pada halaman edit lwg
	// karena tidak dibutuhkan reload pada halaman tersebut.
	public function update(){
		$logoImage='';
		$bannerImage='';
		$file='';
		// Check if any files were uploaded
		if (!empty($_FILES)) {
			$uploadPath = './image/lwg'; // Set the path where you want to store the uploaded files
	
			// Load the CodeIgniter upload library
			$this->load->library('upload');
	
			// Loop through each uploaded file
			$i=0;
			foreach ($_FILES as $fieldName => $fileData) {

				$path = $fileData['name'];
				$ext = pathinfo($path, PATHINFO_EXTENSION);

				if($fieldName=='banner'){
					$config = array(
						'file_name' => 'banner',
						'upload_path' => $uploadPath,
						'allowed_types' => "*",
						'overwrite' => TRUE,
					);

					$bannerImage.=$config['file_name'].'.'.$ext;
				}
				else if($fieldName=='logo'){
					$config = array(
						'file_name' => 'logo',
						'upload_path' => $uploadPath,
						'allowed_types' => "*",
						'overwrite' => TRUE,
					);

					$logoImage.=$config['file_name'].'.'.$ext;
				}
				else{
					$config = array(
						'file_name' => $i++,
						'upload_path' => $uploadPath,
						'allowed_types' => "*",
						'overwrite' => TRUE,
					);

					$file.=$config['file_name'].'.'.$ext.',';
				}
	
				// Initialize the upload library with the config
				$this->upload->initialize($config);

				// Upload the file
				if ($this->upload->do_upload($fieldName)) {
					// File upload successful
					$uploadData = $this->upload->data();
				} else {
					// File upload failed
					$error = $this->upload->display_errors();
				}
			}
		}
	
		$lastmodified = date("Y-m-d H:i:s");

		if($bannerImage != ""){
			$id = 1;
			$data = array(
				'data_value' => $bannerImage,
				'lastmodified' => $lastmodified
			);
			$where = array(
				'id_lwg' => $id
			);
			$this->Modellwg->update_data($where,$data,'wb_lwg');
		}

		// title
		$id = 2;
		$data = array(
			'data_value' => $this->input->post('title'),
			'lastmodified' => $lastmodified
		);
		$where = array(
			'id_lwg' => $id
		);
		$this->Modellwg->update_data($where,$data,'wb_lwg');

		// description
		$id = 3;
		$data = array(
			'data_value' => $this->input->post('description'),
			'lastmodified' => $lastmodified
		);
		$where = array(
			'id_lwg' => $id
		);
		$this->Modellwg->update_data($where,$data,'wb_lwg');

		// gallery
		$id = 4;
		$data = array(
			'data_value' => $file,
			'lastmodified' => $lastmodified
		);
		$where = array(
			'id_lwg' => $id
		);
		$this->Modellwg->update_data($where,$data,'wb_lwg');

		// yt link
		$id = 5;
		$data = array(
			'data_value' => $this->input->post('ytlink'),
			'lastmodified' => $lastmodified
		);
		$where = array(
			'id_lwg' => $id
		);
		$this->Modellwg->update_data($where,$data,'wb_lwg');

		// ig link
		$id = 6;
		$data = array(
			'data_value' => $this->input->post('iglink'),
			'lastmodified' => $lastmodified
		);
		$where = array(
			'id_lwg' => $id
		);
		$this->Modellwg->update_data($where,$data,'wb_lwg');

		// fb link
		$id = 7;
		$data = array(
			'data_value' => $this->input->post('fblink'),
			'lastmodified' => $lastmodified
		);
		$where = array(
			'id_lwg' => $id
		);
		$this->Modellwg->update_data($where,$data,'wb_lwg');

		// tw link
		$id = 8;
		$data = array(
			'data_value' => $this->input->post('twlink'),
			'lastmodified' => $lastmodified
		);
		$where = array(
			'id_lwg' => $id
		);
		$this->Modellwg->update_data($where,$data,'wb_lwg');

		$id = 9;
		$data = array(
			'data_value' => $this->input->post('keterangan'),
			'lastmodified' => $lastmodified
		);
		$where = array(
			'id_lwg' => $id
		);
		$this->Modellwg->update_data($where,$data,'wb_lwg');

		if($logoImage != ""){
			$id = 10;
			$data = array(
				'data_value' => $logoImage,
				'lastmodified' => $lastmodified
			);
			$where = array(
				'id_lwg' => $id
			);
			$this->Modellwg->update_data($where,$data,'wb_lwg');
		}
	}

}