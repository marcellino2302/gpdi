<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Jemaat extends MX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modeljemaat');
	}

    public function index(){
		//load view with get data
		$this->page->view('jemaat_index', array (
			'grid'		=> $this->Modeljemaat->get_index(),
			'kebun'		=> $this->Modeljemaat->get_kebun(),
		));
    }

    public function checkSum($valPar){
        if($valPar != ''){
            return $valPar;
        }else {
            return DateTime::createFromFormat('d-F-Y', '')->format('Y-m-d');
        }
    }

	public function addJemaat(){
		//load view with get data
		$this->page->view('jemaat_add', array (
			'grid'		=> $this->Modeljemaat->get_index(),
			'max_group' => $this->Modeljemaat->get_max_group(),
			'kebun'		=> $this->Modeljemaat->get_kebun(),
		));
	}

	public function checkJemaat(){
		$group = $this->input->post('group');
		echo json_encode($this->Modeljemaat->check_jemaat($group));
		return json_encode($this->Modeljemaat->check_jemaat($group));
	}

	public function jemaatUpdate(){
		$jemaatUpdateStatus = $this->input->post('updateStatus');
		$idUpdateJemaat = $this->input->post('idUpdateJemaat');

		$this->Modeljemaat->jemaat_update($jemaatUpdateStatus, $idUpdateJemaat);
	}

	public function save(){
		//array data from input post
		$data['group'] 		  			= $this->input->post('id_keluarga');
		$data['nama'] 		  			= strtoupper($this->input->post('nama_add'));
		$data['tempat_lahir']  			= strtoupper($this->input->post('tempat_lahir_add'));
        $data['tanggal_lahir'] 			= ($this->input->post('tanggal_lahir_add')) != "" ? date("Y/m/d", strtotime($this->input->post('tanggal_lahir_add'))) : "";
		$data['lp']		      			= $this->input->post('jenis_kelamin_add');
		$data['pendidikan_terakhir'] 	= $this->input->post('pendidikan_add');
		$data['pekerjaan'] 	  			= strtoupper($this->input->post('pekerjaan_add'));
		$data['alamat']					= strtoupper($this->input->post('alamat_add'));
        $data['nomor_hp'] 	        	= $this->input->post('hp_add');
        $data['email']        			= $this->input->post('email_add');
        $data['diserahkan_anak']    	= ($this->input->post('diserahkan_anak_add')) != "" ? date("Y/m/d", strtotime($this->input->post('diserahkan_anak_add'))) : '';
        $data['baptis_selam']  			= ($this->input->post('baptis_selam_add')) != "" ? date("Y/m/d", strtotime($this->input->post('baptis_selam_add'))) : '';
        $data['nikah'] 	      			= ($this->input->post('tanggal_nikah_add')) != "" ? date("Y/m/d", strtotime($this->input->post('tanggal_nikah_add'))) : '';
        $data['smk'] 	      			= $this->input->post('smk_add');
        $data['kebun'] 	      			= $this->input->post('kebun_add');
        $data['hubungan_kk'] 	      	= $this->input->post('hubungan_kk_add');

		//save data
		$this->Modeljemaat->simpan($data);

		//redirect pages
		redirect($this->agent->referrer());
    }

	public function save_edit(){
		//array data from input post
		$data['id'] 					= $this->input->post('id_jemaat_edit');
        $data['group'] 		  			= $this->input->post('id_keluarga');
		$data['nama'] 		  			= strtoupper($this->input->post('nama_edit'));
		$data['tempat_lahir']  			= strtoupper($this->input->post('tempat_lahir_edit'));
        $data['tanggal_lahir'] 			= ($this->input->post('tanggal_lahir_edit')) != "" ? date("Y/m/d", strtotime($this->input->post('tanggal_lahir_edit'))) : "";
		$data['lp']		      			= $this->input->post('jenis_kelamin_edit');
		$data['pendidikan_terakhir'] 	= $this->input->post('pendidikan_edit');
		$data['pekerjaan'] 	  			= strtoupper($this->input->post('pekerjaan_edit'));
		$data['alamat']					= strtoupper($this->input->post('alamat_edit'));
        $data['nomor_hp'] 	        	= $this->input->post('hp_edit');
        $data['email']        			= $this->input->post('email_edit');
        $data['diserahkan_anak']    	= ($this->input->post('diserahkan_anak_edit')) != "" ? date("Y/m/d", strtotime($this->input->post('diserahkan_anak_edit'))) : '';
        $data['baptis_selam']  			= ($this->input->post('baptis_selam_edit')) != "" ? date("Y/m/d", strtotime($this->input->post('baptis_selam_edit'))) : '';
        $data['nikah'] 	      			= ($this->input->post('tanggal_nikah_edit')) != "" ? date("Y/m/d", strtotime($this->input->post('tanggal_nikah_edit'))) : '';
        $data['smk'] 	      			= $this->input->post('smk_edit');
        $data['kebun'] 	      			= $this->input->post('kebun_edit');
        $data['hubungan_kk'] 	      	= $this->input->post('hubungan_kk_edit');
		$data['id_user']				= $this->input->post('id_user');

		//update data
		$this->Modeljemaat->simpan_edit($data);

		//redirect page
		redirect($this->agent->referrer());
    }

	public function delete(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_jemaat_delete');
		$data['nama'] 		= $this->input->post('nama_jemaat_delete');
		$data['id_user'] 	= $this->input->post('id_user');

		//delete data
		$this->Modeljemaat->hapus($data);

		//redirect page
		redirect($this->agent->referrer());
    }

	public function aktif(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_jemaat_aktif');
		$data['nama'] 		= $this->input->post('nama_jemaat_aktif');
		$data['id_user'] 	= $this->input->post('id_user');

		//aktif data
		$this->Modeljemaat->aktif($data);

		//redirect page
		redirect($this->agent->referrer());
    }

	public function nonaktif(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_jemaat_nonaktif');
		$data['nama'] 		= $this->input->post('nama_jemaat_nonaktif');
		$data['status'] 	= $this->input->post('keterangan_nonaktif');
		$data['id_user'] 	= $this->input->post('id_user');

		//non aktif data
		$this->Modeljemaat->nonaktif($data);

		//redirect page
		redirect($this->agent->referrer());
    }
}