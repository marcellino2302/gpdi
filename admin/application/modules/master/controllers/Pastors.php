<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Pastors extends MX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modelpastors');
	}

    public function index(){
		//load view with get data
		$this->page->view('pastors_index', array (
			'grid'		=> $this->Modelpastors->get_index(),
		));
    }

	public function save(){
		//array data from input post
		$data['nama'] 		= $this->input->post('nama_add_pastors');
		$data['foto'] 		= $_FILES['foto_user2']['name'];
		$data['id_user'] 	= $this->input->post('id_user');

		//save data
		$this->Modelpastors->simpan($data);

		//upload image
		$config = array(
			'file_name' => $this->input->post('id_user')."-".str_replace(" ","",$_FILES['foto_user2']['name']),
			'upload_path' => "./image/pastors/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf|xls|xlsx",
			'overwrite' => TRUE,
		);
		$this->load->library('upload', $config);
		$this->upload->do_upload('foto_user2');

		//redirect pages
		redirect($this->agent->referrer());
    }

	public function save_edit(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_pastors_edit');
		$data['nama'] 		= $this->input->post('nama_pastors');
		$data['id_user'] 	= $this->input->post('id_user');
		if($_FILES['foto_user']['name'] == '' ||  $_FILES['foto_user']['name'] == null){
			$data['foto'] 		= $this->input->post('fotoLama');
			$foto 				= $this->input->post('fotoLama');
		}else{
			$data['foto'] 		= $this->input->post('id_user')."-".str_replace(" ","",$_FILES['foto_user']['name']);
			$foto				= $this->input->post('id_user')."-".str_replace(" ","",$_FILES['foto_user']['name']);
		}

		//update data
		$this->Modelpastors->simpan_edit($data);

		//upload image
		$config = array(
			'file_name' => $foto,
			'upload_path' => "./image/pastors/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf|xls|xlsx",
			'overwrite' => TRUE,
		);
		$this->load->library('upload', $config);
		if($_FILES['foto_user']['name'] == '' ||  $_FILES['foto_user']['name'] == null){
		}else{
			$this->upload->do_upload('foto_user');
		}

		//redirect page
		redirect($this->agent->referrer());
    }

	public function delete(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_pastors_delete');
		$data['nama'] 		= $this->input->post('nama_pastors_delete');
		$data['id_user'] 	= $this->input->post('id_user');

		//delete data
		$this->Modelpastors->hapus($data);

		//redirect page
		redirect($this->agent->referrer());
    }
}