<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Community
        </h1>
      </div><!-- /.col -->
      <div class="col-sm-6">

      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">

          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style ="width: 5%;">No</th>
                  <th>Nama KeBun</th>
                  <th>PKS</th>
                  <th>No PKS</th>
                  <th>Wilayah</th>
                  <th style ="text-align: center;">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $no = 1;
                  foreach($grid as $data){
                  ?>
                <tr <?= $data->status=='1'?'class="table-danger"':'' ?>>
                  <td><?= number_format($no++,0)?></td>
                  <td><?= $data->nama_komsel; ?></td>
                  <td><?= $data->nama; ?></td>
                  <td><?= $data->nomor_hp; ?></td>
                  <td><?= $data->wilayah; ?></td>
                  <td style="text-align: center;">

                    <button class="btn btn-info btn-sm" name="id_ev" style="margin-right: 15px;"
                      data-a="<?= $data->id; ?>"
                      data-b="<?= $data->nama_komsel; ?>"
                      data-c="<?= $data->nama; ?> - <?= $data->nomor_hp; ?>"
                      data-d="<?= $data->nomor_hp; ?>"
                      data-e="<?= $data->wilayah; ?>"
                      data-toggle="modal" data-target="#modal-edit-community" data-backdrop="static"
                      data-keyboard="false">
                      <i class="fas fa-pencil-alt">
                      </i>
                      Edit
                    </button>

                    <button class="btn btn-primary btn-sm" name="community_member" id="community_member" style="margin-right: 15px;" onclick="javascript:location.href = '<?= BASE_URL()?>master/community/community_list/'+'<?= $data->id; ?>'"><i class="fa fa-users" aria-hidden="true"></i> Member</button>

                    <?php
                    if($data->status == '0'){
                      ?>
                      <button class="btn btn-danger btn-sm" data-backdrop="static" data-keyboard="false"
                        data-c="<?= $data->id; ?>" 
                        data-v="<?= $data->nama_komsel; ?>"
                        data-toggle="modal" data-target="#modal-nonaktif-community">
                        <i class="fas fa-toggle-off">
                        </i>
                        Non Aktif
                      </button>
                      <?php
                    }else{
                      ?>
                      <button class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false"
                        data-c="<?= $data->id; ?>" 
                        data-v="<?= $data->nama_komsel; ?>"
                        data-toggle="modal" data-target="#modal-aktif-community">
                        <i class="fas fa-toggle-on">
                        </i>
                        Aktif
                      </button>
                      <?php
                    }
                    ?>
                    

                  </td>
                </tr>

                <?php } ?>

              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

      </div>
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>

<div class="modal fade" id="modal-delete-community">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin menghapus KeBun ini ?<br>
          Nama KeBun &nbsp; : <b id="nama_community_delete"></b><br>
      </div>
      <form action="<?= site_url('master/community/delete');?>" method="post">
      <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user;?>" />
        <input type="hidden" name="id_community_delete" id="id_community_delete" value="">

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger">Yes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-aktif-community">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin aktifkan KeBun ini ?<br>
          Nama KeBun &nbsp; : <b id="nama_community_aktif"></b><br>
      </div>
      <form action="<?= site_url('master/community/aktif');?>" method="post">
      <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user;?>" />
        <input type="hidden" name="id_community_aktif" id="id_community_aktif" value="">

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger">Yes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-nonaktif-community">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin non aktifkan KeBun ini ?<br>
          Nama KeBun &nbsp; : <b id="nama_community_nonaktif"></b><br>
      </div>
      <form action="<?= site_url('master/community/nonaktif');?>" method="post">
      <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user;?>" />
        <input type="hidden" name="id_community_nonaktif" id="id_community_nonaktif" value="">

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger">Yes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<div class="modal fade" id="modal-edit-community">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Edit Data KeBun</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('master/community/save_edit');?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="form-group row">
            <label for="nama_edit_community" class="col-sm-12 col-form-label">Nama KeBun (*)</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="nama_edit_community" name="nama_edit_community" placeholder="Ketikan Nama KeBun"
                value="" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="pks_edit_community" class="col-sm-12 col-form-label">PKS (*)</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="pks_edit_community" name="pks_edit_community" placeholder="Ketikan PKS"
                value="" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="wilayah_edit_community" class="col-sm-12 col-form-label">Wilayah (*)</label>
            <div class="col-sm-12">
              <textarea rows=3 class="form-control" id="wilayah_edit_community" name="wilayah_edit_community" placeholder="Ketikan Wilayah" required></textarea>
            </div>
          </div>
          
          <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
          <input type="hidden" name="id_community_edit" id="id_community_edit" value="">
          <input type="hidden" class="form-group" id="fotoLama" name="fotoLama">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-add">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel2">Tambahkan Data KeBun</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('master/community/save')?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">
        <div class="form-group row">
            <label for="nama_add_community" class="col-sm-12 col-form-label">Nama KeBun (*)</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="nama_add_community" name="nama_add_community" placeholder="Ketikan KeBun"
                value="" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="pks_add_community" class="col-sm-12 col-form-label">PKS (*)</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="pks_add_community" name="pks_add_community" placeholder="Ketikan PKS"
                value="" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="wilayah_add_community" class="col-sm-12 col-form-label">Wilayah (*)</label>
            <div class="col-sm-12">
              <textarea rows=3 class="form-control" id="wilayah_add_community" name="wilayah_add_community" placeholder="Ketikan Wilayah" required></textarea>
            </div>
          </div>
          

          <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">

          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
</div>

<style>
  .ui-autocomplete {
    border-radius: 0.5em;
    max-height: 200px;
    overflow-y: auto;
    overflow-x: hidden;
    padding-right: 20px;
    position: absolute;
    z-index: 99999 !important;
    padding: 0;
    margin-top: 2px;
  }

  .ui-autocomplete>li {
    padding: 3px 20px;
  }

  .ui-autocomplete>li.ui-state-focus {
    background-color: #DDD;
  }

  .ui-helper-hidden-accessible {
    display: none;
  }
</style>

<script>
  $(document).ready(function(){
    $(function () {
      var jemaatList = [<?php foreach($jemaat as $jemaatList){
        echo '"'.($jemaatList->nama).' - '.($jemaatList->nomor_hp).'",';
      } ;?>];

      $('#modal-edit-community').on('shown.bs.modal', function() {

        $("#pks_edit_community").autocomplete({
          source: function(request, response) {
            var results = $.ui.autocomplete.filter(jemaatList, request.term);

            response(results.slice(0, 10));
          }
        });
      })

      $('#modal-add').on('shown.bs.modal', function() {

        $("#pks_add_community").autocomplete({
          source: function(request, response) {
            var results = $.ui.autocomplete.filter(jemaatList, request.term);

            response(results.slice(0, 10));
          }
        });
      })
    });
  });

  $(function () {

    $("#example1").DataTable({
      "responsive": true,
      "lengthChange": false,
      "autoWidth": false,
      "paging": true,
      "stateSave": true,
      // "sorting": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print"],
      buttons: [
          {
              text: '+ Add KeBun',
              action: function ( e, dt, node, config ) {
                  $('#modal-add').modal({backdrop: 'static', keyboard: false});  
              }
          }
      ]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    });

  $('#modal-delete-community').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient_c = button.data('c');

    var recipient_v = button.data('v');

    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('.id_community_delete').val(recipient_c);
    document.getElementById("id_community_delete").value = recipient_c;

    document.getElementById("nama_community_delete").innerHTML = recipient_v;
  })

  $('#modal-aktif-community').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient_c = button.data('c');

    var recipient_v = button.data('v');

    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('.id_community_aktif').val(recipient_c);
    document.getElementById("id_community_aktif").value = recipient_c;

    document.getElementById("nama_community_aktif").innerHTML = recipient_v;
  })

  $('#modal-nonaktif-community').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient_c = button.data('c');

    var recipient_v = button.data('v');

    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('.id_community_nonaktif').val(recipient_c);
    document.getElementById("id_community_nonaktif").value = recipient_c;

    document.getElementById("nama_community_nonaktif").innerHTML = recipient_v;
  })


  $('#modal-edit-community').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient_a = button.data('a');
    var recipient_b = button.data('b');
    var recipient_c = button.data('c');
    var recipient_d = button.data('d');
    var recipient_e = button.data('e');

    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('.id_community_edit').val(recipient_a);
    document.getElementById("id_community_edit").value = recipient_a;
          
    modal.find('.nama_edit_community').val(recipient_b);
    document.getElementById("nama_edit_community").value = recipient_b;

    modal.find('.pks_edit_community').val(recipient_c);
    document.getElementById("pks_edit_community").value = recipient_c;

    modal.find('.wilayah_edit_community').val(recipient_e);
    document.getElementById("wilayah_edit_community").value = recipient_e;

  });

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', {
      'placeholder': 'dd/mm/yyyy'
    })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('dd/mm/yyyy', {
      'placeholder': 'dd/mm/yyyy'
    })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate2').datetimepicker({
      format: 'DD-MMMM-yyyy'
    });
    //Date range picker
    $('#reservation2').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime2').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'DD/MM/YYYY'
      }
    })

    //Date range picker
    $('#reservationdate').datetimepicker({
      format: 'DD-MMMM-yyyy'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'DD/MM/YYYY'
      }
    })

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'DD/MM/YYYY'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()
  })

  $('#modal-add').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var modal = $(this)
  })

</script>