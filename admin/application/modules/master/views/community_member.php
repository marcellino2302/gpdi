<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <div>
          <div style="display: flex; align-items: center; text-align: center; justify-content: center;">
            <h1 class="m-0" style="margin-left: 1em;"><?php foreach($komsel as $komsel){?> Community Member</h1>
          </div>
          <button class="btn btn-primary" id="backButton" style="margin-bottom : 0.5em;"><i class="fa fa-angle-left" aria-hidden="true"></i> Community List</button>
        </div>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="card card-primary" style="margin-left: 1em; margin-right: 1em;">
  <div class="card-header">
    <input type="hidden" name="id_komsel" id="id_komsel" value="<?= $komsel->id; ?>">
    <h3 class="card-title" style="font-size: 1.8em;"><?= $komsel->nama_komsel; ?></h3>
  </div>

  <div class="card-body">

    <div class="row" style="display:none;">
      <div class="col-md">

        <div class="form-group">
          <div>
            <label for="nama_edit_community">Nama Kebun(*)</label>
            <input type="text" class="form-control" id="nama_community" name="nama_community" value="<?= ($komsel->nama_komsel);?>" readonly>
          </div>
        </div>

      </div>

      <div class="col-md-2">

        <div class="form-group">
          <label for="nama_edit_community">Action</label>

          <div style="text-align: center;">
            <button class="btn btn-info btn-sm" name="id_ev" style="margin-right: 15px;"
              data-a="<?= $komsel->id; ?>"
              data-b="<?= $komsel->nama_komsel; ?>"
              data-c="<?= $komsel->nama; ?> - <?= $komsel->nomor_hp; ?>"
              data-d="<?= $komsel->nomor_hp; ?>"
              data-e="<?= $komsel->wilayah; ?>"
              data-toggle="modal" data-target="#modal-edit-community" data-backdrop="static"
              data-keyboard="false">
              <i class="fas fa-pencil-alt">
              </i>
              Edit
            </button>

            <?php
            if($komsel->status == '0'){
              ?>
              <button class="btn btn-danger btn-sm" data-backdrop="static" data-keyboard="false"
                data-c="<?= $komsel->id; ?>" 
                data-v="<?= $komsel->nama_komsel; ?>"
                data-toggle="modal" data-target="#modal-nonaktif-community">
                <i class="fas fa-toggle-off">
                </i>
                Non Aktif
              </button>
              <?php
            }else{
              ?>
              <button class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false"
                data-c="<?= $komsel->id; ?>" 
                data-v="<?= $komsel->nama_komsel; ?>"
                data-toggle="modal" data-target="#modal-aktif-community">
                <i class="fas fa-toggle-on">
                </i>
                Aktif
              </button>
              <?php
            }
            ?>
          </div>
          
        </div>

      </div>

    </div>

    <div class="row">

      <div class="col-md-3">

        <div class="form-group">
          <label for="nama_edit_community">Nama PKS</label>
          <input type="text" class="form-control" id="pks_community" name="pks_community" value="<?= ($komsel->nama);?>" readonly>
        </div>

      </div>

      <div class="col-md-3">

        <div class="form-group">
          <label for="nama_edit_community">Nomor PKS</label>
          <input type="text" class="form-control" id="nomors_community" name="nomor_community" value="<?= ($komsel->nomor_hp);?>" readonly>
        </div>

      </div>

      <div class="col-md-6">

        <div class="form-group">
          <label for="nama_edit_community">Wilayah Kebun</label>
          <input type="text" class="form-control" id="wilayah_community" name="wilayah_community" value="<?= ($komsel->wilayah);?>" readonly>
        </div>
        <?php };?>
      </div>

    </div>

  </div>
</div>

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">

          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th></th>
                  <th style="width: 5%;">No</th>
                  <th>Keluarga</th>
                  <th>Nama Jemaat</th>
                  <th style="text-align: center;">Updated</th>
                  <th>Tempat Lahir</th>
                  <th>Tanggal Lahir</th>
                  <th>Jenis Kelamin</th>
                  <th class="none"></th>
                  <th style="text-align: center;">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $no = 1;
                  foreach($member as $data){
                  ?>
                <tr <?php if ($data->status=='4'){
                  echo 'class="table-secondary"';
                }else if(($data->status!='0') && ($data->status!='4')){
                  echo 'class="table-danger"';
                }?>>
                  <td></td>
                  <td><?= number_format($no++,0)?></td>
                  <td><?= $data->group; ?></td>
                  <td><?= $data->nama; ?></td>
                  <td style="text-align: center;">
                    <?php if($data->updated == '1'){ ?>
                      <div class="icheck-primary d-inline">
                        <input type="checkbox" id="updatedJemaat<?= $no ;?>" onchange="jemaatUpdate(this.id);" name="<?= $data->id ;?>" checked>
                        <label for="updatedJemaat<?= $no ;?>"></label>
                      </div>
                    <?php } else { ?> 
                      <div class="icheck-primary d-inline">
                        <input type="checkbox" id="updatedJemaat<?= $no ;?>" onchange="jemaatUpdate(this.id);" name="<?= $data->id ;?>">
                        <label for="updatedJemaat<?= $no ;?>"></label>
                      </div>
                    <?php } ?>
                  </td>
                  <td><?= $data->tempat_lahir; ?></td>
                  <td><?= ($data->tgl_lahir == null) ? "-" : date("D, d F Y", strtotime(str_replace("-","/",$data->tgl_lahir))) ?></td>
                  <td><?= ($data->lp == 'P') ? 'PEREMPUAN' : 'LAKI-LAKI'; ?></td>
                  <td class="none">
                    <table class="table table-bordered table-striped">
                      <tr>
                        <th>Alamat</th>
                        <th>Pendidikan Terakhir</th>
                        <th>Pekerjaan</th>
                        <th>No HP</th>
                        <th>Email</th>
                        <th>Diserahkan Anak</th>
                        <th>Baptis Selam</th>
                        <th>Menikah</th>
                        <th>SMK</th>
                        <th>KeBun</th>
                        <th>Hubungan KK</th>
                      </tr>
                      <tr>
                        <td><?= ($data->alamat == null) ? '-' : $data->alamat ?></td>
                        <td><?= ($data->pendidikan_terakhir == null) ? '-': $data->pendidikan_terakhir; ?></td>
                        <td><?= ($data->pekerjaan == null) ? '-': $data->pekerjaan; ?></td>
                        <td><?= ($data->nomor_hp == null) ? '-': $data->nomor_hp; ?></td>
                        <td><?= ($data->email == null) ? '-': $data->email; ?></td>
                        <td><?= ($data->diserahkan_anak == null) ? '-' : date_format(date_create($data->diserahkan_anak),"d/m/Y"); ?>
                        </td>
                        <td><?= ($data->baptis_selam == null) ? '-' : date_format(date_create($data->baptis_selam),"d/m/Y"); ?>
                        </td>
                        <td><?= ($data->nikah == null) ? '-' : date_format(date_create($data->nikah),"d/m/Y"); ?></td>
                        <td><?= ($data->smk == null) ? '-': $data->smk; ?></td>
                        <td><?= ($data->nama_komsel == null) ? '-': $data->nama_komsel; ?></td>
                        <td><?= ($data->hubungan_kk == null) ? '-': $data->hubungan_kk; ?></td>
                      </tr>
                    </table>
                  </td>
                  <td style="text-align: center;">
                    <button class="btn btn-info btn-sm" name="id_ev" style="margin-right: 15px;"
                      data-a="<?= $data->id; ?>" data-b="<?= $data->group; ?>" data-c="<?= $data->nama; ?>"
                      data-d="<?= $data->tempat_lahir; ?>" data-e="<?= $data->tgl_lahir; ?>" data-f="<?= $data->lp; ?>"
                      data-g="<?= $data->pendidikan_terakhir; ?>" data-h="<?= $data->pekerjaan; ?>"
                      data-i="<?= $data->nomor_hp; ?>" data-j="<?= $data->email; ?>"
                      data-k="<?= $data->diserahkan_anak; ?>" data-l="<?= $data->baptis_selam; ?>"
                      data-m="<?= $data->nikah; ?>" data-n="<?= $data->smk; ?>" data-o="<?= $data->nama_komsel; ?>"
                      data-p="<?= $data->hubungan_kk; ?>" data-q="<?= $data->alamat?>" data-toggle="modal" data-target="#modal-edit-jemaat"
                      data-backdrop="static" data-keyboard="false">
                      <i class="fas fa-pencil-alt">
                      </i>
                      Edit
                    </button>
                    <?php
                    if($data->status == '0'){
                      ?>
                    <button class="btn btn-danger btn-sm" data-backdrop="static" data-keyboard="false"
                      data-c="<?= $data->id; ?>" data-v="<?= $data->nama; ?>" data-toggle="modal"
                      data-target="#modal-nonaktif-jemaat">
                      <i class="fas fa-toggle-off">
                      </i>
                      Non Aktif
                    </button>
                    <?php
                    }else{
                      ?>
                    <button class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false"
                      data-c="<?= $data->id; ?>" data-v="<?= $data->nama; ?>" data-toggle="modal"
                      data-target="#modal-aktif-jemaat">
                      <i class="fas fa-toggle-on">
                      </i>
                      Aktif
                    </button>
                    <?php
                    }
                    ?>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

      </div>
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>

<div class="modal fade" id="modal-edit-community">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Edit Data KeBun</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('master/community/add_member_nongpdi');?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="form-group row">
            <label for="nama_edit_community" class="col-sm-12 col-form-label">Nama KeBun (*)</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="nama_edit_community" name="nama_edit_community" placeholder="Ketikan Nama KeBun"
                value="" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="pks_edit_community" class="col-sm-12 col-form-label">PKS (*)</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="pks_edit_community" name="pks_edit_community" placeholder="Ketikan PKS"
                value="" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="wilayah_edit_community" class="col-sm-12 col-form-label">Wilayah (*)</label>
            <div class="col-sm-12">
              <textarea rows=3 class="form-control" id="wilayah_edit_community" name="wilayah_edit_community" placeholder="Ketikan Wilayah" required></textarea>
            </div>
          </div>

          <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
          <input type="hidden" name="id_community_edit" id="id_community_edit" value="">
          <input type="hidden" class="form-group" id="fotoLama" name="fotoLama">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-aktif-community">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin aktifkan KeBun ini ?<br>
          Nama KeBun &nbsp; : <b id="nama_community_aktif"></b><br>
      </div>
      <form action="<?= site_url('master/community/aktif');?>" method="post">
      <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user;?>" />
        <input type="hidden" name="id_community_aktif" id="id_community_aktif" value="">

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger">Yes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-nonaktif-community">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin non aktifkan KeBun ini ?<br>
          Nama KeBun &nbsp; : <b id="nama_community_nonaktif"></b><br>
      </div>
      <form action="<?= site_url('master/community/nonaktif');?>" method="post">
      <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user;?>" />
        <input type="hidden" name="id_community_nonaktif" id="id_community_nonaktif" value="">

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger">Yes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-add-jemaat-gpdi">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Add Member GPDI</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('master/community/add_member_gpdi');?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="row">

            <div class="col-md-12">

              <div class="form-group">
                <label for="jemaat_add">Jemaat (*)</label>
                <div>
                  <input type="text" class="form-control" id="jemaat_add" name="jemaat_add" placeholder="Masukan Jemaat"
                    value="" required>
                </div>
              </div>

            </div>

          </div>
          
          <input type="hidden" name="id_komsel_gpdi" id="id_komsel_gpdi" value="">
          <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Add</button>
          </div>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-add-jemaat-nongpdi">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Add Member Non GPDI</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('master/community/add_member_nongpdi');?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="row">

            <div class="col-md-6">

            <div class="form-group">
                <label for="nama_nongpdi">ID Keluarga</label>
                <div>
                  <input type="text" class="form-control" id="id_keluarga_nongpdi" name="id_keluarga_nongpdi" placeholder="Masukan ID Keluarga" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="nama_nongpdi">Nama Jemaat (*)</label>
                <div>
                  <input type="text" class="form-control" id="nama_nongpdi" name="nama_nongpdi" placeholder="Masukan Nama"
                    value="" required>
                </div>
              </div>

              <div class="form-group">
                <label for="tempat_lahir_nongpdi">Tempat Lahir</label>
                <div>
                  <input type="text" class="form-control" id="tempat_lahir_nongpdi" name="tempat_lahir_nongpdi"
                    placeholder="Masukan Tempat Lahir" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="hp_nongpdi">No Hp</label>
                <div>
                  <input type="text" class="form-control" id="hp_nongpdi" name="hp_nongpdi" placeholder="Masukan Nomor HP"
                    value="" data-inputmask='"mask": "9999999999999"' data-mask>
                </div>
              </div>

              <div class="form-group">
                <label for="pendidikan_nongpdi">Pendidikan Terakhir</label>
                <div>
                  <select type="text" class="form-control" id="pendidikan_nongpdi" name="pendidikan_nongpdi"
                    placeholder="Masukan Pendidikan Terakhir" value="">
                    <option hidden selected value="">Pilih Pendidikan Terakhir</option>
                    <option value="Tidak / Belum Sekolah">Tidak / Belum Sekolah</option>
                    <option value="Tidak Tamat SD / Sederajat">Tidak Tamat SD / Sederajat</option>
                    <option value="SD / Sederajat">Tamat SD / Sederajat</option>
                    <option value="SMP / Sederajat">SMP / Sederajat</option>
                    <option value="SMA / Sederajat">SMA / Sederajat</option>
                    <option value="Diploma 1/2">Diploma 1/2</option>
                    <option value="Akademi / Diploma 3 / Sarjana Muda">Akademi / Diploma 3 / Sarjana Muda</option>
                    <option value="Diploma 4 / Strata 1">Diploma 4 / Strata 1</option>
                    <option value="Strata 2">Strata 2</option>
                    <option value="Strata 3">Strata 3</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="alamat_nongpdi">Alamat</label>
                <div>
                  <input type="text" class="form-control" id="alamat_nongpdi" name="alamat_nongpdi" value="" placeholder="Masukan alamat">
                </div>
              </div>

              <div class="form-group">
                <label for="baptis_selam_nongpdi">Dibaptis Selam</label>
                <div>
                  <input type="date" class="form-control" id="baptis_selam_nongpdi" name="baptis_selam_nongpdi"
                    placeholder="Masukan Tanggal Baptis" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="tanggal_nikah_nongpdi">Tanggal Menikah</label>
                <div>
                  <input type="date" class="form-control" id="tanggal_nikah_nongpdi" name="tanggal_nikah_nongpdi"
                    placeholder="Masukan Tanggal Menikah" value="">
                </div>
              </div>

            </div>
            
            <div class="col-md-6">

              <div class="form-group">
                <label for="hubungan_kk_nongpdi">Hubungan di Kartu Keluarga</label>
                <div>
                  <select type="text" class="form-control" id="hubungan_kk_nongpdi" name="hubungan_kk_nongpdi" value="">
                    <option hidden selected value="">Pilih Hubungan</option>
                    <option value="Kepala Keluarga">Kepala Keluarga</option>
                    <option value="Pasangan">Pasangan</option>
                    <option value="Anak">Anak</option>
                    <option value="Anak Mantu">Anak Mantu</option>
                    <option value="Orang Lain Serumah">Orang Lain Serumah</option>
                    <option value="Lain-lain">Lain-lain</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="jenis_kelamin_nongpdi">Jenis Kelamin (*)</label>
                <div>
                  <select type="text" class="form-control" id="jenis_kelamin_nongpdi" name="jenis_kelamin_nongpdi" placeholder="Masukan Jenis Kelamin" value=""  required>
                    <option selected hidden>Pilih Jenis Kelamin</option>
                    <option value="L">Laki-Laki</option>
                    <option value="P">Perempuan</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="tanggal_lahir_nongpdi">Tanggal Lahir</label>
                <div>
                  <input type="date" class="form-control" id="tanggal_lahir_nongpdi" name="tanggal_lahir_nongpdi"
                    placeholder="Masukan Tanggal Lahir" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="email_nongpdi">Email</label>
                <div>
                  <input type="text" class="form-control" id="email_nongpdi" name="email_nongpdi"
                    placeholder="Masukan Alamat Email" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="pekerjaan_nongpdi">Pekerjaan</label>
                <div>
                  <input type="text" class="form-control" id="pekerjaan_nongpdi" name="pekerjaan_nongpdi"
                    placeholder="Masukan Pekerjaan" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="kebun_nongpdi">Nama KeBun</label>
                <div>
                  <input type="text" class="form-control" id="kebun_nongpdi" name="kebun_nongpdi" placeholder="Masukan KeBun" value="" readonly>
                </div>
              </div>

              <div class="form-group">
                <label for="smk_nongpdi">Angkatan SMK</label>
                <div>
                  <input type="text" class="form-control" id="smk_nongpdi" name="smk_nongpdi" placeholder="Masukan Angkatan SMK"
                    value="">
                </div>
              </div>

              <div class="form-group">
                <label for="diserahkan_anak_nongpdi">Diserahkan Anak</label>
                <div>
                  <input type="date" class="form-control" id="diserahkan_anak_nongpdi" name="diserahkan_anak_nongpdi"
                    placeholder="Masukan Tanggal Diserahkan Anak" value="">
                </div>
              </div>

            </div>

          </div>

          <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
          <input type="hidden" name="id_jemaat_nongpdi" id="id_jemaat_nongpdi" value="">
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-edit-jemaat">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Edit Data Jemaat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('master/jemaat/save_edit');?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="row">

            <div class="col-md-6">

              <div class="form-group">
                <label for="nama_edit">ID Keluarga (*)</label>
                <div>
                  <input type="text" class="form-control" id="id_keluarga" name="id_keluarga" placeholder="Masukan ID Keluarga" value=""
                    required>
                </div>
              </div>

              <div class="form-group">
                <label for="nama_edit">Nama Jemaat (*)</label>
                <div>
                  <input type="text" class="form-control" id="nama_edit" name="nama_edit" placeholder="Masukan Nama"
                    value="" required>
                </div>
              </div>

              <div class="form-group">
                <label for="tempat_lahir_edit">Tempat Lahir (*)</label>
                <div>
                  <input type="text" class="form-control" id="tempat_lahir_edit" name="tempat_lahir_edit"
                    placeholder="Masukan Tempat Lahir" value="" required>
                </div>
              </div>

              <div class="form-group">
                <label for="hp_edit">No Hp (*)</label>
                <div>
                  <input type="text" class="form-control" id="hp_edit" name="hp_edit" placeholder="Masukan Nomor HP"
                    value="" data-inputmask='"mask": "9999999999999"' data-mask required>
                </div>
              </div>

              <div class="form-group">
                <label for="pendidikan_edit">Pendidikan Terakhir (*)</label>
                <div>
                  <select type="text" class="form-control" id="pendidikan_edit" name="pendidikan_edit"
                    placeholder="Masukan Pendidikan Terakhir" value="" required>
                    <option hidden selected value="">Pilih Pendidikan Terakhir</option>
                    <option value="Tidak / Belum Sekolah">Tidak / Belum Sekolah</option>
                    <option value="Tidak Tamat SD / Sederajat">Tidak Tamat SD / Sederajat</option>
                    <option value="SD / Sederajat">Tamat SD / Sederajat</option>
                    <option value="SMP / Sederajat">SMP / Sederajat</option>
                    <option value="SMA / Sederajat">SMA / Sederajat</option>
                    <option value="Diploma 1/2">Diploma 1/2</option>
                    <option value="Akademi / Diploma 3 / Sarjana Muda">Akademi / Diploma 3 / Sarjana Muda</option>
                    <option value="Diploma 4 / Strata 1">Diploma 4 / Strata 1</option>
                    <option value="Strata 2">Strata 2</option>
                    <option value="Strata 3">Strata 3</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="alamat_edit">Alamat (*)</label>
                <div>
                  <input type="text" class="form-control" id="alamat_edit" name="alamat_edit" value="" placeholder="Masukan Alamat" required>
                </div>
              </div>

              <div class="form-group">
                <label for="baptis_selam_edit">Dibaptis Selam</label>
                <div>
                  <input type="date" class="form-control" id="baptis_selam_edit" name="baptis_selam_edit"
                    placeholder="Masukan Tanggal Baptis" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="tanggal_nikah_edit">Tanggal Menikah</label>
                <div>
                  <input type="date" class="form-control" id="tanggal_nikah_edit" name="tanggal_nikah_edit"
                    placeholder="Masukan Tanggal Menikah" value="">
                </div>
              </div>

            </div>
            
            <div class="col-md-6">

              <div class="form-group">
                <label for="hubungan_kk_edit">Hubungan di Kartu Keluarga (*)</label>
                <div>
                  <select type="text" class="form-control" id="hubungan_kk_edit" name="hubungan_kk_edit" required>
                    <option hidden selected value="">Pilih Hubungan</option>
                    <option value="Kepala Keluarga">Kepala Keluarga</option>
                    <option value="Pasangan">Pasangan</option>
                    <option value="Anak">Anak</option>
                    <option value="Anak Mantu">Anak Mantu</option>
                    <option value="Orang Lain Serumah">Orang Lain Serumah</option>
                    <option value="Lain-lain">Lain-lain</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="jenis_kelamin_edit">Jenis Kelamin (*)</label>
                <div>
                  <select type="text" class="form-control" id="jenis_kelamin_edit" name="jenis_kelamin_edit"
                    placeholder="Masukan Jenis Kelamin" value="" required>
                    <option selected hidden>Pilih Jenis Kelamin</option>
                    <option value="L">Laki-Laki</option>
                    <option value="P">Perempuan</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="tanggal_lahir_edit">Tanggal Lahir (*)</label>
                <div>
                  <input type="date" class="form-control" id="tanggal_lahir_edit" name="tanggal_lahir_edit"
                    placeholder="Masukan Tanggal Lahir" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="email_edit">Email (*)</label>
                <div>
                  <input type="text" class="form-control" id="email_edit" name="email_edit"
                    placeholder="Masukan Alamat Email" value="" required>
                </div>
              </div>

              <div class="form-group">
                <label for="pekerjaan_edit">Pekerjaan(*)</label>
                <div>
                  <input type="text" class="form-control" id="pekerjaan_edit" name="pekerjaan_edit"
                    placeholder="Masukan Pekerjaan" value="" required>
                </div>
              </div>

              <div class="form-group">
                <label for="kebun_edit">Nama KeBun</label>
                <div>
                  <input type="text" class="form-control" id="kebun_edit" name="kebun_edit" placeholder="Masukan KeBun" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="smk_edit">Angkatan SMK</label>
                <div>
                  <input type="text" class="form-control" id="smk_edit" name="smk_edit" placeholder="Masukan Angkatan SMK"
                    value="">
                </div>
              </div>

              <div class="form-group">
                <label for="diserahkan_anak_edit">Diserahkan Anak</label>
                <div>
                  <input type="date" class="form-control" id="diserahkan_anak_edit" name="diserahkan_anak_edit"
                    placeholder="Masukan Tanggal Diserahkan Anak" value="">
                </div>
              </div>

            </div>

          </div>

          <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
          <input type="hidden" name="id_jemaat_edit" id="id_jemaat_edit" value="">
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-nonaktif-jemaat">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('master/jemaat/nonaktif');?>" method="post">
        <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user;?>" />

        <div class="card-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <p>Apakah anda yakin non aktifkan Jemaat ini ?<br>
                  Nama Jemaat &nbsp; : <b id="nama_jemaat_nonaktif"></b><br><br>
                  <label>Keterangan</label>
                  <select class="form-control" name="keterangan_nonaktif" id="keterangan_nonaktif" required>
                    <option value="">-- Pilih Keterangan --</option>
                    <option value="1">Meninggal</option>
                    <option value="2">Pindah Gereja</option>
                    <option value="4">Bukan Jemaat (Terdaftar sebagai Anggota KeBun)</option>
                    <option value="3">Lain-lain</option>
                  </select>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" name="id_jemaat_nonaktif" id="id_jemaat_nonaktif" value="">
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger">Yes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-aktif-jemaat">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin aktifkan Jemaat ini ?<br>
          Nama Jemaat &nbsp; : <b id="nama_jemaat_aktif"></b><br>
      </div>
      <form action="<?= site_url('master/jemaat/aktif');?>" method="post">
        <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user;?>" />
        <input type="hidden" name="id_jemaat_aktif" id="id_jemaat_aktif" value="">

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger">Yes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<style>
  .ui-autocomplete {
    border-radius: 0.5em;
    max-height: 200px;
    overflow-y: auto;
    overflow-x: hidden;
    padding-right: 20px;
    position: absolute;
    z-index: 99999 !important;
    padding: 0;
    margin-top: 2px;
  }

  .ui-autocomplete>li {
    padding: 3px 20px;
  }

  .ui-autocomplete>li.ui-state-focus {
    background-color: #DDD;
  }

  .ui-helper-hidden-accessible {
    display: none;
  }
</style>

<script>
  $(document).ready(function(){
    $("#backButton").on('click', function(){location.href = "<?= BASE_URL()?>master/community";});

    $(function () {
      var jemaatList = [<?php foreach($nonmember as $jemaatList){
        echo '"'.($jemaatList->nama).' - '.($jemaatList->nomor_hp).' - '.($jemaatList->alamat).'",';
      } ;?>];

      $('#modal-add-jemaat-gpdi').on('shown.bs.modal', function() {
        $("#jemaat_add").autocomplete({
          source: function(request, response) {
            var results = $.ui.autocomplete.filter(jemaatList, request.term);

            response(results.slice(0, 10));
          }
        });

        $('#id_komsel_gpdi').val($('#id_komsel').val());
      });

      $('#modal-add-jemaat-nongpdi').on('show.bs.modal', function (event) {
        $('#kebun_nongpdi').val($('#nama_community').val());
      });
    });

    $(function () {
      var kebunList = [<?php foreach($kebun as $komsel){
        echo '"'.($komsel->nama_komsel).'",';
      } ;?>];

      $('#modal-edit-jemaat').on('shown.bs.modal', function() {
        $("#kebun_edit").autocomplete({
          source: kebunList
        });
      })
    });

    $('#example1').DataTable({
      "responsive": true,
      "lengthChange": false,
      "autoWidth": false,
      "paging": true,
      "ordering": false,
      "stateSave": true,
      buttons: [{
        text: '+ Add Jemaat (GPDI)',
        action: function (e, dt, node, config) {
          $('#modal-add-jemaat-gpdi').modal('show');
        }
      },{
        text: '+ Add Jemaat (NON GPDI)',
        action: function (e, dt, node, config) {
          $('#modal-add-jemaat-nongpdi').modal('show');
        }
      }]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    function jemaatUpdate(jemaatId){

      var jemaatUpdateStatus = 0;
      var jemaatUpdateId = $('#'+jemaatId).attr('name');

      if($('#'+jemaatId).is(":checked")){
        jemaatUpdateStatus = 1;
      }
      else{
        jemaatUpdateStatus = 0;
      }

      $.ajax({
        type : "POST",
        url : "<?= site_url('master/jemaat/jemaatUpdate') ?>",
        data : {
          'updateStatus': jemaatUpdateStatus,
          'idUpdateJemaat': jemaatUpdateId
        }
      })
    }

  });

  function jemaatUpdate(jemaatId){

    var jemaatUpdateStatus = 0;
    var jemaatUpdateId = $('#'+jemaatId).attr('name');

    if($('#'+jemaatId).is(":checked")) {
      jemaatUpdateStatus = 1;
    }
    else{
      jemaatUpdateStatus = 0;
    }

    $.ajax({
      type : "POST",
      url : "<?= site_url('master/jemaat/jemaatUpdate') ?>",
      data : {
        'updateStatus': jemaatUpdateStatus,
        'idUpdateJemaat': jemaatUpdateId
      }
    });
  };

  $('#modal-edit-community').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient_a = button.data('a');
    var recipient_b = button.data('b');
    var recipient_c = button.data('c');
    var recipient_d = button.data('d');
    var recipient_e = button.data('e');

    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('.id_community_edit').val(recipient_a);
    document.getElementById("id_community_edit").value = recipient_a;
          
    modal.find('.nama_edit_community').val(recipient_b);
    document.getElementById("nama_edit_community").value = recipient_b;

    modal.find('.pks_edit_community').val(recipient_c);
    document.getElementById("pks_edit_community").value = recipient_c;

    modal.find('.wilayah_edit_community').val(recipient_e);
    document.getElementById("wilayah_edit_community").value = recipient_e;

  });

  $('#modal-aktif-community').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var recipient_c = button.data('c');

    var recipient_v = button.data('v');

    var modal = $(this);
    modal.find('.id_community_aktif').val(recipient_c);
    document.getElementById("id_community_aktif").value = recipient_c;

    document.getElementById("nama_community_aktif").innerHTML = recipient_v;
  })

  $('#modal-nonaktif-community').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var recipient_c = button.data('c');

    var recipient_v = button.data('v');

    var modal = $(this);
    modal.find('.id_community_nonaktif').val(recipient_c);
    document.getElementById("id_community_nonaktif").value = recipient_c;

    document.getElementById("nama_community_nonaktif").innerHTML = recipient_v;
  })

  $('#modal-edit-jemaat').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var recipient_a = button.data('a');
    var recipient_b = button.data('b');
    var recipient_c = button.data('c');
    var recipient_d = button.data('d');
    var recipient_e = button.data('e');
    var recipient_f = button.data('f');
    var recipient_g = button.data('g');
    var recipient_h = button.data('h');
    var recipient_i = button.data('i');
    var recipient_j = button.data('j');
    var recipient_k = button.data('k');
    var recipient_l = button.data('l');
    var recipient_m = button.data('m');
    var recipient_n = button.data('n');
    var recipient_o = button.data('o');
    var recipient_p = button.data('p');
    var recipient_q = button.data('q');

    var modal = $(this);
    document.getElementById("id_jemaat_edit").value = recipient_a;

    document.getElementById("id_keluarga").value = recipient_b;
    document.getElementById("nama_edit").value = recipient_c;

    modal.find('.hubungan_kk_edit').val(recipient_p);
    document.getElementById("hubungan_kk_edit").value = recipient_p;

    document.getElementById("tempat_lahir_edit").value = recipient_d;

    var dateX = new Date(recipient_e);
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];

    dayX = ("0" + dateX.getDate()).slice(-2);
    monthX = ("0" + (dateX.getMonth() + 1)).slice(-2);
    yearX = dateX.getFullYear();
    modal.find('.tanggal_lahir_edit').val(yearX + '-' + monthX + '-' + dayX);
    $("#tanggal_lahir_edit").val(yearX + '-' + monthX + '-' + dayX);


    if (recipient_f == "l") {
      modal.find('.jenis_kelamin_edit').val("L");
      document.getElementById("jenis_kelamin_edit").value = "L";
    } else if (recipient_f == "p") {
      modal.find('.jenis_kelamin_edit').val("P");
      document.getElementById("jenis_kelamin_edit").value = "P";
    } else {
      modal.find('.jenis_kelamin_edit').val(recipient_f);
      document.getElementById("jenis_kelamin_edit").value = recipient_f;
    }

    modal.find('.hp_edit').val(recipient_i);
    document.getElementById("hp_edit").value = recipient_i;

    modal.find('.email_edit').val(recipient_j);
    document.getElementById("email_edit").value = recipient_j;

    modal.find('.pendidikan_edit').val(recipient_g);
    document.getElementById("pendidikan_edit").value = recipient_g;

    modal.find('.pekerjaan_edit').val(recipient_h);
    document.getElementById("pekerjaan_edit").value = recipient_h;

    // -------------------------

    var dateX1 = new Date(recipient_l);
    const monthNames1 = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    dayX1 = ("0" + dateX1.getDate()).slice(-2);
    monthX1 = ("0" + (dateX1.getMonth() + 1)).slice(-2);
    yearX1 = dateX1.getFullYear();
    modal.find('.baptis_selam_edit').val(yearX1 + '-' + monthX1 + '-' + dayX1);
    $("#baptis_selam_edit").val(yearX1 + '-' + monthX1 + '-' + dayX1);


    var dateX3 = new Date(recipient_k);
    const monthNames3 = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    dayX3 = ("0" + dateX3.getDate()).slice(-2);
    monthX3 = ("0" + (dateX3.getMonth() + 1)).slice(-2);
    yearX3 = dateX3.getFullYear();
    modal.find('.diserahkan_anak_edit').val(yearX3 + '-' + monthX3 + '-' + dayX3);
    $("#diserahkan_anak_edit").val(yearX3 + '-' + monthX3 + '-' + dayX3);

    var dateX2 = new Date(recipient_m);
    const monthNames2 = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    dayX2 = ("0" + dateX2.getDate()).slice(-2);
    monthX2 = ("0" + (dateX2.getMonth() + 1)).slice(-2);
    yearX2 = dateX2.getFullYear();
    modal.find('.tanggal_nikah_edit').val(yearX2 + '-' + monthX2 + '-' + dayX2);
    $("#tanggal_nikah_edit").val(yearX2 + '-' + monthX2 + '-' + dayX2);

    modal.find('.smk_edit').val(recipient_m);
    document.getElementById("smk_edit").value = recipient_n;

    modal.find('.kebun_edit').val(recipient_n);
    document.getElementById("kebun_edit").value = recipient_o;

    modal.find('.alamat_edit').val(recipient_q);
    document.getElementById("alamat_edit").value = recipient_q;
  });

  $('#modal-nonaktif-jemaat').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var recipient_c = button.data('c');

    var recipient_v = button.data('v');

    var modal = $(this);
    modal.find('.id_jemaat_nonaktif').val(recipient_c);
    document.getElementById("id_jemaat_nonaktif").value = recipient_c;

    document.getElementById("nama_jemaat_nonaktif").innerHTML = recipient_v;
  })

  $('#modal-aktif-jemaat').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var recipient_c = button.data('c');

    var recipient_v = button.data('v');

    var modal = $(this);
    modal.find('.id_jemaat_aktif').val(recipient_c);
    document.getElementById("id_jemaat_aktif").value = recipient_c;

    document.getElementById("nama_jemaat_aktif").innerHTML = recipient_v;
  })

</script>