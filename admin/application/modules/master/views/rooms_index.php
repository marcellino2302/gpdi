<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Rooms
        </h1>
      </div><!-- /.col -->
      <div class="col-sm-6">

      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">

          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style ="width: 5%;">No</th>
                  <th>Nama Ruangan</th>
                  <th>Lokasi Ruangan</th>
                  <th style ="text-align: center;">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $no = 1;
                  foreach($grid as $data){
                  ?>
                <tr>
                  <td><?= number_format($no++,0)?></td>
                  <td><?= $data->nama_rooms; ?></td>
                  <td><?= $data->lantai; ?></td>
                  <td style="text-align: center;">
                    <button class="btn btn-info btn-sm" name="id_ev" style="margin-right: 15px;"
                      data-a="<?= $data->id_rooms; ?>"
                      data-b="<?= $data->nama_rooms; ?>"
                      data-c="<?= $data->lantai; ?>"
                      data-toggle="modal" data-target="#modal-edit-rooms" data-backdrop="static"
                      data-keyboard="false">
                      <i class="fas fa-pencil-alt">
                      </i>
                      Edit
                    </button>
                    <button class="btn btn-danger btn-sm" data-backdrop="static" data-keyboard="false"
                      data-c="<?= $data->id_rooms; ?>" 
                      data-v="<?= $data->nama_rooms; ?>"
                      data-toggle="modal" data-target="#modal-delete-rooms">
                      <i class="fas fa-trash">
                      </i>
                      Delete
                    </button>

                  </td>
                </tr>

                <?php } ?>

              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

      </div>
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>

<div class="modal fade" id="modal-delete-rooms">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin menghapus Ruangan ini ?<br>
          Nama Ruangan &nbsp; : <b id="nama_rooms_delete"></b><br>
      </div>
      <form action="<?= site_url('master/rooms/delete');?>" method="post">
      <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user;?>" />
        <input type="hidden" name="id_rooms_delete" id="id_rooms_delete" value="">

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger">Yes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal-edit-rooms">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Edit Data Ruangan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('master/rooms/save_edit');?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">
        <div class="form-group row">
            <label for="nama_edit_rooms" class="col-sm-12 col-form-label">Nama Ruangan (*)</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="nama_edit_rooms" name="nama_edit_rooms" placeholder="Masukan Nama Ruang"
                value="" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="lantai_add_rooms" class="col-sm-12 col-form-label">Lokasi Ruangan (*)</label>
            <div class="col-sm-12">
              <select type="text" class="form-control" id="lantai_edit_rooms" name="lantai_edit_rooms"
                value="" required><option value="" selected hidden>Masukan Lokasi Ruangan</option><option value="Luar Gereja">Luar Gereja</option><option value="GSG">GSG</option><option value="Basement">Basement</option><option value="1">1</option><option value="2">2</option><option value="2A">2A</option><option value="2B">2B</option></select>
            </div>
          </div>
          
          <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
          <input type="hidden" name="id_rooms_edit" id="id_rooms_edit" value="">
          <input type="hidden" class="form-group" id="fotoLama" name="fotoLama">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-add">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel2">Tambahkan Data Ruangan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('master/rooms/save')?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">
        <div class="form-group row">
            <label for="nama_add_rooms" class="col-sm-12 col-form-label">Nama Ruangan (*)</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="nama_add_rooms" name="nama_add_rooms" placeholder="Masukan Nama Ruang"
                value="" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="lantai_add_rooms" class="col-sm-12 col-form-label">Lokasi Ruangan (*)</label>
            <div class="col-sm-12">
              <select type="text" class="form-control" id="lantai_add_rooms" name="lantai_add_rooms" placeholder="Masukan Lantai"
                value="" required><option value="" selected hidden>Masukan Lokasi Ruangan</option><option value="Luar Gereja">Luar Gereja</option><option value="GSG">GSG</option><option value="Basement">Basement</option><option value="1">1</option><option value="2">2</option><option value="2A">2A</option><option value="2B">2B</option></select>
            </div>
          </div>

          <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
</div>
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "lengthChange": false,
      "autoWidth": false,
      "paging": true,
      // "sorting": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print"],
      buttons: [
          {
              text: '+ Add Room',
              action: function ( e, dt, node, config ) {
                  $('#modal-add').modal({backdrop: 'static', keyboard: false});
              }
          }
      ]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    });

  $('#modal-delete-rooms').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient_c = button.data('c');
    var recipient_v = button.data('v');

    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('.id_rooms_delete').val(recipient_c);
    document.getElementById("id_rooms_delete").value = recipient_c;

    document.getElementById("nama_rooms_delete").innerHTML = recipient_v;
  });

  $('#modal-edit-rooms').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient_a = button.data('a');
    var recipient_b = button.data('b');
    var recipient_c = button.data('c');

    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('.id_rooms_edit').val(recipient_a);
    document.getElementById("id_rooms_edit").value = recipient_a;

    modal.find('.nama_edit_rooms').val(recipient_b);
    document.getElementById("nama_edit_rooms").value = recipient_b;

    modal.find('.lantai_edit_rooms').val(recipient_c);
    document.getElementById("lantai_edit_rooms").value = recipient_c;

  });

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', {
      'placeholder': 'dd/mm/yyyy'
    })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('dd/mm/yyyy', {
      'placeholder': 'dd/mm/yyyy'
    })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate2').datetimepicker({
      format: 'DD-MMMM-yyyy'
    });
    //Date range picker
    $('#reservation2').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime2').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'DD/MM/YYYY'
      }
    })

    //Date range picker
    $('#reservationdate').datetimepicker({
      format: 'DD-MMMM-yyyy'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'DD/MM/YYYY'
      }
    })

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'DD/MM/YYYY'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()
  })

  $('#modal-add').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var modal = $(this)
  })

</script>