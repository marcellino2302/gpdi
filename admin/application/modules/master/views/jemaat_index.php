<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Jemaat
        </h1>
      </div><!-- /.col -->
      <div class="col-sm-6">

      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">

          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th></th>
                  <th style="width: 5%;">No</th>
                  <th>Keluarga</th>
                  <th>Nama Jemaat</th>
                  <th style="text-align: center;">Updated</th>
                  <th>Tempat Lahir</th>
                  <th>Tanggal Lahir</th>
                  <th>Jenis Kelamin</th>
                  <th class="none"></th>
                  <th style="text-align: center;">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $no = 1;
                  foreach($grid as $data){
                  ?>
                <tr <?php if ($data->status=='4'){
                  echo 'class="table-secondary"';
                }else if(($data->status!='0') && ($data->status!='4')){
                  echo 'class="table-danger"';
                }?>>
                  <td></td>
                  <td><?= number_format($no++,0)?></td>
                  <td><?= $data->group; ?></td>
                  <td><?= $data->nama; ?></td>
                  <td style="text-align: center;">
                    <?php if($data->updated == '1'){ ?>
                      <div class="icheck-primary d-inline">
                        <input type="checkbox" id="updatedJemaat<?= $no ;?>" onchange="jemaatUpdate(this.id);" name="<?= $data->id ;?>" checked>
                        <label for="updatedJemaat<?= $no ;?>"></label>
                      </div>
                    <?php } else { ?> 
                      <div class="icheck-primary d-inline">
                        <input type="checkbox" id="updatedJemaat<?= $no ;?>" onchange="jemaatUpdate(this.id);" name="<?= $data->id ;?>">
                        <label for="updatedJemaat<?= $no ;?>"></label>
                      </div>
                    <?php } ?>
                  </td>
                  <td><?= $data->tempat_lahir; ?></td>
                  <td><?= ($data->tgl_lahir == null) ? "-" : date("D, d F Y", strtotime(str_replace("-","/",$data->tgl_lahir))) ?></td>
                  <td><?= ($data->lp == 'P') ? 'PEREMPUAN' : 'LAKI-LAKI'; ?></td>
                  <td class="none">
                    <table class="table table-bordered table-striped">
                      <tr>
                        <th>Alamat</th>
                        <th>Pendidikan Terakhir</th>
                        <th>Pekerjaan</th>
                        <th>No HP</th>
                        <th>Email</th>
                        <th>Diserahkan Anak</th>
                        <th>Baptis Selam</th>
                        <th>Menikah</th>
                        <th>SMK</th>
                        <th>KeBun</th>
                        <th>Hubungan KK</th>
                      </tr>
                      <tr>
                        <td><?= ($data->alamat == null) ? '-' : $data->alamat ?></td>
                        <td><?= ($data->pendidikan_terakhir == null) ? '-': $data->pendidikan_terakhir; ?></td>
                        <td><?= ($data->pekerjaan == null) ? '-': $data->pekerjaan; ?></td>
                        <td><?= ($data->nomor_hp == null) ? '-': $data->nomor_hp; ?></td>
                        <td><?= ($data->email == null) ? '-': $data->email; ?></td>
                        <td><?= ($data->diserahkan_anak == null) ? '-' : date_format(date_create($data->diserahkan_anak),"d/m/Y"); ?>
                        </td>
                        <td><?= ($data->baptis_selam == null) ? '-' : date_format(date_create($data->baptis_selam),"d/m/Y"); ?>
                        </td>
                        <td><?= ($data->nikah == null) ? '-' : date_format(date_create($data->nikah),"d/m/Y"); ?></td>
                        <td><?= ($data->smk == null) ? '-': $data->smk; ?></td>
                        <td><?= ($data->nama_komsel == null) ? '-': $data->nama_komsel; ?></td>
                        <td><?= ($data->hubungan_kk == null) ? '-': $data->hubungan_kk; ?></td>
                      </tr>
                    </table>
                  </td>
                  <td style="text-align: center;">
                    <button class="btn btn-info btn-sm" name="id_ev" style="margin-right: 15px;"
                      data-a="<?= $data->id; ?>" data-b="<?= $data->group; ?>" data-c="<?= $data->nama; ?>"
                      data-d="<?= $data->tempat_lahir; ?>" data-e="<?= $data->tgl_lahir; ?>" data-f="<?= $data->lp; ?>"
                      data-g="<?= $data->pendidikan_terakhir; ?>" data-h="<?= $data->pekerjaan; ?>"
                      data-i="<?= $data->nomor_hp; ?>" data-j="<?= $data->email; ?>"
                      data-k="<?= $data->diserahkan_anak; ?>" data-l="<?= $data->baptis_selam; ?>"
                      data-m="<?= $data->nikah; ?>" data-n="<?= $data->smk; ?>" data-o="<?= $data->nama_komsel; ?>"
                      data-p="<?= $data->hubungan_kk; ?>" data-q="<?= $data->alamat?>" data-toggle="modal" data-target="#modal-edit-jemaat"
                      data-backdrop="static" data-keyboard="false">
                      <i class="fas fa-pencil-alt">
                      </i>
                      Edit
                    </button>
                    <?php
                    if($data->status == '0'){
                      ?>
                    <button class="btn btn-danger btn-sm" data-backdrop="static" data-keyboard="false"
                      data-c="<?= $data->id; ?>" data-v="<?= $data->nama; ?>" data-toggle="modal"
                      data-target="#modal-nonaktif-jemaat">
                      <i class="fas fa-toggle-off">
                      </i>
                      Non Aktif
                    </button>
                    <?php
                    }else{
                      ?>
                    <button class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false"
                      data-c="<?= $data->id; ?>" data-v="<?= $data->nama; ?>" data-toggle="modal"
                      data-target="#modal-aktif-jemaat">
                      <i class="fas fa-toggle-on">
                      </i>
                      Aktif
                    </button>
                    <?php
                    }
                    ?>
                    <!-- <button class="btn btn-danger btn-sm" data-backdrop="static" data-keyboard="false"
                      data-c="<?= $data->id; ?>" 
                      data-v="<?= $data->nama; ?>"
                      data-toggle="modal" data-target="#modal-delete-jemaat">
                      <i class="fas fa-trash">
                      </i>
                      Delete
                    </button> -->

                  </td>
                </tr>

                <?php } ?>

              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

      </div>
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>

<div class="modal fade" id="modal-delete-jemaat">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin menghapus Jemaat ini ?<br>
          Nama Jemaat &nbsp; : <b id="nama_jemaat_delete"></b><br>
      </div>
      <form action="<?= site_url('master/jemaat/delete');?>" method="post">
        <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user;?>" />
        <input type="hidden" name="id_jemaat_delete" id="id_jemaat_delete" value="">

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger">Yes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-aktif-jemaat">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin aktifkan Jemaat ini ?<br>
          Nama Jemaat &nbsp; : <b id="nama_jemaat_aktif"></b><br>
      </div>
      <form action="<?= site_url('master/jemaat/aktif');?>" method="post">
        <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user;?>" />
        <input type="hidden" name="id_jemaat_aktif" id="id_jemaat_aktif" value="">

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger">Yes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-nonaktif-jemaat">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('master/jemaat/nonaktif');?>" method="post">
        <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user;?>" />

        <div class="card-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <p>Apakah anda yakin non aktifkan Jemaat ini ?<br>
                  Nama Jemaat &nbsp; : <b id="nama_jemaat_nonaktif"></b><br><br>
                  <label>Keterangan</label>
                  <select class="form-control" name="keterangan_nonaktif" id="keterangan_nonaktif" required>
                    <option value="">-- Pilih Keterangan --</option>
                    <option value="1">Meninggal</option>
                    <option value="2">Pindah Gereja</option>
                    <option value="4">Bukan Jemaat (Terdaftar sebagai Anggota KeBun)</option>
                    <option value="3">Lain-lain</option>
                  </select>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" name="id_jemaat_nonaktif" id="id_jemaat_nonaktif" value="">
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger">Yes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal-edit-jemaat">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Edit Data Jemaat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('master/jemaat/save_edit');?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="row">

            <div class="col-md-6">

              <div class="form-group">
                <label for="nama_edit">ID Keluarga (*)</label>
                <div>
                  <input type="text" class="form-control" id="id_keluarga" name="id_keluarga" placeholder="" value=""
                    required>
                </div>
              </div>

              <div class="form-group">
                <label for="nama_edit">Nama Jemaat (*)</label>
                <div>
                  <input type="text" class="form-control" id="nama_edit" name="nama_edit" placeholder="Masukan Nama"
                    value="" required>
                </div>
              </div>

              <div class="form-group">
                <label for="tempat_lahir_edit">Tempat Lahir (*)</label>
                <div>
                  <input type="text" class="form-control" id="tempat_lahir_edit" name="tempat_lahir_edit"
                    placeholder="Masukan Tempat Lahir" value="" required>
                </div>
              </div>

              <div class="form-group">
                <label for="hp_edit">No Hp (*)</label>
                <div>
                  <input type="text" class="form-control" id="hp_edit" name="hp_edit" placeholder="Masukan Nomor HP"
                    value="" data-inputmask='"mask": "9999999999999"' data-mask required>
                </div>
              </div>

              <div class="form-group">
                <label for="pendidikan_edit">Pendidikan Terakhir (*)</label>
                <div>
                  <select type="text" class="form-control" id="pendidikan_edit" name="pendidikan_edit"
                    placeholder="Masukan Pendidikan Terakhir" value="" required>
                    <option hidden selected>Pilih Pendidikan Terakhir</option>
                    <option value="Tidak / Belum Sekolah">Tidak / Belum Sekolah</option>
                    <option value="Tidak Tamat SD / Sederajat">Tidak Tamat SD / Sederajat</option>
                    <option value="SD / Sederajat">Tamat SD / Sederajat</option>
                    <option value="SMP / Sederajat">SMP / Sederajat</option>
                    <option value="SMA / Sederajat">SMA / Sederajat</option>
                    <option value="Diploma 1/2">Diploma 1/2</option>
                    <option value="Akademi / Diploma 3 / Sarjana Muda">Akademi / Diploma 3 / Sarjana Muda</option>
                    <option value="Diploma 4 / Strata 1">Diploma 4 / Strata 1</option>
                    <option value="Strata 2">Strata 2</option>
                    <option value="Strata 3">Strata 3</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="alamat_edit">Alamat (*)</label>
                <div>
                  <input type="text" class="form-control" id="alamat_edit" name="alamat_edit" value="" required>
                </div>
              </div>

              <div class="form-group">
                <label for="baptis_selam_edit">Dibaptis Selam</label>
                <div>
                  <input type="date" class="form-control" id="baptis_selam_edit" name="baptis_selam_edit"
                    placeholder="Masukan Tanggal Baptis" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="tanggal_nikah_edit">Tanggal Menikah</label>
                <div>
                  <input type="date" class="form-control" id="tanggal_nikah_edit" name="tanggal_nikah_edit"
                    placeholder="Masukan Tanggal Menikah" value="">
                </div>
              </div>

            </div>
            
            <div class="col-md-6">

              <div class="form-group">
                <label for="hubungan_kk_edit">Hubungan di Kartu Keluarga (*)</label>
                <div>
                  <select type="text" class="form-control" id="hubungan_kk_edit" name="hubungan_kk_edit" value="" required>
                    <option hidden selected>Pilih Hubungan</option>
                    <option value="Kepala Keluarga">Kepala Keluarga</option>
                    <option value="Pasangan">Pasangan</option>
                    <option value="Anak">Anak</option>
                    <option value="Anak Mantu">Anak Mantu</option>
                    <option value="Orang Lain Serumah">Orang Lain Serumah</option>
                    <option value="Lain-lain">Lain-lain</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="jenis_kelamin_edit">Jenis Kelamin (*)</label>
                <div>
                  <select type="text" class="form-control" id="jenis_kelamin_edit" name="jenis_kelamin_edit"
                    placeholder="Masukan Jenis Kelamin" value="" required>
                    <option selected hidden>Pilih Jenis Kelamin</option>
                    <option value="L">Laki-Laki</option>
                    <option value="P">Perempuan</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="tanggal_lahir_edit">Tanggal Lahir (*)</label>
                <div>
                  <input type="date" class="form-control" id="tanggal_lahir_edit" name="tanggal_lahir_edit"
                    placeholder="Masukan Tanggal Lahir" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="email_edit">Email (*)</label>
                <div>
                  <input type="text" class="form-control" id="email_edit" name="email_edit"
                    placeholder="Masukan Alamat Email" value="" required>
                </div>
              </div>

              <div class="form-group">
                <label for="pekerjaan_edit">Pekerjaan(*)</label>
                <div>
                  <input type="text" class="form-control" id="pekerjaan_edit" name="pekerjaan_edit"
                    placeholder="Masukan Pekerjaan" value="" required>
                </div>
              </div>

              <div class="form-group">
                <label for="kebun_edit">Nama KeBun</label>
                <div>
                  <input type="text" class="form-control" id="kebun_edit" name="kebun_edit" placeholder="Masukan KeBun" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="smk_edit">Angkatan SMK</label>
                <div>
                  <input type="text" class="form-control" id="smk_edit" name="smk_edit" placeholder="Masukan Angkatan SMK"
                    value="">
                </div>
              </div>

              <div class="form-group">
                <label for="diserahkan_anak_edit">Diserahkan Anak</label>
                <div>
                  <input type="date" class="form-control" id="diserahkan_anak_edit" name="diserahkan_anak_edit"
                    placeholder="Masukan Tanggal Diserahkan Anak" value="">
                </div>
              </div>

            </div>

          </div>

          <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
          <input type="hidden" name="id_jemaat_edit" id="id_jemaat_edit" value="">
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<style>
  tr.group,
  tr.group:hover {
    background-color: #b5dcff !important;
  }

  .ui-autocomplete {
    border-radius: 0.5em;
    max-height: 200px;
    overflow-y: auto;
    overflow-x: hidden;
    padding-right: 20px;
    position: absolute;
    z-index: 99999 !important;
    padding: 0;
    margin-top: 2px;
  }

  .ui-autocomplete>li {
    padding: 3px 20px;
  }

  .ui-autocomplete>li.ui-state-focus {
    background-color: #DDD;
  }

  .ui-helper-hidden-accessible {
    display: none;
  }

</style>
<script>
  $(document).ready(function () {

    $(function () {
      var kebunList = [<?php foreach($kebun as $komsel){
        echo '"'.($komsel->nama_komsel).'",';
      } ;?>];

      $('#modal-edit-jemaat').on('shown.bs.modal', function() {

        $("#kebun_edit").autocomplete({
          source: kebunList
        });
      })
    });

    var groupColumn = 2;
    var table = $('#example1').DataTable({
      columnDefs: [{
        visible: false,
        targets: groupColumn
      }],
      order: [groupColumn, 'asc'],
      drawCallback: function (settings) {
        var api = this.api();
        var rows = api.rows({
          page: 'current'
        }).nodes();
        var last = null;

        api
          .column(groupColumn, {
            page: 'current'
          })
          .data()
          .each(function (group, i) {
            if (group == "" || last !== group) {
              $(rows)
                .eq(i)
                .before('<tr class="group"><td colspan="9">ID KELUARGA : ' + group + '</td></tr>');
              last = group;
            }
          });
      },
      "responsive": true,
      "lengthChange": false,
      "autoWidth": false,
      "paging": true,
      "ordering": false,
      "stateSave": true,
      buttons: [{
        text: '+ Add Jemaat',
        action: function (e, dt, node, config) {
          location.href = "<?= base_url(); ?>" + "master/jemaat/addJemaat";
        }
      }]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    $('#example tbody').on('click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if (currentOrder[0] === groupColumn && currentOrder[1] === 'asc') {
        table.order([groupColumn, 'desc']).draw();
      } else {
        table.order([groupColumn, 'asc']).draw();
      }
    });
  });

  // $(function () {
  //   $("#example1").DataTable({
  //     "responsive": true,
  //     "lengthChange": false,
  //     "autoWidth": false,
  //     "paging": true,
  //     // "sorting": false,
  //     // "buttons": ["copy", "csv", "excel", "pdf", "print"],
  //     buttons: [
  //         {
  //             text: '+ edit Jemaat',
  //             action: function ( e, dt, node, config ) {
  //                 $('#modal-edit').modal('show');
  //             }
  //         }
  //     ]
  //   }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  //   });
  
  function jemaatUpdate(jemaatId){

    var jemaatUpdateStatus = 0;
    var jemaatUpdateId = $('#'+jemaatId).attr('name');

    if($('#'+jemaatId).is(":checked")) {
      jemaatUpdateStatus = 1;
    }
    else{
      jemaatUpdateStatus = 0;
    }

    $.ajax({
      type : "POST",
      url : "<?= site_url('master/jemaat/jemaatUpdate') ?>",
      data : {
        'updateStatus': jemaatUpdateStatus,
        'idUpdateJemaat': jemaatUpdateId
      }
    })
  }

  $('#modal-delete-jemaat').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient_c = button.data('c');

    var recipient_v = button.data('v');

    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('.id_jemaat_delete').val(recipient_c);
    document.getElementById("id_jemaat_delete").value = recipient_c;

    document.getElementById("nama_jemaat_delete").innerHTML = recipient_v;
  })

  $('#modal-aktif-jemaat').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient_c = button.data('c');

    var recipient_v = button.data('v');

    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('.id_jemaat_aktif').val(recipient_c);
    document.getElementById("id_jemaat_aktif").value = recipient_c;

    document.getElementById("nama_jemaat_aktif").innerHTML = recipient_v;
  })

  $('#modal-nonaktif-jemaat').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient_c = button.data('c');

    var recipient_v = button.data('v');

    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('.id_jemaat_nonaktif').val(recipient_c);
    document.getElementById("id_jemaat_nonaktif").value = recipient_c;

    document.getElementById("nama_jemaat_nonaktif").innerHTML = recipient_v;
  })

  $('#modal-edit-jemaat').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient_a = button.data('a');
    var recipient_b = button.data('b');
    var recipient_c = button.data('c');
    var recipient_d = button.data('d');
    var recipient_e = button.data('e');
    var recipient_f = button.data('f');
    var recipient_g = button.data('g');
    var recipient_h = button.data('h');
    var recipient_i = button.data('i');
    var recipient_j = button.data('j');
    var recipient_k = button.data('k');
    var recipient_l = button.data('l');
    var recipient_m = button.data('m');
    var recipient_n = button.data('n');
    var recipient_o = button.data('o');
    var recipient_p = button.data('p');
    var recipient_q = button.data('q');
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    document.getElementById("id_jemaat_edit").value = recipient_a;

    document.getElementById("id_keluarga").value = recipient_b;
    document.getElementById("nama_edit").value = recipient_c;

    modal.find('.hubungan_kk_edit').val(recipient_p);
    document.getElementById("hubungan_kk_edit").value = recipient_p;

    document.getElementById("tempat_lahir_edit").value = recipient_d;

    var dateX = new Date(recipient_e);
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];

    dayX = ("0" + dateX.getDate()).slice(-2);
    monthX = ("0" + (dateX.getMonth() + 1)).slice(-2);
    yearX = dateX.getFullYear();
    modal.find('.tanggal_lahir_edit').val(yearX + '-' + monthX + '-' + dayX);
    $("#tanggal_lahir_edit").val(yearX + '-' + monthX + '-' + dayX);


    if (recipient_f == "l") {
      modal.find('.jenis_kelamin_edit').val("L");
      document.getElementById("jenis_kelamin_edit").value = "L";
    } else if (recipient_f == "p") {
      modal.find('.jenis_kelamin_edit').val("P");
      document.getElementById("jenis_kelamin_edit").value = "P";
    } else {
      modal.find('.jenis_kelamin_edit').val(recipient_f);
      document.getElementById("jenis_kelamin_edit").value = recipient_f;
    }

    modal.find('.hp_edit').val(recipient_i);
    document.getElementById("hp_edit").value = recipient_i;

    modal.find('.email_edit').val(recipient_j);
    document.getElementById("email_edit").value = recipient_j;

    modal.find('.pendidikan_edit').val(recipient_g);
    document.getElementById("pendidikan_edit").value = recipient_g;

    modal.find('.pekerjaan_edit').val(recipient_h);
    document.getElementById("pekerjaan_edit").value = recipient_h;

    // -------------------------

    var dateX1 = new Date(recipient_l);
    const monthNames1 = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    dayX1 = ("0" + dateX1.getDate()).slice(-2);
    monthX1 = ("0" + (dateX1.getMonth() + 1)).slice(-2);
    yearX1 = dateX1.getFullYear();
    modal.find('.baptis_selam_edit').val(yearX1 + '-' + monthX1 + '-' + dayX1);
    $("#baptis_selam_edit").val(yearX1 + '-' + monthX1 + '-' + dayX1);


    var dateX3 = new Date(recipient_k);
    const monthNames3 = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    dayX3 = ("0" + dateX3.getDate()).slice(-2);
    monthX3 = ("0" + (dateX3.getMonth() + 1)).slice(-2);
    yearX3 = dateX3.getFullYear();
    modal.find('.diserahkan_anak_edit').val(yearX3 + '-' + monthX3 + '-' + dayX3);
    $("#diserahkan_anak_edit").val(yearX3 + '-' + monthX3 + '-' + dayX3);

    var dateX2 = new Date(recipient_m);
    const monthNames2 = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    dayX2 = ("0" + dateX2.getDate()).slice(-2);
    monthX2 = ("0" + (dateX2.getMonth() + 1)).slice(-2);
    yearX2 = dateX2.getFullYear();
    modal.find('.tanggal_nikah_edit').val(yearX2 + '-' + monthX2 + '-' + dayX2);
    $("#tanggal_nikah_edit").val(yearX2 + '-' + monthX2 + '-' + dayX2);

    modal.find('.smk_edit').val(recipient_m);
    document.getElementById("smk_edit").value = recipient_n;

    modal.find('.kebun_edit').val(recipient_n);
    document.getElementById("kebun_edit").value = recipient_o;

    modal.find('.alamat_edit').val(recipient_q);
    document.getElementById("alamat_edit").value = recipient_q;
  })

  $('#modal-edit').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var modal = $(this)
  })

</script>