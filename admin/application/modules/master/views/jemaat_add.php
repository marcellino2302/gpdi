<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Jemaat
        </h1>
      </div><!-- /.col -->
      <div class="col-sm-6">

      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-5">
        <div class="card">

          <!-- /.card-header -->
          <div class="card-body">
            <button class="btn btn-primary" id="backButton" style="margin-bottom : 0.5em;"><i class="fa fa-angle-left" aria-hidden="true"></i>  Kembali</button>

            <div class="card card-primary">

              <div class="card-header">
                <h3 class="card-title">Add Jemaat</h3>
              </div>

              <form action="<?= site_url('master/jemaat/save')?>" method="post" enctype="multipart/form-data">

                <div class="card-body">

                  <div class="form-group row">
                    <label for="nama_add" class="col-sm-5 col-form-label">ID Keluarga (*)</label>
                    <div class="col-sm">
                      <input type="text" class="form-control" id="id_keluarga" name="id_keluarga" placeholder="ID Keluarga terakhir : <?php foreach($max_group as $group){echo $group->group;} ?>" value="" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="nama_add" class="col-sm-5 col-form-label">Nama Jemaat (*)</label>
                    <div class="col-sm">
                      <input type="text" class="form-control" id="nama_add" name="nama_add" placeholder="Masukan Nama" value="" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="hubungan_kk_add" class="col-sm-5 col-form-label">Hubungan di Kartu Keluarga (*)</label>
                    <div class="col-sm">
                      <select type="text" class="form-control" id="hubungan_kk_add" name="hubungan_kk_add" value="" required>
                        <option hidden selected>Pilih Hubungan</option>
                        <option value="Kepala Keluarga">Kepala Keluarga</option>
                        <option value="Pasangan">Pasangan</option>
                        <option value="Anak">Anak</option>
                        <option value="Anak Mantu">Anak Mantu</option>
                        <option value="Orang Lain Serumah">Orang Lain Serumah</option>
                        <option value="Lain-lain">Lain-lain</option>
                      </select>
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="tempat_lahir_add" class="col-sm-5 col-form-label">Tempat Lahir (*)</label>
                    <div class="col-sm">
                      <input type="text" class="form-control" id="tempat_lahir_add" name="tempat_lahir_add" placeholder="Masukan Tempat Lahir" value="" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="tanggal_nikah_add" class="col-sm-5 col-form-label">Tanggal Lahir (*)</label>
                    <div class="col-sm">
                      <input type="date" class="form-control" id="tanggal_lahir_add" name="tanggal_lahir_add" placeholder="Masukan Tanggal Lahir" value="">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="jenis_kelamin_add" class="col-sm-5 col-form-label">Jenis Kelamin (*)</label>
                    <div class="col-sm">
                      <select type="text" class="form-control" id="jenis_kelamin_add" name="jenis_kelamin_add" placeholder="Masukan Jenis Kelamin" value="" required><option selected hidden>Pilih Jenis Kelamin</option><option value="L">Laki-Laki</option><option value="P">Perempuan</option></select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="alamat_add" class="col-sm-5 col-from-label">Alamat (*)</label>
                    <div class="col-sm">
                      <input type="text" class="form-control" id="alamat_add" name="alamat_add" placeholder="Masukan Alamat" value="" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="hp_add" class="col-sm-5 col-form-label">No Hp (*)</label>
                    <div class="col-sm">
                      <input type="text" class="form-control" id="hp_add" name="hp_add" placeholder="Masukan Nomor HP"
                        value="" data-inputmask='"mask": "9999999999999"' data-mask required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="hp_add" class="col-sm-5 col-form-label">Email (*)</label>
                    <div class="col-sm">
                      <input type="text" class="form-control" id="email_add" name="email_add" placeholder="Masukan Alamat Email" value="" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="pendidikan_add" class="col-sm-5 col-form-label">Pendidikan Terakhir (*)</label>
                    <div class="col-sm">
                      <select type="text" class="form-control" id="pendidikan_add" name="pendidikan_add" placeholder="Masukan Pendidikan Terakhir" value="" required>
                      <option hidden selected>Pilih Pendidikan Terakhir</option>
                      <option value="Tidak / Belum Sekolah">Tidak / Belum Sekolah</option>
                      <option value="Tidak Tamat SD / Sederajat">Tidak Tamat SD / Sederajat</option>
                      <option value="SD / Sederajat">Tamat SD / Sederajat</option>
                      <option value="SMP / Sederajat">SMP / Sederajat</option>
                      <option value="SMA / Sederajat">SMA / Sederajat</option>
                      <option value="Diploma 1/2">Diploma 1/2</option>
                      <option value="Akademi / Diploma 3 / Sarjana Muda">Akademi / Diploma 3 / Sarjana Muda</option>
                      <option value="Diploma 4 / Strata 1">Diploma 4 / Strata 1</option>
                      <option value="Strata 2">Strata 2</option>
                      <option value="Strata 3">Strata 3</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="pekerjaan_add" class="col-sm-5 col-form-label">Pekerjaan(*)</label>
                    <div class="col-sm">
                      <input type="text" class="form-control" id="pekerjaan_add" name="pekerjaan_add" placeholder="Masukan Pekerjaan" value="" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="baptis_selam_add" class="col-sm-5 col-form-label">Dibaptis Selam</label>
                    <div class="col-sm">
                      <input type="date" class="form-control" id="baptis_selam_add" name="baptis_selam_add" placeholder="Masukan Tanggal Baptis" value="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="smk_add" class="col-sm-5 col-form-label">Angkatan SMK</label>
                    <div class="col-sm">
                      <input type="text" class="form-control" id="smk_add" name="smk_add" placeholder="Masukan Angkatan SMK" value="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="kebun_add" class="col-sm-5 col-form-label">Nama KeBun</label>
                    <div class="col-sm">
                      <input type="text" class="form-control" id="kebun_add" name="kebun_add" placeholder="Masukan KeBun" value="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="tanggal_nikah_add" class="col-sm-5 col-form-label">Tanggal Menikah</label>
                    <div class="col-sm">
                      <input type="date" class="form-control" id="tanggal_nikah_add" name="tanggal_nikah_add" placeholder="Masukan Tanggal Menikah" value="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="diserahkan_anak_add" class="col-sm-5 col-form-label">Diserahkan Anak</label>
                    <div class="col-sm">
                      <input type="date" class="form-control" id="diserahkan_anak_add" name="diserahkan_anak_add" placeholder="Masukan Tanggal Diserahkan Anak" value="">
                    </div>
                  </div>

                  <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
                </div>

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>

              </form>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>

      <div class="col-md">
        <div class="card">
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style ="width: 5%;">No</th>
                  <th>Keluarga</th>
                  <th>Nama Jemaat</th>
                  <th>Hubungan Keluarga</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $no = 1;
                  foreach($grid as $data){
                  ?>
                <tr>
                  <td><?= number_format($no++,0)?></td>
                  <td><?= $data->group; ?></td>
                  <td><?= $data->nama; ?></td>
                  <td><?= $data->hubungan_kk; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>

<style>
  .form-group{
    padding : 0.2em;
    transition: 0.5s;
    border-radius: 0.6em;
  }

  .form-control{
    border-radius: 0.6em;
  }

  .form-group:hover, .form-control:focus + .form-group{
      background-color: #b5dcff !important;
      
  }
  
  tr.group,
  tr.group:hover {
      background-color: #b5dcff !important;
  }

  .ui-autocomplete {
      border-radius: 0.5em;
      max-height: 200px;
      overflow-y: auto;
      /* prevent horizontal scrollbar */
      overflow-x: hidden;
      /* add padding to account for vertical scrollbar */
      padding-right: 20px;
  } 

</style>

<script>

  $(document).ready(function() {
    $("#backButton").on('click', function(){location.href = "<?= BASE_URL()?>master/jemaat/index";});

    var groupColumn = 1;
    var table = $('#example1').DataTable({
        columnDefs: [{ visible: false, targets: groupColumn }],
        order : [groupColumn, 'asc'],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({ page: 'current' }).nodes();
            var last = null;

            api
                .column(groupColumn, { page: 'current' })
                .data()
                .each(function (group, i) {
                    if(group == "" || last !== group) {
                      $(rows)
                        .eq(i)
                        .before('<tr class="group"><td colspan="9">ID KELUARGA : ' + group + '</td></tr>');
                      last = group;
                    }
                });
        },
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        "paging": true,
        "ordering" : false,
    });

    var idCheck = <?php foreach($max_group as $group){echo $group->group;} ?>;
    $.fn.dataTableExt.afnFiltering.push(
      function(oSettings, aData, iDataIndex) {
        if (typeof aData._id == 'undefined') {
          aData._id = aData[1];
        }

        if (idCheck == '') {
          return true
        }

        if (idCheck == aData._id) {
          return true;
        }
        else {
          return false;
        }
      }
    );

    $('#id_keluarga').on('keyup', function () {
        idCheck = $("#id_keluarga").val();
        table.draw();
    });

  });

  $(function () {
    var kebunList = [<?php foreach($kebun as $komsel){
      echo '"'.($komsel->nama_komsel).'",';
    } ;?>];

    $('#kebun_add').autocomplete({
      source : kebunList
    });
  });

  $('#modal-add').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var modal = $(this)
  })

</script>