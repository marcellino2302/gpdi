<style>
    .preview-container {
      display: flex;
      flex-wrap: wrap;
    }
    .preview-image {
      width: 200px;
      height: 200px;
      object-fit: cover;
      margin: 10px;
    }
    .image-preview {
      width: 400px !important;
      height: 400px !important;
      object-fit: cover !important;
      object-position: center !important;
    }
</style>
<script src="//cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.1/tinymce.min.js"></script>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Data LWG
        </h1>
      </div><!-- /.col -->
      <div class="col-sm-6">

      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">

          <!-- /.card-header -->
          <div class="card-body">
          <form id="frm" method="post" action="<?= site_url('master/Lwg/update');?>" class="needs-validation" novalidation>
              <div class="row">
                <div class="col-4">
                    <img id="preview" src="<?= base_url('image/lwg');?>/<?= $grid[0]->data_value ?>" alt="Image" class="image-preview" />
                </div>

                <div class="col-8">
                    <div class="form-group">
                        <label>Banner (1920x433)</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="banner" name="banner" placeholder="Enter Banner">
                                <label class="custom-file-label" for="banner">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-4">
                    <img id="preview-logo" src="<?= base_url('image/lwg');?>/<?= $grid[9]->data_value ?>" alt="Image" class="image-preview" />
                </div>

                <div class="col-8">
                    <div class="form-group">
                        <label>Logo 1:1</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="logo" name="logo" placeholder="Enter Logo">
                                <label class="custom-file-label" for="logo">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <br>
                      <label>Title</label>
                      <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="<?= $grid[1]->data_value ?>" required>
                  </div>
              </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        
                        <label>Introduction</label>
                        <div id="description" name="description" required><?= $grid[2]->data_value ?></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        
                        <label>Description</label>
                        <div id="keterangan" name="keterangan" required><?= $grid[8]->data_value ?></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        
                        <label>Gallery</label>
                    </div>
                </div>
            </div>

            <div class="row">
                
                <!--Image container -->
                <div class="row"
                  data-type="imagesloader"
                  data-errorformat="Accepted file formats"
                  data-errorsize="Maximum size accepted"
                  data-errorduplicate="File already loaded"
                  data-errormaxfiles="Maximum number of images you can upload"
                  data-errorminfiles="Minimum number of images to upload"
                  data-modifyimagetext="Modify immage">

                  <!-- Progress bar -->
                  <div class="col-12 order-1 mt-2">
                    <div data-type="progress" class="progress" style="width:400px; display:none;">
                      <div data-type="progressBar" class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 100%;">Load in progress...</div>
                    </div>
                  </div>
                  <!-- Model -->
                  <div data-type="image-model" class="" style=" display:none;  width: 250px; height: 250px; margin: 0 10px 60px 10px;">
                    <div class="ratio-box text-center" data-type="image-ratio-box">
                      <img data-type="noimage" class="btn btn-light ratio-img img-fluid p-2 image border dashed rounded" src="<?php echo base_url('/assets/jquery-imageuploader/photo-camera-gray.svg'); ?>" style="cursor:pointer; width: 250px; height: 250px;">
                      <div data-type="loading" class="img-loading" style="color:#218838; display:none;">
                        <span class="fa fa-2x fa-spin fa-spinner"></span>
                      </div>
                      <img data-type="preview" class="btn btn-light ratio-img img-fluid p-2 image border dashed rounded" src="" style="display: none; cursor: default; width: 250px; height: 250px;">
                    </div>
                    <!-- Buttons -->
                    <div data-type="image-buttons" class="row justify-content-center mt-2">
                      <button data-type="add" class="btn btn-outline-secondary" type="button"><span class="fa fa-camera mr-2"></span>Add</button>
                      <button data-type="btn-modify" type="button" class="btn btn-outline-secondary m-0" data-toggle="popover" data-placement="right" style="display:none;">
                      <span class="fa fa-pencil-alt mr-2"></span>Modify
                      </button>
                    </div>
                  </div>
                  <!-- Popover operations -->
                  <div data-type="popover-model" style="display:none">
                    <div data-type="popover" class="ml-3 mr-3" style="min-width:150px;">
                      <div class="row">
                        <div class="col p-0">
                          <button data-operation="main" class="btn btn-block btn-secondary btn-sm rounded-pill" type="button"><span class="fa fa-angle-double-up mr-2"></span>Main</button>
                        </div>
                      </div>
                      <div class="row mt-2">
                        <div class="col-6 p-0 pr-1">
                          <button data-operation="left" class="btn btn-block btn-outline-secondary btn-sm rounded-pill" type="button"><span class="fa fa-angle-left mr-2"></span>Left</button>
                        </div>
                        <div class="col-6 p-0 pl-1">
                          <button data-operation="right" class="btn btn-block btn-outline-success btn-sm rounded-pill" type="button">Right<span class="fa fa-angle-right ml-2"></span></button>
                        </div>
                      </div>
                      <div class="row mt-2">
                        <div class="col-6 p-0 pr-1">
                          <button data-operation="rotateanticlockwise" class="btn btn-block btn-outline-success btn-sm rounded-pill" type="button"><span class="fas fa-undo-alt mr-2"></span>Rotate</button>
                        </div>
                        <div class="col-6 p-0 pl-1">
                          <button data-operation="rotateclockwise" class="btn btn-block btn-outline-success btn-sm rounded-pill" type="button">Rotate<span class="fas fa-redo-alt ml-2"></span></button>
                        </div>
                      </div>
                      <div class="row mt-2">
                        <button data-operation="remove" class="btn btn-outline-danger btn-sm btn-block" type="button"><span class="fa fa-times mr-2"></span>Remove</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="input-group">
                    <!--Hidden file input for images-->
                    <input id="files" type="file" name="files[]" data-button="" multiple="" accept="image/jpeg, image/png, image/gif," style="display:none;">
                  </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Youtube Link <i class="fab fa-youtube"></i></label>
                        <input type="link" class="form-control" id="ytlink" name="ytlink" placeholder="Enter Youtube Link" value="<?= $grid[4]->data_value ?>">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Instagram Link <i class="fab fa-instagram"></i></label>
                        <input type="link" class="form-control" id="iglink" name="iglink" placeholder="Enter Instagram Link" value="<?= $grid[5]->data_value ?>">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Facebook Link <i class="fab fa-facebook"></i></label>
                        <input type="link" class="form-control" id="fblink" name="fblink" placeholder="Enter Facebook Link" value="<?= $grid[6]->data_value ?>">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Twitter Link <i class="fab fa-twitter"></i></label>
                        <input type="link" class="form-control" id="twlink" name="twlink" placeholder="Enter Twitter Link" value="<?= $grid[7]->data_value ?>">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" id="submit" name="submit" value="SUBMIT">
                        <input type="reset" class="btn btn-danger" id="reset" name="reset" value="RESET">
                    </div>
                </div>

            </div>
          </form>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

      </div>
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>

<script>
    let formData = new FormData();

    function previewImage() {
      const fileInput = document.getElementById('banner');
      const previewImage = document.getElementById('preview');

      fileInput.addEventListener('change', function() {
        let file = this.files[0];
        let reader = new FileReader();

        reader.addEventListener('load', function() {
          previewImage.src = reader.result;
        }, false);

        if (file) {
          reader.readAsDataURL(file);
          formData.append('banner', file); // Append "banner" file to FormData
        }
      }, false);

      const fileInputLogo = document.getElementById('logo');
      const previewImageLogo = document.getElementById('preview-logo');

      fileInputLogo.addEventListener('change', function() {
        let file = this.files[0];
        let reader = new FileReader();

        reader.addEventListener('load', function() {
          previewImageLogo.src = reader.result;
        }, false);

        if (file) {
          reader.readAsDataURL(file);
          formData.append('logo', file); // Append "banner" file to FormData
        }
      }, false);
    }

    previewImage();

    $(document).ready(function() {
        $('#description').summernote({
                height: 250
        });

        $('#keterangan').summernote({
                height: 750
        });

        var imagesloader = $('[data-type=imagesloader]').imagesloader({

        // animation speed
        fadeTime: 'slow', 

        // saved image
        imagesToLoad: [
          <?php $gallery_row = explode(',', $grid[3]->data_value);
            $i=0;
            foreach($gallery_row as $row){
              ?>
              {"Url":"<?= base_url('image/lwg');?>/<?= $row ?>","Name":"image<?=$i++?>"},
              <?php
            }
          ?>
        ],

        // input ID
        inputID: 'files', 

        // maximum number of files
        maxfiles: 20,

        // max image bytes
        maxSize: 5000 * 1024,

        // min image count
        minSelect: 1,

        // allowed file types
        filesType: ["image/jpeg", "image/png", "image/gif"],

        // max/min height
        maxWidth: 1280,
        maxHeight: 1024,

        // image type
        imgType: "image/jpeg",

        // image quality from 0 to 1
        imgQuality: .9,

        // error messages
        errorformat: "Accepted format",
        errorsize: "Max size allowed",
        errorduplicate: "File already uploaded",
        errormaxfiles: "Max images you can upload",
        errorminfiles: "Minimum number of images to upload",

        // text for modify image button
        modifyimagetext: "Modify image",

        // angle of each rotation
        rotation: 90

        });

        $frm = $('#frm');
        $frm.submit(function (e) {
  e.preventDefault();
  e.stopPropagation();

  let $form = $(this);
  let files = imagesloader.data('format.imagesloader').AttachmentArray;
  let il = imagesloader.data('format.imagesloader');

  // Append each file to the FormData
  files.forEach((file, index) => {
    // Create a new Blob object for each file using the Base64 data
    let blob = b64toBlob(file.Base64, file.AttachmentType);
    formData.append(`file_${index}`, blob, file.FileName);
    console.log(file.FileName);
  });

  let banner = $('#banner').val();
  formData.append('banner', banner);

  let logo = $('#logo').val();
  formData.append('logo', logo);

  let title = $('#title').val();
  formData.append('title', title);

  let description = $('#description').summernote('code');
  formData.append('description', description);

  const keterangan = $('#keterangan').summernote('code');
  formData.append('keterangan', keterangan);

  let ytlink = $('#ytlink').val();
  formData.append('ytlink', ytlink);

  let iglink = $('#iglink').val();
  formData.append('iglink', iglink);

  let fblink = $('#fblink').val();
  formData.append('fblink', fblink);

  let twlink = $('#twlink').val();
  formData.append('twlink', twlink);

  let additionalData = {
    // Include any additional form data if required
  };

  // Append additional form data to the FormData
  for (let key in additionalData) {
    formData.append(key, additionalData[key]);
  }

  console.log(formData);

  $.ajax({
    type: $form.attr('method'),
    url: $form.attr('action'),
    data: formData,
    contentType: false, // Set content type to false for the browser to detect it
    processData: false, // Set processData to false to prevent jQuery from processing data
    success: function (response) {
      Swal.fire({
        icon: 'success',
        title: 'Success',
        text: 'Data has been updated!',
        confirmButtonColor: '#218838'
      })
    },
    error: function (error) {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: error,
        confirmButtonColor: '#c6313a'
      })
    },
  });
});


// Helper function to convert Base64 to Blob with correct MIME type based on file extension
function b64toBlob(b64Data, sliceSize = 512) {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);

    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  // Get the file extension from the Base64 data (assumed to be after "data:image/")
  const contentType = b64Data.substring("data:image/".length, b64Data.indexOf(";base64"));

  const blob = new Blob(byteArrays, { type: `image/${contentType}` });
  return blob;
}


    });
  </script>