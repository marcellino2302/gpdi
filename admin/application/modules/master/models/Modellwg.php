<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Modellwg extends CI_Model {

	public function get_index(){
		$result = $this->db->query("SELECT * FROM wb_lwg")->result();

		return $result;
	}

	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}	
}