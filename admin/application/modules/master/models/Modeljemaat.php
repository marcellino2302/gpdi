<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Modeljemaat extends CI_Model {

	public function get_index(){
		//build query index
		$result = $this->db->query("SELECT jmt.*, kmsl.nama_komsel FROM `wb_jemaat_fix` jmt LEFT JOIN `wb_komsel` kmsl ON kmsl.id=jmt.komsel WHERE jmt.deleted_at IS NULL ORDER BY jmt.group ASC")->result();
		
		// order_by('ID2','asc')->get_where('wb_jemaat',array('deleted_at'=>NULL,'ID2 <> '=>'30'))->result();
		//return query result
		return $result;
	}

	public function get_max_group(){
		$result = $this->db->query("SELECT MAX(wb_jemaat_fix.group) AS `group` FROM wb_jemaat_fix")->result();

		return $result;
	}

	public function get_kebun(){
		$result = $this->db->query("SELECT * FROM `wb_komsel`")->result();

		return $result;
	}

	public function check_jemaat($group){
		$result = $this->db->query("SELECT * FROM wb_jemaat_fix WHERE `group`=$group")->result();

		return $result;
	}

	public function jemaat_update($jemaatUpdateStatus, $idUpdateJemaat){

		$result = $this->db->query("UPDATE `wb_jemaat_fix` SET `updated`=$jemaatUpdateStatus WHERE `id` =$idUpdateJemaat;");

		return $result;
	}

	public function simpan($data){

		$namaKebun = $data['kebun'];
		$result = $this->db->query("SELECT `id` FROM wb_komsel WHERE `nama_komsel`= '$namaKebun'")->result();

		$komsel = 0;
		foreach($result as $idKomsel){
			$komsel = $idKomsel->id;
		}

		//array data
		$array = array(
			'group'					=> $data['group'],
			'komsel'				=> $komsel,
			'nama' 					=> $data['nama'],
			'tempat_lahir' 			=> $data['tempat_lahir'],
			'tgl_lahir'				=> $data['tanggal_lahir'],
			'lp' 					=> $data['lp'],
			'alamat'				=> $data['alamat'],
			'pendidikan_terakhir' 	=> $data['pendidikan_terakhir'],
			'pekerjaan' 			=> $data['pekerjaan'],
			'nomor_hp' 				=> str_replace("_", "", $data['nomor_hp']),
			'email' 				=> $data['email'],
			'diserahkan_anak' 		=> $data['diserahkan_anak'],
			'baptis_selam' 			=> $data['baptis_selam'],
			'nikah' 				=> $data['nikah'],
			'smk' 					=> $data['smk'],
			'kebun' 				=> $data['kebun'],
			'hubungan_kk' 			=> $data['hubungan_kk']
		);
		//insert
		return $this->db->insert('wb_jemaat_fix', $array);
	}

	public function simpan_edit($data){

		$namaKebun = $data['kebun'];
		$result = $this->db->query("SELECT `id` FROM wb_komsel WHERE `nama_komsel`= '$namaKebun'")->result();

		$komsel = 0;
		foreach($result as $idKomsel){
			$komsel = $idKomsel->id;
		}

		//array data
		$array = array(
			'group'					=> $data['group'],
			'komsel'				=> $komsel,
			'nama' 					=> $data['nama'],
			'tempat_lahir' 			=> $data['tempat_lahir'],
			'tgl_lahir'				=> $data['tanggal_lahir'],
			'lp' 					=> $data['lp'],
			'alamat'				=> $data['alamat'],
			'pendidikan_terakhir' 	=> $data['pendidikan_terakhir'],
			'pekerjaan' 			=> $data['pekerjaan'],
			'nomor_hp' 				=> str_replace("_", "", $data['nomor_hp']),
			'email' 				=> $data['email'],
			'diserahkan_anak' 		=> $data['diserahkan_anak'],
			'baptis_selam' 			=> $data['baptis_selam'],
			'nikah' 				=> $data['nikah'],
			'smk' 					=> $data['smk'],
			'kebun' 				=> $data['kebun'],
			'hubungan_kk' 			=> $data['hubungan_kk'],
			'update_at' 			=> date("Y-m-d h:i:s")	,
			'update_by' 			=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id', $data['id']);
		//update	
		return $this->db->update('wb_jemaat_fix');
	}

	public function hapus($data){
		//array data
		$array = array(
			'deleted_at' 		=> date("Y-m-d h:i:s")	,
			'deleted_by' 		=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id', $data['id']);
		//update
		return $this->db->update('wb_jemaat_fix');
	}

	public function aktif($data){
		//array data
		$array = array(
			'status' 			=> '0',
			'update_at' 		=> date("Y-m-d h:i:s"),
			'update_by' 		=> $data['id_user']
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id', $data['id']);
		//update
		return $this->db->update('wb_jemaat_fix');
	}

	public function nonaktif($data){
		//array data
		$array = array(
			'status' 			=> $data['status'],
			'update_at' 		=> date("Y-m-d h:i:s"),
			'update_by' 		=> $data['id_user']
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id', $data['id']);
		//update
		return $this->db->update('wb_jemaat_fix');
	}
}