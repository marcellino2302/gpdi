<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelcommunity extends CI_Model {

	public function get_index(){
		$result = $this->db->query("SELECT kmsl.*, jmt.nama, jmt.nomor_hp FROM `wb_komsel` kmsl LEFT JOIN `wb_jemaat_fix` jmt ON kmsl.id_pks=jmt.id WHERE kmsl.`deleted_at` IS NULL ORDER BY kmsl.`id` ASC")->result();

		return $result;
	}

	public function get_jemaat(){
		$result = $this->db->query("SELECT `nama`, `nomor_hp`, `alamat` FROM `wb_jemaat_fix`")->result();

		return $result;
	}

	public function get_komsel($idKomsel){
		$result = $this->db->query("SELECT kmsl.*, jmt.nama, jmt.nomor_hp FROM `wb_komsel` kmsl LEFT JOIN `wb_jemaat_fix` jmt ON kmsl.id_pks=jmt.id WHERE kmsl.`deleted_at` IS NULL AND kmsl.`id`=$idKomsel ORDER BY kmsl.`id` ASC")->result();

		return $result;
	}

	public function get_member($idKomsel){
		$result = $this->db->query("SELECT jmt.*, kmsl.nama_komsel FROM `wb_jemaat_fix` jmt LEFT JOIN `wb_komsel` kmsl ON kmsl.id=jmt.komsel WHERE jmt.deleted_at IS NULL AND `komsel`=$idKomsel ORDER BY jmt.group ASC")->result();

		return $result;
	}

	public function get_nonmember(){
		$result = $this->db->query("SELECT * FROM `wb_jemaat_fix` WHERE `komsel` IS NULL")->result();

		return $result;
	}

	public function add_gpdi($data){
		$array = array(
			'komsel' 		=> $data['id_komsel'],
			'update_by'		=> $data['id_user'],
			'update_at'		=> date("Y-m-d h:i:s")
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('nama', $data['nama']);
		$this->db->where('nomor_hp', $data['nomor'] == "" ? null : $data['nomor']);
		$this->db->where('alamat', $data['alamat'] == "" ? null : $data['alamat']);
		//update	
		return $this->db->update('wb_jemaat_fix');
	}

	public function add_nongpdi($data){

		$namaKebun = $data['kebun'];
		$result = $this->db->query("SELECT `id` FROM wb_komsel WHERE `nama_komsel`= '$namaKebun'")->result();

		$komsel = 0;
		foreach($result as $idKomsel){
			$komsel = $idKomsel->id;
		}

		//array data
		$array = array(
			'group'					=> $data['group']=="" ? null : $data['group'],
			'komsel'				=> $komsel,
			'nama' 					=> $data['nama'] =="" ? null : $data['nama'],
			'tempat_lahir' 			=> $data['tempat_lahir'] =="" ? null : $data['tempat_lahir'],
			'tgl_lahir'				=> $data['tanggal_lahir'] =="" ? null : $data['tanggal_lahir'],
			'lp' 					=> $data['lp'] =="" ? null : $data['lp'],
			'alamat'				=> $data['alamat'] =="" ? null : $data['alamat'],
			'pendidikan_terakhir' 	=> $data['pendidikan_terakhir'] =="" ? null : $data['pendidikan_terakhir'],
			'pekerjaan' 			=> $data['pekerjaan'] =="" ? null : $data['pekerjaan'],
			'nomor_hp' 				=> str_replace("_", "", $data['nomor_hp'])  =="" ? null : str_replace("_", "", $data['nomor_hp']),
			'email' 				=> $data['email'] =="" ? null : $data['email'],
			'diserahkan_anak' 		=> $data['diserahkan_anak'] =="" ? null : $data['diserahkan_anak'],
			'baptis_selam' 			=> $data['baptis_selam'] =="" ? null : $data['baptis_selam'],
			'nikah' 				=> $data['nikah'] =="" ? null : $data['nikah'],
			'smk' 					=> $data['smk'] =="" ? null : $data['smk'],
			'kebun' 				=> $data['kebun'] =="" ? null : $data['kebun'],
			'hubungan_kk' 			=> $data['hubungan_kk'] =="" ? null : $data['hubungan_kk'],
			'status'				=> "4",
		);
		//insert
		return $this->db->insert('wb_jemaat_fix', $array);
	}

	public function simpan($data){

		$namaJemaat = explode(" - ",$data['pks'])[0];
		$result = $this->db->query("SELECT `id` FROM `wb_jemaat_fix` WHERE `nama`= '$namaJemaat'")->result();

		$pks = 0;
		foreach($result as $idJemaat){
			$pks = $idJemaat->id;
		}

		//array data
		$array = array(
			'nama_komsel' 		=> strtoupper($data['nama']),
			'id_pks'			=> $pks,
			'pks' 				=> $namaJemaat,
			'wilayah' 			=> $data['wilayah'],
			'created_at' 		=> date("Y-m-d h:i:s"),
			'created_by' 		=> $data['id_user']
		);
		//insert
		return $this->db->insert('wb_komsel', $array);
	}

	public function simpan_edit($data){

		$namaJemaat = explode(" - ",$data['pks'])[0];
		$result = $this->db->query("SELECT `id` FROM `wb_jemaat_fix` WHERE `nama`= '$namaJemaat'")->result();

		$pks = 0;
		foreach($result as $idJemaat){
			$pks = $idJemaat->id;
		}

		//array data
		$array = array(
			'nama_komsel' 		=> strtoupper($data['nama']),
			'id_pks'			=> $pks,
			'pks' 				=> $namaJemaat,
			'wilayah' 			=> $data['wilayah'],
			'updated_at' 		=> date("Y-m-d h:i:s"),
			'updated_by' 		=> $data['id_user']
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id', $data['id']);
		//update	
		return $this->db->update('wb_komsel');
	}

	public function hapus($data){
		//array data
		$array = array(
			'deleted_at' 		=> date("Y-m-d h:i:s")	,
			'deleted_by' 		=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id', $data['id']);
		//update
		return $this->db->update('wb_komsel');
	}

	public function aktif($data){
		//array data
		$array = array(
			'status' 			=> '0',
			'updated_at' 		=> date("Y-m-d h:i:s"),
			'updated_by' 		=> $data['id_user']
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id', $data['id']);
		//update
		return $this->db->update('wb_komsel');
	}

	public function nonaktif($data){
		//array data
		$array = array(
			'status' 			=> '1',
			'updated_at' 		=> date("Y-m-d h:i:s"),
			'updated_by' 		=> $data['id_user']
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id', $data['id']);
		//update
		return $this->db->update('wb_komsel');
	}
}