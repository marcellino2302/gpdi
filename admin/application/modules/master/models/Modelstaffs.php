<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelstaffs extends CI_Model {

	public function get_index(){
		//build query index
		$result = $this->db->order_by('id_staffs','asc')
						    ->get_where('wb_staffs',array('deleted_at'=>NULL))
						    ->result();
		//return query result
		return $result;
	}

	public function simpan($data){
		//array data
		$array = array(
			'nama_staffs' 		=> $data['nama'],
			'photos_staffs'	=> $data['id_user']."-".str_replace(" ","",$data['foto']),
			'created_at' 		=> date("Y-m-d h:i:s"),
			'created_by' 		=> $data['id_user'],
		);
		//insert
		return $this->db->insert('wb_staffs', $array);
	}

	public function simpan_edit($data){
		//array data
		$array = array(
			'nama_staffs' 		=> $data['nama'],
			'photos_staffs' 	=> $data['foto'],
			'updated_at' 		=> date("Y-m-d h:i:s"),
			'updated_by' 		=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_staffs', $data['id']);
		//update	
		return $this->db->update('wb_staffs');
	}

	public function hapus($data){
		//array data
		$array = array(
			'deleted_at' 		=> date("Y-m-d h:i:s")	,
			'deleted_by' 		=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_staffs', $data['id']);
		//update
		return $this->db->update('wb_staffs');
	}
}