<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelrooms extends CI_Model {

	public function get_index(){
		//build query index
		$result = $this->db->order_by('id_rooms','asc')
						    ->get_where('ad_rooms',array('deleted_at'=>NULL))
						    ->result();
		//return query result
		return $result;
	}

	public function simpan($data){
		//array data
		$array = array(
			'nama_rooms' 		=> $data['room'],
			'lantai'			=> $data['lantai'],
			'created_at' 		=> date("Y-m-d h:i:s"),
			'created_by' 		=> $data['id_user'],
		);
		//insert
		return $this->db->insert('ad_rooms', $array);
	}

	public function simpan_edit($data){
		//array data
		$array = array(
			'nama_rooms' 		=> $data['room'],
			'lantai'			=> $data['lantai'],
			'updated_by' 		=> date("Y-m-d h:i:s"),
			'updated_by' 		=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_rooms', $data['id']);
		//update	
		return $this->db->update('ad_rooms');
	}

	public function hapus($data){
		//array data
		$array = array(
			'deleted_at' 		=> date("Y-m-d h:i:s")	,
			'deleted_by' 		=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_rooms', $data['id']);
		//update
		return $this->db->update('ad_rooms');
	}
}