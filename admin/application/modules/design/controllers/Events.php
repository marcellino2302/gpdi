<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Events extends MX_Controller {
	
	public	 function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modelevents');
	}

    public function index(){
		//load view with get data
		$this->page->view('events_index', array (
			'grid'		=> $this->Modelevents->get_index(),
		));
    }

	public function save(){
		//array data from input post
		$data['nama'] 		= $this->input->post('nama_add_event');
		$data['halaman'] 	= $this->input->post('halaman_add_event');
		$data['foto'] 		= $_FILES['foto_user2']['name'];
		$data['id_user'] 	= $this->input->post('id_user');

		//save data
		$this->Modelevents->simpan($data);

		//upload image
		$config = array(
			'file_name' => $this->input->post('id_user')."-".str_replace(" ","",$_FILES['foto_user2']['name']),
			'upload_path' => "./image/events/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf|xls|xlsx",
			'overwrite' => TRUE,
		);
		$this->load->library('upload', $config);
		$this->upload->do_upload('foto_user2');

		//redirect page
		redirect($this->agent->referrer());
    }

	public function save_edit(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_event_edit');
		$data['nama'] 		= $this->input->post('nama_edit_event');
		$data['halaman'] 	= $this->input->post('halaman_edit_event');
		$data['id_user'] 	= $this->input->post('id_user');
		if($_FILES['foto_user']['name'] == '' ||  $_FILES['foto_user']['name'] == null){
			$data['foto'] 		= $this->input->post('fotoLama');
			$foto 				= $this->input->post('fotoLama');
		}else{
			$data['foto'] 		= $this->input->post('id_user')."-".str_replace(" ","",$_FILES['foto_user']['name']);
			$foto				= $this->input->post('id_user')."-".str_replace(" ","",$_FILES['foto_user']['name']);
		}

		//update data
		$this->Modelevents->simpan_edit($data);

		//upload image
		$config = array(
			'file_name' => $foto,
			'upload_path' => "./image/events/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf|xls|xlsx",
			'overwrite' => TRUE,
		);
		$this->load->library('upload', $config);
		if($_FILES['foto_user']['name'] == '' ||  $_FILES['foto_user']['name'] == null){
		}else{
			$this->upload->do_upload('foto_user');
		}

		//redirect page
		redirect($this->agent->referrer());
    }

	public function delete(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_event_delete');
		$data['nama'] 		= $this->input->post('nama_event_delete');
		$data['id_user'] 	= $this->input->post('id_user');

		//delete data
		$this->Modelevents->hapus($data);

		//redirect page
		redirect($this->agent->referrer());
    }
}