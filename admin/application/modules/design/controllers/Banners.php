<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Banners extends MX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modelbanners');
	}

    public function index(){
		//load view with get data
		$this->page->view('banners_index', array (
			'grid'		=> $this->Modelbanners->get_index(),
		));
    }

	public function save(){
		//array data from input post
		$data['nama'] 		= $this->input->post('nama_add_banner');
		$data['halaman'] 	= $this->input->post('halaman_add_banner');
		$data['foto'] 		= $_FILES['foto_user2']['name'];
		$data['id_user'] 	= $this->input->post('id_user');

		//save data
		$this->Modelbanners->simpan($data);

		//upload image
		$config = array(
			'file_name' => $this->input->post('id_user')."-".str_replace(" ","",$_FILES['foto_user2']['name']),
			'upload_path' => "./image/banners/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf|xls|xlsx",
			'overwrite' => TRUE,
		);
		$this->load->library('upload', $config);
		$this->upload->do_upload('foto_user2');

		//redirect page
		redirect($this->agent->referrer());
    }

	public function save_edit(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_banner_edit');
		$data['nama'] 		= $this->input->post('nama_banner');
		$data['halaman'] 	= $this->input->post('halaman_banner');
		$data['id_user'] 	= $this->input->post('id_user');
		if($_FILES['foto_user']['name'] == '' ||  $_FILES['foto_user']['name'] == null){
			$data['foto'] 		= $this->input->post('fotoLama');
			$foto 				= $this->input->post('fotoLama');
		}else{
			$data['foto'] 		= $this->input->post('id_user')."-".str_replace(" ","",$_FILES['foto_user']['name']);
			$foto				= $this->input->post('id_user')."-".str_replace(" ","",$_FILES['foto_user']['name']);
		}

		//update data
		$this->Modelbanners->simpan_edit($data);

		//upload image
		$config = array(
			'file_name' => $foto,
			'upload_path' => "./image/banners/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf|xls|xlsx",
			'overwrite' => TRUE,
		);
		$this->load->library('upload', $config);
		if($_FILES['foto_user']['name'] == '' ||  $_FILES['foto_user']['name'] == null){
		}else{
			$this->upload->do_upload('foto_user');
		}

		//redirect page
		redirect($this->agent->referrer());
    }

	public function delete(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_banner_delete');
		$data['nama'] 		= $this->input->post('nama_banner_delete');
		$data['id_user'] 	= $this->input->post('id_user');

		//delete data
		$this->Modelbanners->hapus($data);

		//redirect page
		redirect($this->agent->referrer());
    }
}