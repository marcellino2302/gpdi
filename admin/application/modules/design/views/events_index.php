 <!-- Content Header (Page header) -->
  <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Events
              </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">

            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style ="text-align: center;">Photos</th>
                        <th class="none"></th>
                        <th style ="width: 5%;">No</th>
                        <th style ="width: 60%;">Nama Event</th>
                        <th>Halaman Event</th>
                        <th style ="text-align: center;">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $no = 1;
                        foreach($grid as $data){
                        ?>
                        <tr>
                            <td></td>
                            <td class="none"><img class="shadow" style="width: 300px; border: 1px solid black;"
                                src="<?= base_url('image/events'); ?>/<?php if($data->photos_event!=""){echo $data->photos_event;}else{echo "0.jpg";} ?>"
                                alt="your image" /></td>
                            <td><?= number_format($no++,0)?></td>
                            <td><?= $data->event_name; ?></td>
                            <td><?= $data->halaman_event; ?></td>
                            <td style="text-align: center;">
                            <button class="btn btn-info btn-sm" name="id_ev" style="margin-right: 15px;"
                                data-a="<?= $data->id_wb_event; ?>"
                                data-b="<?= $data->photos_event; ?>"
                                data-c="<?= $data->event_name; ?>"
                                data-d="<?= $data->halaman_event; ?>"
                                data-toggle="modal" data-target="#modal-edit-event" data-backdrop="static"
                                data-keyboard="false">
                                <i class="fas fa-pencil-alt">
                                </i>
                                Edit
                            </button>
                            <button class="btn btn-danger btn-sm" data-backdrop="static" data-keyboard="false"
                                data-c="<?= $data->id_wb_event; ?>" 
                                data-v="<?= $data->event_name; ?>"
                                data-toggle="modal" data-target="#modal-delete-events">
                                <i class="fas fa-trash">
                                </i>
                                Delete
                            </button>

                            </td>
                        </tr>

                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
          </div>
          <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->

  <div class="modal fade" id="modal-delete-events">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Apakah anda yakin menghapus Event ini ?<br>
            Nama Event &nbsp; : <b id="nama_edit_event_delete"></b><br>
        </div>
        <form action="<?= site_url('/design/events/delete'); ?>" method="post">
        <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
          <input type="hidden" name="id_event_delete" id="id_event_delete" value="">

          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-danger">Yes</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->


  <div class="modal fade" id="modal-edit-event">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">Edit Data Event</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?= site_url('/design/events/save_edit'); ?>" method="post" enctype="multipart/form-data">
          <div class="modal-body">
            <div class="form-group row">
              <label for="nama_edit_event" class="col-sm-12 col-form-label">Nama Event (*)</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="nama_edit_event" name="nama_edit_event" placeholder="Ketikan Nama Events"
                  value="" required>
              </div>
            </div>
            
            <div class="form-group row">
              <label for="halaman_edit_event" class="col-sm-12 col-form-label">Halaman Event (*)</label>
              <div class="col-sm-12">
                <input type="number" class="form-control" id="halaman_edit_event" name="halaman_edit_event" placeholder="Ketikan Halaman Events"
                  value="" required>
              </div>
            </div>
            
            <div class="form-group">
              <label for="foto_user">Foto Event (*)</label>
              <input class="form-control" type="file" accept="image/png, image/gif, image/jpeg"  id="foto_user" name="foto_user" >
              <label for="foto_user"><img id="foto_up"
                  style="width: 200px; border: 1px solid black; margin-top: 30px; padding: 10px;"
                  src="<?= base_url('image/events'); ?>/0.jpg" alt="your image" /></label>
            </div>
            

            <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
            <input type="hidden" name="id_event_edit" id="id_event_edit" value="">
            <input type="hidden" class="form-group" id="fotoLama" name="fotoLama">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <div class="modal fade" id="modal-add">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel2">Tambahkan Data event</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?= site_url('/design/events/save'); ?>" method="post" enctype="multipart/form-data">
          <div class="modal-body">
            <div class="form-group row">
              <label for="nama_add_event" class="col-sm-12 col-form-label">Nama Event (*)</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="nama_add_event" name="nama_add_event"
                  placeholder="Ketikan Nama Events" value="" required>
              </div>
            </div>

            <div class="form-group row">
              <label for="halaman_add_event" class="col-sm-12 col-form-label">Halaman Event (*)</label>
              <div class="col-sm-12">
                <input type="number" class="form-control" id="halaman_add_event" name="halaman_add_event"
                  placeholder="Ketikan Halaman Events" value="" required>
              </div>
            </div>

            <div class="form-group">
              <label for="foto_user2">Foto Event (*)</label>
              <input class="form-control" type="file" accept="image/png, image/gif, image/jpeg"  id="foto_user2" name="foto_user2" required>
              <label for="foto_user2"><img id="foto_up2"
                  style="width: 200px; border: 1px solid black; margin-top: 30px; padding: 10px;"
                  src="<?= base_url('image/events'); ?>/0.jpg" alt="your image" /></label>
            </div>
            

            <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
  </div>

  <script>
  $(document).ready(function(){
    var $modal = $('#modal');
    var image = document.getElementById('foto_up2');
    var cropper;

    $('#upload_image').change(function(event){
        var files = event.target.files;
        var done = function (url) {
            image.src = url;
            $modal.modal('show');
        };

        if (files && files.length > 0)
        {
              reader = new FileReader();
              reader.onload = function (event) {
                  done(reader.result);
              };
              reader.readAsDataURL(files[0]);
        }
    });

    $modal.on('shown.bs.modal', function() {
        cropper = new Cropper(image, {
          aspectRatio: 1,
          viewMode: 3,
          preview: '.preview'
        });
    }).on('hidden.bs.modal', function() {
        cropper.destroy();
        cropper = null;
    });

    $("#crop").click(function(){
        canvas = cropper.getCroppedCanvas({
            width: 400,
            height: 400,
        });

        canvas.toBlob(function(blob) {
            //url = URL.createObjectURL(blob);
            var reader = new FileReader();
            reader.readAsDataURL(blob); 
            reader.onloadend = function() {
                var base64data = reader.result;  
              
            }
        });
      });

    });

  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#foto_up').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
  }

  $("#foto_user").change(function () {
      readURL(this);
  });

  function readURL1(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#foto_up2').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
  }

  $("#foto_user2").change(function () {
      readURL1(this);
  });

  $(function () {
      $("#example1").DataTable({
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        "paging": false,
        // "sorting": false,
        // "buttons": ["copy", "csv", "excel", "pdf", "print"],
        buttons: [
                {
                    text: '+ Add Event',
                    action: function ( e, dt, node, config ) {
                        $('#modal-add').modal({backdrop: 'static', keyboard: false});
                    }
                }
            ]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

  });

  $('#modal-delete-events').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var recipient_c = button.data('c');

      var recipient_v = button.data('v');

      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('.id_event_delete').val(recipient_c);
      document.getElementById("id_event_delete").value = recipient_c;


      document.getElementById("nama_edit_event_delete").innerHTML = recipient_v;
  })

  $('#modal-edit-event').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var recipient_a = button.data('a');
      var recipient_b = button.data('b');
      var recipient_c = button.data('c');
      var recipient_d = button.data('d');

      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('.id_event_edit').val(recipient_a);
      document.getElementById("id_event_edit").value = recipient_a;

      modal.find('.fotoLama').val(recipient_b);
      document.getElementById("fotoLama").value = recipient_b;
      if (recipient_b=="") {
        document.getElementById("foto_up").src = "<?= base_url('image/events'); ?>/0.jpg"
      }else{
        document.getElementById("foto_up").src = "<?= base_url('image/events'); ?>/"+recipient_b;
      }
      modal.find('.nama_edit_event').val(recipient_c);
      document.getElementById("nama_edit_event").value = recipient_c;
      modal.find('.halaman_edit_event').val(recipient_d);
      document.getElementById("halaman_edit_event").value = recipient_d;
  })

  $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()

      //Initialize Select2 Elements
      $('.select2bs4').select2({
          theme: 'bootstrap4'
      })

      //Datemask dd/mm/yyyy
      $('#datemask').inputmask('dd/mm/yyyy', {
          'placeholder': 'dd/mm/yyyy'
      })
      //Datemask2 mm/dd/yyyy
      $('#datemask2').inputmask('dd/mm/yyyy', {
          'placeholder': 'dd/mm/yyyy'
      })
      //Money Euro
      $('[data-mask]').inputmask()
      
      //Date range picker
      $('#reservationdate2').datetimepicker({
          format: 'DD-MMMM-yyyy'
      });
      //Date range picker
      $('#reservation2').daterangepicker()
      //Date range picker with time picker
      $('#reservationtime2').daterangepicker({
          timePicker: true,
          timePickerIncrement: 30,
          locale: {
              format: 'DD/MM/YYYY'
          }
      })

      //Date range picker
      $('#reservationdate').datetimepicker({
          format: 'DD-MMMM-yyyy'
      });
      //Date range picker
      $('#reservation').daterangepicker()
      //Date range picker with time picker
      $('#reservationtime').daterangepicker({
          timePicker: true,
          timePickerIncrement: 30,
          locale: {
              format: 'DD/MM/YYYY'
          }
      })

      //Timepicker
      $('#timepicker').datetimepicker({
          format: 'DD/MM/YYYY'
      })

      //Bootstrap Duallistbox
      $('.duallistbox').bootstrapDualListbox()
  })  

  $('#modal-add').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var modal = $(this)
  })
        
$(document).ready(function(){
  var $modal = $('#modal');
  var image = document.getElementById('sample_image');
  var cropper;

  //$("body").on("change", ".image", function(e){
  $('#foto_user').change(function(event){
      var files = event.target.files;
      var done = function (url) {
          image.src = url;
          $modal.modal('show');
      };

      if (files && files.length > 0)
      {
        reader = new FileReader();
        reader.onload = function (event) {
            done(reader.result);
        };
        reader.readAsDataURL(files[0]);
      }
  });

  $modal.on('shown.bs.modal', function() {
      cropper = new Cropper(image, {
        aspectRatio: 1,
        viewMode: 3,
        preview: '.preview'
      });
  }).on('hidden.bs.modal', function() {
      cropper.destroy();
      cropper = null;
  });

  $("#crop").click(function(){
      canvas = cropper.getCroppedCanvas({
          width: 400,
          height: 400,
      });

      canvas.toBlob(function(blob) {
          //url = URL.createObjectURL(blob);
          var reader = new FileReader();
          reader.readAsDataURL(blob); 
          reader.onloadend = function() {
              var base64data = reader.result;  
            
              $.ajax({
                url: "crop/upload.php",
                  method: "POST",                	
                  data: {image: base64data},
                  success: function(data){
                      console.log(data);
                      $modal.modal('hide');
                      $('#uploaded_image').attr('src', data);
                      //alert("success upload image");
                  }
                });
          }
      });
    });

});
</script>