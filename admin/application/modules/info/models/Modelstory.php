<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelstory extends CI_Model {

	public function get_index(){
		//build query index
		$result = $this->db->order_by('urutan','asc')
						   ->get_where('wb_story',array('deleted_at'=>NULL))
						   ->result();
		//return query result
		return $result;
	}

	public function simpan($data){
		//array data
		$array = array(
			'tittle' 			=> $data['nama'],
			'urutan'	 		=> $data['urutan'],
			'content'			=> $data['content'],
			'photos_story'		=> $data['id_user']."-".str_replace(" ","",$data['foto']),
			'created_at' 		=> date("Y-m-d h:i:s")	,
			'created_by' 		=> $data['id_user'],
		);
		//insert
		return $this->db->insert('wb_story', $array);
	}

	public function simpan_edit($data){
		//array data
		$array = array(
			'tittle' 		=> $data['nama'],
			'urutan'	 		=> $data['urutan'],
			'content'			=> $data['content'],
			'photos_story' 		=> $data['foto'],
			'updated_at' 		=> date("Y-m-d h:i:s")	,
			'updated_by' 		=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_wb_story', $data['id']);
		//update	
		return $this->db->update('wb_story');
	}

	public function hapus($data){
		//array data
		$array = array(
			'deleted_at' 		=> date("Y-m-d h:i:s")	,
			'deleted_by' 		=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_wb_story', $data['id']);
		//update
		return $this->db->update('wb_story');
	}
}