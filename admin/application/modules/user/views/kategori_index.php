 <!-- Content Header (Page header) -->
  <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Kategori User
              </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">

            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                
                <!-- /.card-header -->
                <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Kategori User</th>
                        <th>Keterangan</th>
                        <th style ="text-align: center;">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $no = 1;
                        foreach($grid as $data){
                      ?>
                        <tr>
                            <td><?= number_format($no++,0)?></td>
                            <td><?= $data->nama_level; ?></td>
                            <td><?= $data->keterangan; ?></td>
                            <td style="text-align: center;">
                            <button class="btn btn-info btn-sm" name="id_ev" style="margin-right: 15px;"
                                data-a="<?= $data->id_level; ?>"
                                data-b="<?= $data->nama_level; ?>"
                                data-c="<?= $data->keterangan; ?>"
                                data-toggle="modal" data-target="#modal-edit-kategoriuser" data-backdrop="static"
                                data-keyboard="false">
                                <i class="fas fa-pencil-alt">
                                </i>
                                Edit
                            </button>
                            <button class="btn btn-danger btn-sm" data-backdrop="static" data-keyboard="false"
                                data-c="<?= $data->id_level; ?>" 
                                data-v="<?= $data->nama_level; ?>"
                                data-toggle="modal" data-target="#modal-delete-kategoriuser">
                                <i class="fas fa-trash">
                                </i>
                                Delete
                            </button>

                            </td>
                          </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
          </div>
          <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->

  <div class="modal fade" id="modal-delete-kategoriuser">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Apakah anda yakin menghapus Kategori User ini ?<br>
            Nama Kategori User &nbsp; : <b id="nama_kategoriuser_delete"></b><br>
        </div>
        <form action="<?= site_url('/user/kategori/delete'); ?>" method="post">
          <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
          <input type="hidden" name="id_kategoriuser_delete" id="id_kategoriuser_delete" value="">
          
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-danger">Yes</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->


  <div class="modal fade" id="modal-edit-kategoriuser">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">Edit Data Kategori User</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?= site_url('/user/kategori/save_edit'); ?>" method="post" enctype="multipart/form-data">
          <div class="modal-body">

            <div class="form-group row">
              <label for="nama_edit_kategoriuser" class="col-sm-12 col-form-label">Nama Katgeori User (*)</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="nama_edit_kategoriuser" name="nama_edit_kategoriuser"
                  placeholder="Ketikan Nama Kategori User" value="" required>
              </div>
            </div>
            
            <div class="form-group row">
              <label for="keterangan_edit_kategoriuser" class="col-sm-12 col-form-label">Keterangan</label>
              <div class="col-sm-12">
                <textarea class="form-control" id="keterangan_edit_kategoriuser" name="keterangan_edit_kategoriuser"
                  placeholder="Ketikan Keterangan" rows=3></textarea>
              </div>
            </div>

            <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
            <input type="hidden" name="id_kategoriuser_edit" id="id_kategoriuser_edit" value="">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <div class="modal fade" id="modal-add">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel2">Tambahkan Data Kategori User</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?= site_url('/user/kategori/save'); ?>" method="post" enctype="multipart/form-data">
          <div class="modal-body">

            <div class="form-group row">
              <label for="nama_add_kategoriuser" class="col-sm-12 col-form-label">Nama Kategori User (*)</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="nama_add_kategoriuser" name="nama_add_kategoriuser"
                  placeholder="Ketikan Nama Kategori User" value="" required>
              </div>
            </div>
            
            <div class="form-group row">
              <label for="keterangan_add_kategoriuser" class="col-sm-12 col-form-label">Keterangan</label>
              <div class="col-sm-12">
                <textarea class="form-control" id="keterangan_add_kategoriuser" name="keterangan_add_kategoriuser"
                  placeholder="Ketikan Keterangan" rows=3></textarea>
              </div>
            </div>

            <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
  </div>


  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        "paging": false,
        // "sorting": false,
        // "buttons": ["copy", "csv", "excel", "pdf", "print"],
        buttons: [
                {
                    text: '+ Add Kategori User',
                    action: function ( e, dt, node, config ) {
                        $('#modal-add').modal({backdrop: 'static', keyboard: false});
                    }
                }
            ]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });



    $('#modal-delete-kategoriuser').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var recipient_c = button.data('c');

      var recipient_v = button.data('v');

      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('.id_kategoriuser_delete').val(recipient_c);
      document.getElementById("id_kategoriuser_delete").value = recipient_c;


      document.getElementById("nama_kategoriuser_delete").innerHTML = recipient_v;
    })



    $('#modal-edit-kategoriuser').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var recipient_a = button.data('a');
      var recipient_b = button.data('b');
      var recipient_c = button.data('c');

      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('.id_kategoriuser_edit').val(recipient_a);
      document.getElementById("id_kategoriuser_edit").value = recipient_a;

      modal.find('.nama_edit_kategoriuser').val(recipient_b);
      document.getElementById("nama_edit_kategoriuser").value = recipient_b;

      modal.find('.keterangan_edit_kategoriuser').val(recipient_c);
      document.getElementById("keterangan_edit_kategoriuser").value = recipient_c;

    })

    $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()

      //Initialize Select2 Elements
      $('.select2bs4').select2({
        theme: 'bootstrap4'
      })

      //Datemask dd/mm/yyyy
      $('#datemask').inputmask('dd/mm/yyyy', {
        'placeholder': 'dd/mm/yyyy'
      })
      //Datemask2 mm/dd/yyyy
      $('#datemask2').inputmask('dd/mm/yyyy', {
        'placeholder': 'dd/mm/yyyy'
      })
      //Money Euro
      $('[data-mask]').inputmask()

      //Date range picker
      $('#reservationdate2').datetimepicker({
        format: 'DD-MMMM-yyyy'
      });
      //Date range picker
      $('#reservation2').daterangepicker()
      //Date range picker with time picker
      $('#reservationtime2').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
          format: 'DD/MM/YYYY'
        }
      })

      //Date range picker
      $('#reservationdate').datetimepicker({
        format: 'DD-MMMM-yyyy'
      });
      //Date range picker
      $('#reservation').daterangepicker()
      //Date range picker with time picker
      $('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
          format: 'DD/MM/YYYY'
        }
      })

      //Timepicker
      $('#timepicker').datetimepicker({
        format: 'DD/MM/YYYY'
      })

      //Bootstrap Duallistbox
      $('.duallistbox').bootstrapDualListbox()
    })

    $('#modal-add').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var modal = $(this)
    })

  </script>

  <script>
  $(document).ready(function(){
    var $modal = $('#modal');
    var image = document.getElementById('foto_up2');
    var cropper;

    $('#upload_image').change(function(event){
        var files = event.target.files;
        var done = function (url) {
            image.src = url;
            $modal.modal('show');
        };

        if (files && files.length > 0)
        {
              reader = new FileReader();
              reader.onload = function (event) {
                  done(reader.result);
              };
              reader.readAsDataURL(files[0]);
        }
    });

    $modal.on('shown.bs.modal', function() {
        cropper = new Cropper(image, {
          aspectRatio: 1,
          viewMode: 3,
          preview: '.preview'
        });
    }).on('hidden.bs.modal', function() {
        cropper.destroy();
        cropper = null;
    });

    $("#crop").click(function(){
        canvas = cropper.getCroppedCanvas({
            width: 400,
            height: 400,
        });

        canvas.toBlob(function(blob) {
            //url = URL.createObjectURL(blob);
            var reader = new FileReader();
            reader.readAsDataURL(blob); 
            reader.onloadend = function() {
                var base64data = reader.result;  
              
            }
        });
      });

    });

  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#foto_up').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
  }

  $("#foto_user").change(function () {
      readURL(this);
  });

  function readURL1(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#foto_up2').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
  }

  $("#foto_user2").change(function () {
      readURL1(this);
  });
</script>