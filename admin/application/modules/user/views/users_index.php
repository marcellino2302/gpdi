 <!-- Content Header (Page header) -->
 <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Users
              </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">

            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                
                <!-- /.card-header -->
                <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama User</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Level</th>
                            <th style ="text-align: center;">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $no = 1;
                        foreach($grid as $data){
                      ?>
                        <tr>
                            <td><?= number_format($no++,0)?></td>
                            <td><?= $data->nama_user; ?></td>
                            <td><?= $data->username; ?></td>
                            <td><?= $data->email; ?></td>
                            <td><?= $data->nama_level ; ?></td>
                            <td style="text-align: center;">
                            <button class="btn btn-info btn-sm" name="id_ev" style="margin-right: 15px;"
                                data-a="<?= $data->id_user; ?>"
                                data-b="<?= $data->nama_user; ?>"
                                data-c="<?= $data->email; ?>"
                                data-d="<?= $data->username; ?>"
                                data-e="<?= $data->id_level; ?>"
                                data-toggle="modal" data-target="#modal-edit-user" data-backdrop="static"
                                data-keyboard="false">
                                <i class="fas fa-pencil-alt">
                                </i>
                                Edit
                            </button>
                            <button class="btn btn-danger btn-sm" data-backdrop="static" data-keyboard="false"
                                data-c="<?= $data->id_user; ?>" 
                                data-v="<?= $data->nama_user; ?>"
                                data-toggle="modal" data-target="#modal-delete-user">
                                <i class="fas fa-trash">
                                </i>
                                Delete
                            </button>

                            </td>
                          </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
          </div>
          <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
 
<div class="modal fade" id="modal-delete-user">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Apakah anda yakin menghapus User ini ?<br>
            Nama User &nbsp; : <b id="nama_user_delete"></b><br>
        </div>
        <form action="<?= site_url('/user/users/delete'); ?>" method="post">
          <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
          <input type="hidden" name="id_user_delete" id="id_user_delete" value="">
          
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-danger">Yes</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->


  <div class="modal fade" id="modal-edit-user">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">Edit Data User</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?= site_url('/user/users/save_edit'); ?>" method="post" enctype="multipart/form-data">
          <div class="modal-body">

          <div class="form-group row">
              <label for="nama_edit_user" class="col-sm-12 col-form-label">Nama User (*)</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="nama_edit_user" name="nama_edit_user"
                  placeholder="Ketikan Nama User" value="" required>
              </div>
            </div>
            
            <div class="form-group row">
              <label for="nama_edit_email" class="col-sm-12 col-form-label">Email (*)</label>
              <div class="col-sm-12">
                <input type="email" class="form-control" id="nama_edit_email" name="nama_edit_email"
                  placeholder="Ketikan Email" value="" required>
              </div>
            </div>

            <div class="form-group row">
              <label for="nama_edit_username" class="col-sm-12 col-form-label">Username (*)</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="nama_edit_username" name="nama_edit_username"
                  placeholder="Ketikan Username" value="" required>
              </div>
            </div>

            <div class="form-group row">
              <label for="nama_edit_kategori" class="col-sm-12 col-form-label">Level (*)</label>
              <div class="col-sm-12">
                <select class="form-control" id="nama_edit_kategori" name="nama_edit_kategori">
                  <?php 
                    foreach($level as $lv){
                        echo "<option value='".$lv->id_level."' >".$lv->nama_level."</option>";
                    }
                    ?>
                </select>
              </div>
            </div>


            <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
            <input type="hidden" name="id_user_edit" id="id_user_edit" value="">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <div class="modal fade" id="modal-add">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel2">Tambahkan Data User</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?= site_url('/user/users/save'); ?>" method="post" enctype="multipart/form-data">
          <div class="modal-body">

            <div class="form-group row">
              <label for="nama_add_user" class="col-sm-12 col-form-label">Nama User (*)</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="nama_add_user" name="nama_add_user"
                  placeholder="Ketikan Nama User" value="" required>
              </div>
            </div>

            <div class="form-group row">
              <label for="nama_add_email" class="col-sm-12 col-form-label">Email (*)</label>
              <div class="col-sm-12">
                <input type="email" class="form-control" id="nama_add_email" name="nama_add_email"
                  placeholder="Ketikan Email" value="" required>
              </div>
            </div>

            <div class="form-group row">
              <label for="nama_add_username" class="col-sm-12 col-form-label">Username (*)</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="nama_add_username" name="nama_add_username"
                  placeholder="Ketikan Username" value="" required>
              </div>
            </div>
            
            <div class="form-group row">
              <label for="nama_add_password" class="col-sm-12 col-form-label">Password (*)</label>
              <div class="col-sm-12">
                <input type="password" class="form-control" id="nama_add_password" name="nama_add_password"
                  placeholder="Ketikan Password" value="" required>
              </div>
            </div>

            <div class="form-group row">
              <label for="nama_add_kategori" class="col-sm-12 col-form-label">Level (*)</label>
              <div class="col-sm-12">
                <select class="form-control" id="nama_add_kategori" name="nama_add_kategori">
                  <?php 
                    foreach($level as $lv){
                        echo "<option value='".$lv->id_level."' >".$lv->nama_level."</option>";
                    }
                   ?>
                </select>
              </div>
            </div>

            <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
  </div>

  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        "paging": false,
        // "sorting": false,
        // "buttons": ["copy", "csv", "excel", "pdf", "print"],
        buttons: [
                {
                    text: '+ Add User',
                    action: function ( e, dt, node, config ) {
                        $('#modal-add').modal({backdrop: 'static', keyboard: false});
                    }
                }
            ]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });

    $('#modal-delete-user').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var recipient_c = button.data('c');

      var recipient_v = button.data('v');

      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('.id_user_delete').val(recipient_c);
      document.getElementById("id_user_delete").value = recipient_c;


      document.getElementById("nama_user_delete").innerHTML = recipient_v;
    })

    $('#modal-edit-user').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var recipient_a = button.data('a');
      var recipient_b = button.data('b');
      var recipient_c = button.data('c');
      var recipient_d = button.data('d');
      var recipient_e = button.data('e');

      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('.id_user_edit').val(recipient_a);
      document.getElementById("id_user_edit").value = recipient_a;

      modal.find('.nama_edit_user').val(recipient_b);
      document.getElementById("nama_edit_user").value = recipient_b;

      modal.find('.nama_edit_email').val(recipient_c);
      document.getElementById("nama_edit_email").value = recipient_c;

      modal.find('.nama_edit_username').val(recipient_d);
      document.getElementById("nama_edit_username").value = recipient_d;

      modal.find('.nama_edit_kategori').val(recipient_e);
      document.getElementById("nama_edit_kategori").value = recipient_e;

    })

    $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()

      //Initialize Select2 Elements
      $('.select2bs4').select2({
        theme: 'bootstrap4'
      })

      //Datemask dd/mm/yyyy
      $('#datemask').inputmask('dd/mm/yyyy', {
        'placeholder': 'dd/mm/yyyy'
      })
      //Datemask2 mm/dd/yyyy
      $('#datemask2').inputmask('dd/mm/yyyy', {
        'placeholder': 'dd/mm/yyyy'
      })
      //Money Euro
      $('[data-mask]').inputmask()

      //Date range picker
      $('#reservationdate2').datetimepicker({
        format: 'DD-MMMM-yyyy'
      });
      //Date range picker
      $('#reservation2').daterangepicker()
      //Date range picker with time picker
      $('#reservationtime2').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
          format: 'DD/MM/YYYY'
        }
      })

      //Date range picker
      $('#reservationdate').datetimepicker({
        format: 'DD-MMMM-yyyy'
      });
      //Date range picker
      $('#reservation').daterangepicker()
      //Date range picker with time picker
      $('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
          format: 'DD/MM/YYYY'
        }
      })

      //Timepicker
      $('#timepicker').datetimepicker({
        format: 'DD/MM/YYYY'
      })

      //Bootstrap Duallistbox
      $('.duallistbox').bootstrapDualListbox()
    })

    $('#modal-add').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var modal = $(this)
    })
</script>