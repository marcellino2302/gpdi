 <!-- Content Header (Page header) -->
 <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Hak Akses
              </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">

            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                
                <!-- /.card-header -->
                <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Kategori User</th>
                        <th>Keterangan</th>
                        <th style ="text-align: center;">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $no = 1;
                        foreach($grid as $data){
                      ?>
                        <tr>
                            <td><?= number_format($no++,0)?></td>
                            <td><?= $data->nama_level; ?></td>
                            <td><?= $data->keterangan; ?></td>
                            <td style="text-align: center;">
                            <button class="btn btn-info btn-sm" name="id_ev" style="margin-right: 15px;"
                                data-a="<?= $data->id_level; ?>"
                                data-b="<?= $data->nama_level; ?>"
                                data-toggle="modal" data-target="#modal-edit-hakakses" data-backdrop="static"
                                data-keyboard="false">
                                <i class="fas fa-pencil-alt">
                                </i>
                                Edit
                            </button>
                            </td>
                          </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
          </div>
          <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->

      <div class="modal fade" id="modal-edit-hakakses">
    <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">Edit Hak Akses</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?= site_url('/user/groupaccess/save'); ?>" method="post">
          <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <td>Nama Menu</td>
                        <td align="center">Akses</td>
                        <td align="center">Tambah</td>
                        <td align="center">Edit</td>
                        <td align="center">Post</td>
                        <td align="center">Print</td>
                        <td align="center">Hapus</td>
                    </thead>
                    <tbody id="isibodymenu"></tbody>
                </table>
            </div>


            <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
            <input type="hidden" name="id_level_edit" id="id_level_edit" value="">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        "paging": false,
        // "sorting": false,
        // "buttons": ["copy", "csv", "excel", "pdf", "print"],
        // buttons: [
        //         {
        //             text: '+ Add Kategori User',
        //             action: function ( e, dt, node, config ) {
        //                 $('#modal-add').modal({backdrop: 'static', keyboard: false});
        //             }
        //         }
        //     ]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });

    $('#modal-edit-hakakses').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var recipient_a = button.data('a');
      var recipient_b = button.data('b');

      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('.id_level_edit').val(recipient_a);
      document.getElementById("id_level_edit").value = recipient_a;

      modal.find('.exampleModalLabel').html('Edit Hak Akses '+recipient_b);
      document.getElementById("exampleModalLabel").innerHTML = 'Edit Hak Akses '+recipient_b;

      $.ajax({
        type: "post",
        url: "<?= site_url("user/groupaccess/getmenu") ?>",
        data: {
                'level':recipient_a,
              },
        success: function (response) {
          var menu = JSON.parse(response);
          console.log(menu)
          var html = '';
          for (var index = 0; index < menu.length; index++) {
            if(menu[index]['level'] == 'header_menu'){
              html += '<tr class="table-info">';
              html += '<td colspan=7><input type="hidden" name="idmenu[]" value="'+menu[index]['menu']+'"><input type="hidden" name="level[]" value="'+menu[index]['level']+'">'+menu[index]['nama_menu']+'</td>';
              html += '</tr>';
            }else{
              if(menu[index]['level'] == 'main_menu'){
                html += '<tr class="table-active">';
              }else{
                html += '<tr>';
              }
              html += '<td><input type="hidden" name="idmenu[]" value="'+menu[index]['menu']+'"><input type="hidden" name="level[]" value="'+menu[index]['level']+'">'+menu[index]['nama_menu']+'</td>';
              
              if(menu[index]['aksesmenu'] == '1'){
                html += '<td align="center"><input type="checkbox" name="akses'+menu[index]['menu']+'" ';
                if(menu[index]['akses'] == '1'){
                  html += 'checked';
                }
                html += '></td>';
              }else{
                html += '<td><input type="checkbox" name="akses'+menu[index]['menu']+'" hidden value=""></td>';
              }

              if(menu[index]['tambahmenu'] == '1'){
                html += '<td align="center"><input type="checkbox" name="tambah'+menu[index]['menu']+'" ';
                if(menu[index]['tambah'] == '1'){
                  html += 'checked';
                }
                html += '></td>';
              }else{
                html += '<td><input type="checkbox" name="tambah'+menu[index]['menu']+'" hidden value=""></td>';
              }

              if(menu[index]['editmenu'] == '1'){
                html += '<td align="center"><input type="checkbox" name="edit'+menu[index]['menu']+'" ';
                if(menu[index]['edit'] == '1'){
                  html += 'checked';
                }
                html += '></td>';
              }else{
                html += '<td><input type="checkbox" name="edit'+menu[index]['menu']+'" hidden value=""></td>';
              }

              if(menu[index]['postmenu'] == '1'){
                html += '<td align="center"><input type="checkbox" name="post'+menu[index]['menu']+'" ';
                if(menu[index]['post'] == '1'){
                  html += 'checked';
                }
                html += '></td>';
              }else{
                html += '<td><input type="checkbox" name="post'+menu[index]['menu']+'" hidden value=""></td>';
              }

              if(menu[index]['printmenu'] == '1'){
                html += '<td align="center"><input type="checkbox" name="print'+menu[index]['menu']+'" ';
                if(menu[index]['print'] == '1'){
                  html += 'checked';
                }
                html += '></td>';
              }else{
                html += '<td><input type="checkbox" name="print'+menu[index]['menu']+'" hidden value=""></td>';
              }

              if(menu[index]['hapusmenu'] == '1'){
                html += '<td align="center"><input type="checkbox" name="hapus'+menu[index]['menu']+'" ';
                if(menu[index]['hapus'] == '1'){
                  html += 'checked';
                }
                html += '></td>';
              }else{
                html += '<td><input type="checkbox" name="hapus'+menu[index]['menu']+'" hidden value=""></td>';
              }
              
              html += '</tr>';
            }
          }
          document.getElementById("isibodymenu").innerHTML = html;
        }
      });

    })

    $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()

      //Initialize Select2 Elements
      $('.select2bs4').select2({
        theme: 'bootstrap4'
      })

      //Datemask dd/mm/yyyy
      $('#datemask').inputmask('dd/mm/yyyy', {
        'placeholder': 'dd/mm/yyyy'
      })
      //Datemask2 mm/dd/yyyy
      $('#datemask2').inputmask('dd/mm/yyyy', {
        'placeholder': 'dd/mm/yyyy'
      })
      //Money Euro
      $('[data-mask]').inputmask()

      //Date range picker
      $('#reservationdate2').datetimepicker({
        format: 'DD-MMMM-yyyy'
      });
      //Date range picker
      $('#reservation2').daterangepicker()
      //Date range picker with time picker
      $('#reservationtime2').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
          format: 'DD/MM/YYYY'
        }
      })

      //Date range picker
      $('#reservationdate').datetimepicker({
        format: 'DD-MMMM-yyyy'
      });
      //Date range picker
      $('#reservation').daterangepicker()
      //Date range picker with time picker
      $('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
          format: 'DD/MM/YYYY'
        }
      })

      //Timepicker
      $('#timepicker').datetimepicker({
        format: 'DD/MM/YYYY'
      })

      //Bootstrap Duallistbox
      $('.duallistbox').bootstrapDualListbox()
    })

    $('#modal-add').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var modal = $(this)
    })

  </script>

  <script>
  $(document).ready(function(){
    var $modal = $('#modal');
    var image = document.getElementById('foto_up2');
    var cropper;

    $('#upload_image').change(function(event){
        var files = event.target.files;
        var done = function (url) {
            image.src = url;
            $modal.modal('show');
        };

        if (files && files.length > 0)
        {
              reader = new FileReader();
              reader.onload = function (event) {
                  done(reader.result);
              };
              reader.readAsDataURL(files[0]);
        }
    });

    $modal.on('shown.bs.modal', function() {
        cropper = new Cropper(image, {
          aspectRatio: 1,
          viewMode: 3,
          preview: '.preview'
        });
    }).on('hidden.bs.modal', function() {
        cropper.destroy();
        cropper = null;
    });

    $("#crop").click(function(){
        canvas = cropper.getCroppedCanvas({
            width: 400,
            height: 400,
        });

        canvas.toBlob(function(blob) {
            //url = URL.createObjectURL(blob);
            var reader = new FileReader();
            reader.readAsDataURL(blob); 
            reader.onloadend = function() {
                var base64data = reader.result;  
              
            }
        });
      });

    });

  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#foto_up').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
  }

  $("#foto_user").change(function () {
      readURL(this);
  });

  function readURL1(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#foto_up2').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
  }

  $("#foto_user2").change(function () {
      readURL1(this);
  });
</script>