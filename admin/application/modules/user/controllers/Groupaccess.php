<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Groupaccess extends MX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modelgroupaccess');
	}

    public function index(){
		//load view with get data
		$this->page->view('groupaccess_index', array (
			'grid'		=> $this->Modelgroupaccess->get_index(),
		));
    }

	public function getmenu(){
		$result = $this->Modelgroupaccess->getmenuLevel($_POST['level']);
		echo json_encode($result);
	}

	public function save(){
		for ($i=0; $i < count($_POST['idmenu']); $i++) { 
			//array data from input post
			$data['id_user'] 	= $this->input->post('id_user');
			$data['level'] 		= $this->input->post('id_level_edit');
			$data['menu'] 		= $_POST['idmenu'][$i];
			
			$akses 					= "akses".$_POST['idmenu'][$i];
			if(empty($_POST[$akses])){
				$data['akses'] 		= "0";
			} else {
				$data['akses'] 		= "1";
			}

			$tambah 				= "tambah".$_POST['idmenu'][$i];
			if(empty($_POST[$tambah])){
				$data['tambah'] 	= "0";
			} else {
				$data['tambah'] 	= "1";
			}

			$edit 					= "edit".$_POST['idmenu'][$i];
			if(empty($_POST[$edit])){
				$data['edit'] 		= "0";
			} else {
				$data['edit'] 		= "1";
			}

			$post 					= "post".$_POST['idmenu'][$i];
			if(empty($_POST[$post])){
				$data['post'] 		= "0";
			} else {
				$data['post'] 		= "1";
			}

			$print 					= "print".$_POST['idmenu'][$i];
			if(empty($_POST[$post])){
				$data['print'] 		= "0";
			} else {
				$data['print'] 		= "1";
			}

			$hapus 					= "hapus".$_POST['idmenu'][$i];
			if(empty($_POST[$hapus])){
				$data['hapus'] 		= "0";
			} else {
				$data['hapus'] 		= "1";
			}
			
			//save data
			$this->Modelgroupaccess->simpan($data);
		}

		//redirect page
		redirect($this->agent->referrer());
    }
}