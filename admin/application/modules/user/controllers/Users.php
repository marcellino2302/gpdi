<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Users extends MX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modelusers');
	}

    public function index(){
		//load view with get data
		$this->page->view('users_index', array (
			'grid'		=> $this->Modelusers->get_index(),
			'level'		=> $this->Modelusers->get_level(),
		));
    }

	public function save(){
		//array data from input post
		$data['nama'] 		= $this->input->post('nama_add_user');
		$data['email'] 		= $this->input->post('nama_add_email');
		$data['username'] 	= $this->input->post('nama_add_username');
		$data['password'] 	= md5($this->input->post('nama_add_password'));
		$data['level'] 		= $this->input->post('nama_add_kategori');
		$data['id_user'] 	= $this->input->post('id_user');

		//save data
		$this->Modelusers->simpan($data);

		//redirect page
		redirect($this->agent->referrer());
    }
	  
	public function save_edit(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_user_edit');
		$data['nama'] 		= $this->input->post('nama_edit_user');
		$data['email'] 		= $this->input->post('nama_edit_email');
		$data['username'] 	= $this->input->post('nama_edit_username');
		$data['level'] 		= $this->input->post('nama_edit_kategori');
		$data['id_user'] 	= $this->input->post('id_user');
		     
		//update data
		$this->Modelusers->simpan_edit($data);

		//redirect page
		redirect($this->agent->referrer());
    }
	 
	public function delete(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_user_delete');
		$data['nama'] 		= $this->input->post('nama_user_delete');
		$data['id_user'] 	= $this->input->post('id_user');
		  
		//delete data
		$this->Modelusers->hapus($data);

		//redirect page
		redirect($this->agent->referrer());
    }
}