<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Kategori extends MX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modelkategori');
	}

    public function index(){
		//load view with get data
		$this->page->view('kategori_index', array (
			'grid'		=> $this->Modelkategori->get_index(),
		));
    }

	public function save(){
		//array data from input post
		$data['nama'] 		= $this->input->post('nama_add_kategoriuser');
		$data['keterangan'] = $this->input->post('keterangan_add_kategoriuser');
		$data['id_user'] 	= $this->input->post('id_user');

		//save data
		$this->Modelkategori->simpan($data);

		//redirect page
		redirect($this->agent->referrer());
    }
	  
	public function save_edit(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_kategoriuser_edit');
		$data['nama'] 		= $this->input->post('nama_edit_kategoriuser');
		$data['keterangan'] = $this->input->post('keterangan_edit_kategoriuser');
		$data['id_user'] 	= $this->input->post('id_user');
		  
		//update data
		$this->Modelkategori->simpan_edit($data);

		//redirect page
		redirect($this->agent->referrer());
    }
	 
	public function delete(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_kategoriuser_delete');
		$data['nama'] 		= $this->input->post('nama_kategoriuser_delete');
		$data['id_user'] 	= $this->input->post('id_user');

		//delete data
		$this->Modelkategori->hapus($data);

		//redirect page
		redirect($this->agent->referrer());
    }
}