<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelkategori extends CI_Model {

	public function get_index(){
		//build query index
		$result = $this->db->order_by('id_level','asc')
						   ->get_where('ad_level',array('deleted'=>'0'))
						   ->result();
		//return query result
		return $result;
	}

	public function simpan($data){
		//array data
		$array = array(
			'nama_level' 		=> $data['nama'],
			'keterangan' 		=> $data['keterangan'],
			'lastmodified' 		=> date("Y-m-d h:i:s"),
			'user' 				=> $data['id_user'],
		);
		//insert
		$insert = $this->db->insert('ad_level', $array);
		//last insert id
		$insert_id = $this->db->insert_id();
		//insert
		$this->db->query("INSERT INTO wb_akses (id_menu,id_level,created_at,created_by) (SELECT id_menu,$insert_id,NOW(),".$data['id_user']." FROM wb_menu WHERE deleted_at IS NULL)");
		return $insert;
	}

	public function simpan_edit($data){
		//array data
		$array = array(
			'nama_level' 		=> $data['nama'],
			'keterangan' 		=> $data['keterangan'],
			'lastmodified' 		=> date("Y-m-d h:i:s"),
			'user' 				=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_level', $data['id']);
		//update	
		return $this->db->update('ad_level');
	}

	public function hapus($data){
		//array data
		$array = array(
			'lastmodified' 		=> date("Y-m-d h:i:s"),
			'deleted' 			=> '1',
			'user' 				=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_level', $data['id']);
		//update
		return $this->db->update('ad_level');
	}
}