<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelusers extends CI_Model {

	public function get_index(){
		//build query index
		$result = $this->db->order_by('nama_user','asc')
						   ->join('ad_level', 'ad_user.id_level = ad_level.id_level', 'left')
						   ->get_where('ad_user',array('ad_user.deleted'=>'0'))
						   ->result();
		//return query result
		return $result;
	}

	public function get_level(){
		//build query index
		$result = $this->db->order_by('id_level','asc')
						   ->get_where('ad_level',array('deleted'=>'0'))
						   ->result();
		//return query result
		return $result;
	}

	public function simpan($data){
		//array data
		$array = array(
			'nama_user' 		=> $data['nama'],
			'email' 			=> $data['email'],
			'username' 			=> $data['username'],
			'password' 			=> $data['password'],
			'id_level' 			=> $data['level'],
			'lastmodified' 		=> date("Y-m-d h:i:s"),
			'user' 				=> $data['id_user'],
		);
		//insert
		return $this->db->insert('ad_user', $array);
	}

	public function simpan_edit($data){
		//array data
		$array = array(
			'nama_user' 		=> $data['nama'],
			'email' 			=> $data['email'],
			'username' 			=> $data['username'],
			'id_level' 			=> $data['level'],
			'lastmodified' 		=> date("Y-m-d h:i:s"),
			'user' 				=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_user', $data['id']);
		//update	
		return $this->db->update('ad_user');
	}

	public function hapus($data){
		//array data
		$array = array(
			'lastmodified' 		=> date("Y-m-d h:i:s"),
			'deleted' 			=> '1',
			'user' 				=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_user', $data['id']);
		//update
		return $this->db->update('ad_user');
	}
}