<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelgroupaccess extends CI_Model {

	public function get_index(){
		//build query index
		$result = $this->db->order_by('id_level','asc')
						   ->get_where('ad_level',array('deleted'=>'0'))
						   ->result();
		//return query result
		return $result;
	}

	public function getmenuLevel($id){
		//return query result
		return $this->db->query("SELECT mn.id_menu AS menu, mn.nama_menu, mn.akses AS aksesmenu, mn.tambah AS tambahmenu, mn.edit AS editmenu, mn.post AS postmenu, mn.print AS printmenu, mn.hapus AS hapusmenu, mn.level, ak.* FROM wb_menu mn 
		LEFT JOIN wb_akses ak ON mn.id_menu=ak.id_menu AND ak.id_level = '$id'
		WHERE mn.hide=0 AND mn.deleted_at IS NULL
		ORDER BY mn.no_urut ASC")->result();
	}

	public function simpan($data){
		//array data
		$array = array(
			'akses' 		=> $data['akses'],
			'tambah' 		=> $data['tambah'],
			'edit' 			=> $data['edit'],
			'post' 			=> $data['post'],
			'print' 		=> $data['print'],
			'hapus' 		=> $data['hapus'],
			'updated_at' 	=> date("Y-m-d h:i:s"),
			'updated_by' 	=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_menu', $data['menu']);
		$this->db->where('id_level', $data['level']);
		//update	
		return $this->db->update('wb_akses');
	}
}