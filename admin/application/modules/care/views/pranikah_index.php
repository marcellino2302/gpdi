    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-12">
              <h1 class="m-0">Pendaftar Pranikah &nbsp;&nbsp;&nbsp;<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#modal-add" data-backdrop="static" data-keyboard="false"><span>+ Add Participant</span></button>
              </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">

            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
              <div class="card-header">
                        <h3>Belum Di Follow Up</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style ="width: 5%;">No</th>
                        <th style ="width: 10%;">Nama Pendaftar</th>
                        <th style ="width: 20%;">Alamat</th>
                        <th style ="width: 10%;">No Telepon</th>
                        <th style ="width: 10%;">KeBun</th>
                        <th style ="width: 25%;">Keterangan</th>
                        <th style="text-align: center;"  style ="width: 10%;">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $no = 1;
                        foreach($grid1 as $data1){
                        ?>
                      <tr>
                        <td><?= number_format($no++); ?></td>
                        <td><?= $data1->nama_pranikah; ?></td>
                        <td><?= $data1->alamat; ?></td>
                        <td><?= $data1->telepon; ?></td>
                        <td><?= $data1->nama_komsel; ?></td>
                        <td><?= $data1->keterangan; ?></td>
                        <td style="text-align: center;">
                          <a class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false"
                              href="<?= site_url('care/pranikah/follow');?>/<?= $data1->id_pranikah; ?>/<?=$this->session->userdata('pengguna')->id_user?>">
                            <i class="fas fa-check">
                            </i>
                            Sudah Di Follow Up
                          </a>
                          <br>
                          <button class="btn btn-info btn-sm" name="id_ev"
                            data-a="<?= $data1->id_pranikah; ?>" data-b="<?= $data1->nama_pranikah; ?>"
                            data-c="<?= $data1->alamat; ?>" data-d="<?= $data1->telepon; ?>"
                            data-e="<?= $data1->keterangan; ?>" 
                            data-f="<?= $data1->id_komsel; ?>" data-toggle="modal"
                            data-target="#modal-edit-pranikah" data-backdrop="static" data-keyboard="false">
                            <i class="fas fa-pencil-alt">
                            </i>
                            Edit
                          </button>
                          <button class="btn btn-danger btn-sm" data-backdrop="static" data-keyboard="false"
                            data-c="<?= $data1->id_pranikah; ?>" data-v="<?= $data1->nama_pranikah; ?>"
                            data-toggle="modal" data-target="#modal-delete-pranikah">
                            <i class="fas fa-trash">
                            </i>
                            Delete
                          </button>
                          

                        </td>
                      </tr>

                      <?php } ?>


                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
          </div>
          <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->

      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                        <h3>Sudah Di Follow Up</h3>
                </div>
                
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style ="width: 5%;">No</th>
                        <th style ="width: 10%;">Nama Pendaftar</th>
                        <th style ="width: 20%;">Alamat</th>
                        <th style ="width: 10%;">No Telepon</th>
                        <th style ="width: 10%;">KeBun</th>
                        <th style ="width: 25%;">Keterangan</th>
                        <th style="text-align: center;"  style ="width: 10%;">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $no2 = 1;
                        foreach($grid2 as $data2){
                        ?>
                      <tr>
                        <td><?= number_format($no2++); ?></td>
                        <td><?= $data2->nama_pranikah; ?></td>
                        <td><?= $data2->alamat; ?></td>
                        <td><?= $data2->telepon; ?></td>
                        <td><?= $data2->nama_komsel; ?></td>
                        <td><?= $data2->keterangan; ?></td>
                        <td style="text-align: center;">
                          <a class="btn btn-warning btn-sm" data-backdrop="static" data-keyboard="false"
                          href="<?= site_url('care/pranikah/unfollow');?>/<?= $data2->id_pranikah; ?>/<?=$this->session->userdata('pengguna')->id_user?>">
                            <i class="fa fa-times">
                            </i>
                            Belum Di-Follow Up
                          </a>
                          <br>
                          <button class="btn btn-info btn-sm" name="id_ev"
                            data-a="<?= $data2->id_pranikah; ?>" data-b="<?= $data2->nama_pranikah; ?>"
                            data-c="<?= $data2->alamat; ?>" data-d="<?= $data2->telepon; ?>"
                            data-e="<?= $data2->keterangan; ?>" 
                            data-f="<?= $data2->id_komsel; ?>" data-toggle="modal"
                            data-target="#modal-edit-pranikah" data-backdrop="static" data-keyboard="false">
                            <i class="fas fa-pencil-alt">
                            </i>
                            Edit
                          </button>
                          <button class="btn btn-danger btn-sm" data-backdrop="static" data-keyboard="false"
                            data-c="<?= $data2->id_pranikah; ?>" data-v="<?= $data2->nama_pranikah; ?>"
                            data-toggle="modal" data-target="#modal-delete-pranikah">
                            <i class="fas fa-trash">
                            </i>
                            Delete
                          </button>
                          

                        </td>
                      </tr>

                      <?php } ?>


                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
          </div>
          <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
      </section>

<div class="modal fade" id="modal-delete-pranikah">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Apakah anda yakin menghapus Pendaftar Penyerahan Anak ini ?<br>
            Nama Pendaftar &nbsp; : <b id="nama_pranikah_delete"></b><br>
        </div>
        <form action="<?= site_url('care/pranikah/delete');?>" method="post">
          <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
          <input type="hidden" name="id_pranikah_delete" id="id_pranikah_delete" value="">

          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-danger">Yes</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  

  <div class="modal fade" id="modal-edit-pranikah">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">Edit Data Participant</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?= site_url('care/pranikah/save_edit');?>" method="post" enctype="multipart/form-data">
          <div class="modal-body">

            <div class="form-group row">
              <label for="nama_edit_pranikah" class="col-sm-12 col-form-label">Nama Pendaftar (*)</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="nama_edit_pranikah" name="nama_edit_pranikah"
                  placeholder="Ketikan Nama Pendaftar" value="" required>
              </div>
            </div>
            
            <div class="form-group row">
              <label for="nama_edit_alamat" class="col-sm-12 col-form-label">Alamat (*)</label>
              <div class="col-sm-12">
                <textarea class="form-control" id="nama_edit_alamat" name="nama_edit_alamat"
                  placeholder="Ketikan Alamat" value="" rows=3 required></textarea>
              </div>
            </div>

            <div class="form-group row">
              <label for="nama_edit_telepon" class="col-sm-12 col-form-label">No Telepon (*)</label>
              <div class="col-sm-12">
                <input type="text" required class="form-control" id="nama_edit_telepon" name="nama_edit_telepon"
                  placeholder="Ketikan No Telepon" value="" data-inputmask='"mask": "9999999999999"' data-mask>
              </div>
            </div>

            <div class="form-group row">
              <label for="nama_edit_kebun" class="col-sm-12 col-form-label">KeBun (Komsel) (*)</label>
              <div class="col-sm-12">
                <select class="form-control" id="nama_edit_kebun" name="nama_edit_kebun">
                  <?php 
                    foreach($komsel as $km){
                        echo "<option value='".$km->id."' >".$km->nama_komsel."</option>";
                    }
                    ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label for="nama_edit_keterangan" class="col-sm-12 col-form-label">Keterangan Permintaan (*)</label>
              <div class="col-sm-12">
                <textarea class="form-control" id="nama_edit_keterangan" name="nama_edit_keterangan"
                  placeholder="Ketikan Keterangan Permintaan" value="" rows=5 required></textarea>
              </div>
            </div>


            <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
            <input type="hidden" name="id_pranikah_edit" id="id_pranikah_edit" value="">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <div class="modal fade" id="modal-add">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel2">Tambahkan Data Participant</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?= site_url('care/pranikah/save');?>" method="post" enctype="multipart/form-data">
          <div class="modal-body">

            <div class="form-group row">
              <label for="nama_add_pranikah" class="col-sm-12 col-form-label">Nama Pendaftar (*)</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="nama_add_pranikah" name="nama_add_pranikah"
                  placeholder="Ketikan Nama Pendaftar" value="" required>
              </div>
            </div>
            
            <div class="form-group row">
              <label for="nama_add_alamat" class="col-sm-12 col-form-label">Alamat  (*)</label>
              <div class="col-sm-12">
                <textarea class="form-control" id="nama_add_alamat" name="nama_add_alamat"
                  placeholder="Ketikan Alamat" value="" rows=3 required></textarea>
              </div>
            </div>

            <div class="form-group row">
              <label for="nama_add_no_telp" class="col-sm-12 col-form-label">No Telepon (*)</label>
              <div class="col-sm-12">
                <input type="text" required class="form-control" id="nama_add_no_telp" name="nama_add_no_telp"
                  placeholder="Ketikan No Telepon" value="" data-inputmask='"mask": "9999999999999"' data-mask>
              </div>
            </div>

            <div class="form-group row">
              <label for="nama_add_kebun" class="col-sm-12 col-form-label">KeBun (Komsel) (*)</label>
              <div class="col-sm-12">
                <select class="form-control" id="nama_add_kebun" name="nama_add_kebun">
                  <?php 
                    foreach($komsel as $km){
                        echo "<option value='".$km->id."' >".$km->nama_komsel."</option>";
                    }
                    ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label for="nama_add_keterangan" class="col-sm-12 col-form-label">Keterangan Permintaan (*)</label>
              <div class="col-sm-12">
                <textarea class="form-control" id="nama_add_keterangan" name="nama_add_keterangan"
                  placeholder="Ketikan Keterangan Permintaan" value="" rows=5 required></textarea>
              </div>
            </div>


            <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
  </div>

  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        "paging": true,
        // "sorting": false,
        // "buttons": ["copy", "csv", "excel", "pdf", "print"],
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

      $("#example2").DataTable({
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        "paging": true,
        // "sorting": false,
        // "buttons": ["copy", "csv", "excel", "pdf", "print"],
      }).buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');

    });



    $('#modal-delete-pranikah').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var recipient_c = button.data('c');

      var recipient_v = button.data('v');

      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('.id_pranikah_delete').val(recipient_c);
      document.getElementById("id_pranikah_delete").value = recipient_c;


      document.getElementById("nama_pranikah_delete").innerHTML = recipient_v;
    })



    $('#modal-edit-pranikah').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var recipient_a = button.data('a');
      var recipient_b = button.data('b');
      var recipient_c = button.data('c');
      var recipient_d = button.data('d');
      var recipient_e = button.data('e');
      var recipient_f = button.data('f');

      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('.id_pranikah_edit').val(recipient_a);
      document.getElementById("id_pranikah_edit").value = recipient_a;

      modal.find('.nama_edit_pranikah').val(recipient_b);
      document.getElementById("nama_edit_pranikah").value = recipient_b;

      modal.find('.nama_edit_alamat').val(recipient_c);
      document.getElementById("nama_edit_alamat").value = recipient_c;

      modal.find('.nama_edit_telepon').val(recipient_d);
      document.getElementById("nama_edit_telepon").value = recipient_d;

      modal.find('.nama_edit_keterangan').val(recipient_e);
      document.getElementById("nama_edit_keterangan").value = recipient_e;

      modal.find('.nama_edit_kebun').val(recipient_f);
      document.getElementById("nama_edit_kebun").value = recipient_f;

    })

    $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()

      //Initialize Select2 Elements
      $('.select2bs4').select2({
        theme: 'bootstrap4'
      })

      //Datemask dd/mm/yyyy
      $('#datemask').inputmask('dd/mm/yyyy', {
        'placeholder': 'dd/mm/yyyy'
      })
      //Datemask2 mm/dd/yyyy
      $('#datemask2').inputmask('dd/mm/yyyy', {
        'placeholder': 'dd/mm/yyyy'
      })
      //Money Euro
      $('[data-mask]').inputmask()

      //Date range picker
      $('#reservationdate2').datetimepicker({
        format: 'DD-MMMM-yyyy'
      });
      //Date range picker
      $('#reservation2').daterangepicker()
      //Date range picker with time picker
      $('#reservationtime2').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
          format: 'DD/MM/YYYY'
        }
      })

      //Date range picker
      $('#reservationdate').datetimepicker({
        format: 'DD-MMMM-yyyy'
      });
      //Date range picker
      $('#reservation').daterangepicker()
      //Date range picker with time picker
      $('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
          format: 'DD/MM/YYYY'
        }
      })

      //Timepicker
      $('#timepicker').datetimepicker({
        format: 'DD/MM/YYYY'
      })

      //Bootstrap Duallistbox
      $('.duallistbox').bootstrapDualListbox()
    })

    $('#modal-add').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var modal = $(this)
    })

  </script>