<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelkonseling extends CI_Model {

	public function get_index1(){
		//build query index
		$result = $this->db->order_by('id_konseling','desc')
                            ->join('wb_komsel', 'wb_komsel.id = wb_konseling.id_komsel', 'left')
						    ->get_where('wb_konseling',array('wb_konseling.deleted_at'=>NULL,'status_konseling'=>'0'))
						    ->result();
		//return query result
		return $result;
	}

    public function get_index2(){
		//build query index
		$result = $this->db->order_by('id_konseling','desc')
                            ->join('wb_komsel', 'wb_komsel.id = wb_konseling.id_komsel', 'left')
						    ->get_where('wb_konseling',array('wb_konseling.deleted_at'=>NULL,'status_konseling'=>'1'))
						    ->result();
		//return query result
		return $result;
	}

    public function get_komsel(){
		//build query index
		$result = $this->db->query("SELECT * FROM `wb_komsel` WHERE `deleted_at` IS NULL AND `id` = '30' UNION ALL (SELECT * FROM `wb_komsel` WHERE `deleted_at` IS NULL AND `id` <> '30' ORDER BY `id` ASC)")->result();
		//return query result
		return $result;
	}

    public function simpan($data){
		//array data
		$array = array(
			'nama_konseling'   		=> $data['nama'],
			'alamat'			    => $data['alamat'],
			'telepon'			    => str_replace("_","",$data['telp']),
			'id_komsel'			    => $data['kebun'],
			'keterangan'			=> $data['keterangan'],
			'created_at' 		    => date("Y-m-d h:i:s"),
			'created_by' 		    => $data['id_user'],
		);
		//insert
		return $this->db->insert('wb_konseling', $array);
	}

	public function simpan_edit($data){
		//array data
		$array = array(
			'nama_konseling'   		=> $data['nama'],
			'alamat'			    => $data['alamat'],
			'telepon'			    => str_replace("_","",$data['telp']),
			'id_komsel'			    => $data['kebun'],
			'keterangan'			=> $data['keterangan'],
			'updated_by' 		    => date("Y-m-d h:i:s"),
			'updated_by' 		    => $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_konseling', $data['id']);
		//update	
		return $this->db->update('wb_konseling');
	}

	public function hapus($data){
		//array data
		$array = array(
			'deleted_at' 		=> date("Y-m-d h:i:s")	,
			'deleted_by' 		=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_konseling', $data['id']);
		//update
		return $this->db->update('wb_konseling');
	}

    public function followup($data){
		//array data
		$array = array(
			'status_konseling' 		=> 1,
			'updated_at' 		    => date("Y-m-d h:i:s"),
			'updated_by' 		    => $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_konseling', $data['id']);
		//update
		return $this->db->update('wb_konseling');
	}

    public function unfollowup($data){
		//array data
		$array = array(
			'status_konseling' 		=> 0,
			'updated_at' 		    => date("Y-m-d h:i:s"),
			'updated_by' 		    => $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_konseling', $data['id']);
		//update
		return $this->db->update('wb_konseling');
	}
}