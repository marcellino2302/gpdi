<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelpermohonandoa extends CI_Model {

	public function get_index1(){
		//build query index
		$result = $this->db->order_by('id_permohonandoa','desc')
                            ->join('wb_komsel', 'wb_komsel.id = wb_permohonandoa.id_komsel', 'left')
						    ->get_where('wb_permohonandoa',array('wb_permohonandoa.deleted_at'=>NULL,'status_permohonandoa'=>'0'))
						    ->result();
		//return query result
		return $result;
	}

    public function get_index2(){
		//build query index
		$result = $this->db->order_by('id_permohonandoa','desc')
                            ->join('wb_komsel', 'wb_komsel.id = wb_permohonandoa.id_komsel', 'left')
						    ->get_where('wb_permohonandoa',array('wb_permohonandoa.deleted_at'=>NULL,'status_permohonandoa'=>'1'))
						    ->result();
		//return query result
		return $result;
	}

    public function get_komsel(){
		//build query index
		$result = $this->db->query("SELECT * FROM `wb_komsel` WHERE `deleted_at` IS NULL AND `id` = '30' UNION ALL (SELECT * FROM `wb_komsel` WHERE `deleted_at` IS NULL AND `id` <> '30' ORDER BY `id` ASC)")->result();
		//return query result
		return $result;
	}

    public function simpan($data){
		//array data
		$array = array(
			'nama_permohonandoa'   		=> $data['nama'],
			'alamat'			    => $data['alamat'],
			'telepon'			    => str_replace("_","",$data['telp']),
			'id_komsel'			    => $data['kebun'],
			'keterangan'			=> $data['keterangan'],
			'created_at' 		    => date("Y-m-d h:i:s"),
			'created_by' 		    => $data['id_user'],
		);
		//insert
		return $this->db->insert('wb_permohonandoa', $array);
	}

	public function simpan_edit($data){
		//array data
		$array = array(
			'nama_permohonandoa'   		=> $data['nama'],
			'alamat'			    => $data['alamat'],
			'telepon'			    => str_replace("_","",$data['telp']),
			'id_komsel'			    => $data['kebun'],
			'keterangan'			=> $data['keterangan'],
			'updated_by' 		    => date("Y-m-d h:i:s"),
			'updated_by' 		    => $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_permohonandoa', $data['id']);
		//update	
		return $this->db->update('wb_permohonandoa');
	}

	public function hapus($data){
		//array data
		$array = array(
			'deleted_at' 		=> date("Y-m-d h:i:s")	,
			'deleted_by' 		=> $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_permohonandoa', $data['id']);
		//update
		return $this->db->update('wb_permohonandoa');
	}

    public function followup($data){
		//array data
		$array = array(
			'status_permohonandoa' 		=> 1,
			'updated_at' 		    => date("Y-m-d h:i:s"),
			'updated_by' 		    => $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_permohonandoa', $data['id']);
		//update
		return $this->db->update('wb_permohonandoa');
	}

    public function unfollowup($data){
		//array data
		$array = array(
			'status_permohonandoa' 		=> 0,
			'updated_at' 		    => date("Y-m-d h:i:s"),
			'updated_by' 		    => $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_permohonandoa', $data['id']);
		//update
		return $this->db->update('wb_permohonandoa');
	}
}