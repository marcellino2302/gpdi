<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Konseling extends MX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modelkonseling');
	}

    public function index(){
		//load view with get data
		$this->page->view('konseling_index', array (
			'grid1'		=> $this->Modelkonseling->get_index1(),
			'grid2'		=> $this->Modelkonseling->get_index2(),
            'komsel'	=> $this->Modelkonseling->get_komsel(),
		));
    }

	public function save(){
		//array data from input post
		$data['nama'] 		= $this->input->post('nama_add_konseling');
		$data['alamat'] 	= $this->input->post('nama_add_alamat');
		$data['telp'] 	    = $this->input->post('nama_add_no_telp');
		$data['kebun'] 	    = $this->input->post('nama_add_kebun');
		$data['keterangan'] = $this->input->post('nama_add_keterangan');
		$data['id_user'] 	= $this->input->post('id_user');

		//save data
		$this->Modelkonseling->simpan($data);

		//redirect pages
		redirect($this->agent->referrer());
    }

	public function save_edit(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_konseling_edit');
		$data['nama'] 		= $this->input->post('nama_edit_konseling');
		$data['alamat'] 	= $this->input->post('nama_edit_alamat');
		$data['telp'] 	    = $this->input->post('nama_edit_telepon');
		$data['kebun'] 	    = $this->input->post('nama_edit_kebun');
		$data['keterangan'] = $this->input->post('nama_edit_keterangan');
		$data['id_user'] 	= $this->input->post('id_user');

		//update data
		$this->Modelkonseling->simpan_edit($data);

		//redirect page
		redirect($this->agent->referrer());
    }

	public function delete(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_konseling_delete');
		$data['nama'] 		= $this->input->post('nama_konseling_delete');
		$data['id_user'] 	= $this->input->post('id_user');
         
		//delete data
		$this->Modelkonseling->hapus($data);

		//redirect page
		redirect($this->agent->referrer());
    }

    public function follow($id,$user){
		//array data
		$data['id'] 		= $id;
		$data['id_user'] 	= $user;
         
		//update data
		$this->Modelkonseling->followup($data);

		//redirect page
		redirect($this->agent->referrer());
    }

    public function unfollow($id,$user){
		//array data
		$data['id'] 		= $id;
		$data['id_user'] 	= $user;
         
		//update data
		$this->Modelkonseling->unfollowup($data);

		//redirect page
		redirect($this->agent->referrer());
    }
}