<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Visitasi extends MX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modelvisitasi');
	}

    public function index(){
		//load view with get data
		$this->page->view('visitasi_index', array (
			'grid1'		=> $this->Modelvisitasi->get_index1(),
			'grid2'		=> $this->Modelvisitasi->get_index2(),
            'komsel'	=> $this->Modelvisitasi->get_komsel(),
		));
    }

	public function save(){
		//array data from input post
		$data['nama'] 		= $this->input->post('nama_add_visitasi');
		$data['alamat'] 	= $this->input->post('nama_add_alamat');
		$data['telp'] 	    = $this->input->post('nama_add_no_telp');
		$data['kebun'] 	    = $this->input->post('nama_add_kebun');
		$data['keterangan'] = $this->input->post('nama_add_keterangan');
		$data['id_user'] 	= $this->input->post('id_user');

		//save data
		$this->Modelvisitasi->simpan($data);

		//redirect pages
		redirect($this->agent->referrer());
    }

	public function save_edit(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_visitasi_edit');
		$data['nama'] 		= $this->input->post('nama_edit_visitasi');
		$data['alamat'] 	= $this->input->post('nama_edit_alamat');
		$data['telp'] 	    = $this->input->post('nama_edit_telepon');
		$data['kebun'] 	    = $this->input->post('nama_edit_kebun');
		$data['keterangan'] = $this->input->post('nama_edit_keterangan');
		$data['id_user'] 	= $this->input->post('id_user');

		//update data
		$this->Modelvisitasi->simpan_edit($data);

		//redirect page
		redirect($this->agent->referrer());
    }

	public function delete(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_visitasi_delete');
		$data['nama'] 		= $this->input->post('nama_visitasi_delete');
		$data['id_user'] 	= $this->input->post('id_user');
         
		//delete data
		$this->Modelvisitasi->hapus($data);

		//redirect page
		redirect($this->agent->referrer());
    }

    public function follow($id,$user){
		//array data
		$data['id'] 		= $id;
		$data['id_user'] 	= $user;
         
		//update data
		$this->Modelvisitasi->followup($data);

		//redirect page
		redirect($this->agent->referrer());
    }

    public function unfollow($id,$user){
		//array data
		$data['id'] 		= $id;
		$data['id_user'] 	= $user;
         
		//update data
		$this->Modelvisitasi->unfollowup($data);

		//redirect page
		redirect($this->agent->referrer());
    }
}