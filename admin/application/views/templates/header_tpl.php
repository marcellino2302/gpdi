<?php 
	if (!$this->session->has_userdata('pengguna')){
		redirect('site');
		exit;
	}
?>
<!-- <div class="top_nav"> -->
  <!-- <div class="nav_menu">
    <div class="nav toggle">
      <a id="menu_toggle"><i class="fa fa-bars"></i></a>
    </div>
    <nav class="nav navbar-nav">
      <ul class=" navbar-right">
        <li class="nav-item dropdown open" style="padding-left: 15px;">
          <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
            <img src="<?php echo base_url('/assets/images/user.png'); ?>" alt=""><?php echo $this->session->userdata('pengguna')->nama;?>
          </a>
          <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item"  href="<?php echo site_url('/account/ubah_password'); ?>"><i class="fa fa-key pull-right"></i>Change Password</a>
            <a class="dropdown-item"  href="javascript:void(0);" onclick="logout()"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
          </div>
        </li>
      </ul>
    </nav>
  </div>
</div> -->

          <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <div class="nav toggle">
                  <div id="timer-text" style="font-size:30px;padding-top:6px;">01:00:00</div>
                </div>
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 15px;">
                    <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url('/assets/images/user.png'); ?>" alt=""><?php echo $this->session->userdata('pengguna')->nama;?>
                    </a>
                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item"  href="<?php echo site_url('/account/ubah_password'); ?>"><i class="fa fa-key pull-right"></i>Change Password</a>
                      <a class="dropdown-item"  href="javascript:void(0);" onclick="logout()"><i class="fa fa-sign-out pull-right"></i>Log Out</a>
                    </div>
                  </li>
                  <?php
                  if($this->session->userdata('pengguna')->level == '1' || $this->session->userdata('pengguna')->level == '5'){
                    ?>
                  <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                       <i class="fa fa-bell-o" style="font-size: 1.2rem;" aria-hidden="true"></i>
                      <span class="badge bg-green" style="font-size: 0.9rem;"><div id="jumlahpr"></div></span>
                    </a>
                    
                    <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                    <div class="ex3">
                         <span id="dropdownpr"></span>
                         </div>
                    </ul>
                    
                  </li>
                  <?php }
                  ?>
                </ul>
              </nav>
            </div>
          </div>