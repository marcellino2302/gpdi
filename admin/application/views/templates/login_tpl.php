<?php 
	if ($this->session->has_userdata('pengguna')){
		redirect('/dashboard');
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>GPdI Bukit Hermon</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/fontawesome-free/css/all.min.css'); ?>">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/icheck-bootstrap/icheck-bootstrap.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/dist/css/adminlte.min.css'); ?>">

  <link rel="shortcut icon" href="<?php echo base_url('../assets/images/gpdi.ico'); ?>">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="javascript:void(0)">Log In</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg"><img src="<?php echo base_url('../assets/images/logo-admin.png'); ?>" alt=""></p>
      <div id="alert"></div>
      <?php if ($this->session->flashdata('status') == 'failed'): ?>
        <div class="alert alert-danger" role="alert">Wrong Username or Password !!</div>
				<?php endif; ?>
      <form action="<?php echo base_url('/site/login'); ?>" method="post">
        <div class="input-group mb-3">
          <input type="username" class="form-control" placeholder="Username" id="username" name="username" value="<?= get_cookie('cookie_username') !== NULL ? get_cookie('cookie_username') : ''; ?>">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" id="password" name="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" name="remember" id="remember" <?= get_cookie('cookie_remember') !== NULL ? 'checked' : ''; ?>>
              <label for="remember">
                Remember Username
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" id="signin" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?php echo base_url('/assets/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('/assets/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('/assets/dist/js/adminlte.min.js'); ?>"></script>
<!-- SweetAlert2 -->
<script src="<?php echo base_url('/assets/sweetalert2/sweetalert2.min.js'); ?>"></script>

<script>
document.getElementById("username").focus();
var html = $("#username").val();
$("#username").focus().val("").val(html);
</script>
</body>
</html>