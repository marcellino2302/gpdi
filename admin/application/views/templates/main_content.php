<div class="content-header">
  <div class="container-fluid">
      <div class="row mb-2">
          <div class="col-sm-12">
              <h1 class="m-0">Dashboard</h1>
          </div>
      </div>
  </div>
</div>

<section class="content">
<div class="container-fluid">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-info">
        <div class="inner">
          <h3><?= $grid->totaljemaat ?></h3>

          <p>Total Jemaat</p>
        </div>
        <div class="icon"><i class="ion ion-person"></i></div>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3><?= $grid->totalcommunity ?></h3>

          <p>Total Community</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-secondary">
        <div class="inner">
          <h3><?= $grid->totalcare ?></h3>

          <p>Total Care (Belum Follow Up)</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-primary">
        <div class="inner">
          <h3><?= $grid->totalevents ?></h3>

          <p>Total Events</p>
        </div>
        <div class="icon">
          <i class="ion ion-calendar  "></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->
</div><!-- /.container-fluid -->
</section>

<section class="content">
<div class="col-12">
  <div class="card card-primary">
    <div class="card-header">
      <h4 class="card-title">5 Event Terdekat</h4>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse">
          <i class="fas fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
            <?php 
              $no = 0;
              foreach($events as $data1){
                $no++;
              ?>
              <div class="col-sm-<?php if($no<=2){echo '6';}else{echo '4';} ?>" 
              style="background-color: #000000BB; 
              background-position: center;
              background-repeat: no-repeat;
              background-size: cover;
              text-shadow: 1px 1px #000000; 
              background-image: url('<?= $data1->image; ?>');
              background-blend-mode: overlay;
              color: #ffffff !important; padding: 20px;">
                <a href="<?php if($data1->image != "") {echo $data1->image; }else{ echo 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQMAAADCCAMAAAB6zFdcAAAAA1BMVEUAAACnej3aAAAASElEQVR4nO3BMQEAAADCoPVPbQwfoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIC3AcUIAAFkqh/QAAAAAElFTkSuQmCC'; } ?>" 
                data-toggle="lightbox" style="color: #ffffff !important;"
                data-title="<?= $data1->nama_event; ?>" data-gallery="gallery">
                <i class="nav-icon far fa-calendar-alt"></i> 
                  <?php if (date_format(date_create($data1->waktu_mulai),"d F Y") != date_format(date_create($data1->waktu_selesai),"d F Y")) { ?>
                  <?= date_format(date_create($data1->waktu_mulai),"d F Y"); ?> - <?= date_format(date_create($data1->waktu_selesai),"d F Y"); ?><br>
                  <?php }else{ ?>
                  <?= date_format(date_create($data1->waktu_mulai),"d F Y"); ?> (<?= date_format(date_create($data1->waktu_mulai),"H:i"); ?> - <?= date_format(date_create($data1->waktu_selesai),"H:i"); ?>)<br>
                  <?php } ?>
                  <i class="nav-icon fa fa-home"></i> Lokasi / Ruangan<br>
                  <h1><?= $data1->nama_event; ?></h1>
                  
                  <?= substr($data1->deskripsi, 0, 250); ?>...
                </a>
              </div>

            <?php } ?>
      
      
        </div>
      </div>
    </div>
  </div>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
    <div class="card card-primary">
      <div class="card-header">
        <h4 class="card-title">Ulang Tahun Jemaat Terdekat</h4>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse">
            <i class="fas fa-minus"></i>
          </button>
        </div>
      </div>
        <div class="card-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Jemaaat</th>
                <th>Tanggal Lahir</th>
                <th>Umur</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $no =0;
                foreach($jemaat as $data1){
                  
                  $month = date('m');
                  $day = date('d');
                  if((date_format(date_create($data1->tgl_lahir),"m") == $month 
                  && date_format(date_create($data1->tgl_lahir),"d") >= $day-1)
                  || date_format(date_create($data1->tgl_lahir),"m") != $month ){
                    $no++;
              ?>
              <tr>
                <td><?php echo $no; ?></td>
                <td><?= $data1->nama ?></td>
                <td><?= date_format(date_create($data1->tgl_lahir),"d F Y"); ?></td>
                <td><?= date('Y')-date_format(date_create($data1->tgl_lahir),"Y"); ?></td>
                <td style="text-align : center;">
                <button class="btn btn-info btn-sm" name="id_ev" style="margin-right: 15px;"
                  data-a="<?= $data1->id; ?>" data-b="<?= $data1->group; ?>" data-c="<?= $data1->nama; ?>"
                  data-d="<?= $data1->tempat_lahir; ?>" data-e="<?= $data1->tgl_lahir; ?>" data-f="<?= $data1->lp; ?>"
                  data-g="<?= $data1->pendidikan_terakhir; ?>" data-h="<?= $data1->pekerjaan; ?>"
                  data-i="<?= $data1->nomor_hp; ?>" data-j="<?= $data1->email; ?>"
                  data-k="<?= $data1->diserahkan_anak; ?>" data-l="<?= $data1->baptis_selam; ?>"
                  data-m="<?= $data1->nikah; ?>" data-n="<?= $data1->smk; ?>" data-o="<?= $data1->kebun; ?>"
                  data-p="<?= $data1->hubungan_kk; ?>" data-toggle="modal" data-target="#modal-edit-jemaat"
                  data-backdrop="static" data-keyboard="false">
                  <i class="fas fa-pencil-alt">
                  </i>
                  Edit
                </button>
                <?php
                if($data1->status == '0'){
                  ?>
                <button class="btn btn-danger btn-sm" data-backdrop="static" data-keyboard="false"
                  data-c="<?= $data1->id; ?>" data-v="<?= $data1->nama; ?>" data-toggle="modal"
                  data-target="#modal-nonaktif-jemaat">
                  <i class="fas fa-toggle-off">
                  </i>
                  Non Aktif
                </button>
                <?php
                }else{
                  ?>
                <button class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false"
                  data-c="<?= $data1->id; ?>" data-v="<?= $data1->nama; ?>" data-toggle="modal"
                  data-target="#modal-aktif-jemaat">
                  <i class="fas fa-toggle-on">
                  </i>
                  Aktif
                </button>
                <?php
                }
                ?>
                </td>
              </tr>

              <?php }} ?>


            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </div>
  </div>
  <!-- /.row (main row) -->
</div><!-- /.container-fluid -->

    
</section>

<div class="modal fade" id="modal-aktif-jemaat">
<div class="modal-dialog modal-dialog-centered">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>Apakah anda yakin aktifkan Jemaat ini ?<br>
        Nama Jemaat &nbsp; : <b id="nama_jemaat_aktif"></b><br>
    </div>
    <form action="<?= site_url('master/jemaat/aktif');?>" method="post">
      <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user;?>" />
      <input type="hidden" name="id_jemaat_aktif" id="id_jemaat_aktif" value="">

      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-danger">Yes</button>
      </div>
    </form>
  </div>
  <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-nonaktif-jemaat">
<div class="modal-dialog modal-dialog-centered">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="exampleModalLabel">PERHATIAN !!!</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <form action="<?= site_url('master/jemaat/nonaktif');?>" method="post">
      <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user;?>" />

      <div class="card-body">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <p>Apakah anda yakin non aktifkan Jemaat ini ?<br>
                Nama Jemaat &nbsp; : <b id="nama_jemaat_nonaktif"></b><br><br>
                <label>Keterangan</label>
                <select class="form-control" name="keterangan_nonaktif" id="keterangan_nonaktif" required>
                  <option value="">-- Pilih Keterangan --</option>
                  <option value="1">Meninggal</option>
                  <option value="2">Pindah Gereja</option>
                  <option value="3">Lain-lain</option>
                </select>
            </div>
          </div>
        </div>
      </div>
      <input type="hidden" name="id_jemaat_nonaktif" id="id_jemaat_nonaktif" value="">
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-danger">Yes</button>
      </div>
    </form>
  </div>
  <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.content -->
<div class="modal fade" id="modal-edit-jemaat">
<div class="modal-dialog modal-dialog-centered">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="exampleModalLabel">Edit Data Jemaat</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <form action="<?= site_url('master/jemaat/save_edit');?>" method="post" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="form-group row">
          <label for="nama_edit" class="col-sm-5 col-form-label">ID Keluarga (*)</label>
          <div class="col-sm">
            <input type="text" class="form-control" id="id_keluarga" name="id_keluarga" placeholder="" value=""
              required>
          </div>
        </div>

        <div class="form-group row">
          <label for="nama_edit" class="col-sm-5 col-form-label">Nama Jemaat (*)</label>
          <div class="col-sm">
            <input type="text" class="form-control" id="nama_edit" name="nama_edit" placeholder="Masukan Nama"
              value="" required>
          </div>
        </div>

        <div class="form-group row">
          <label for="hubungan_kk_edit" class="col-sm-5 col-form-label">Hubungan di Kartu Keluarga (*)</label>
          <div class="col-sm">
            <select type="text" class="form-control" id="hubungan_kk_edit" name="hubungan_kk_edit" value="" required>
              <option hidden selected>Pilih Hubungan</option>
              <option value="Kepala Keluarga">Kepala Keluarga</option>
              <option value="Pasangan">Pasangan</option>
              <option value="anak">Anak</option>
              <option value="Anak Mantu">Anak Mantu</option>
              <option value="Orang Lain Serumah">Orang Lain Serumah</option>
              <option value="Lain-lain">Lain-lain</option>
            </select>
          </div>
        </div>

        <div class="form-group row">
          <label for="tempat_lahir_edit" class="col-sm-5 col-form-label">Tempat Lahir (*)</label>
          <div class="col-sm">
            <input type="text" class="form-control" id="tempat_lahir_edit" name="tempat_lahir_edit"
              placeholder="Masukan Tempat Lahir" value="" required>
          </div>
        </div>

        <div class="form-group row">
          <label for="tanggal_lahir_edit" class="col-sm-5 col-form-label">Tanggal Lahir (*)</label>
          <div class="col-sm">
            <input type="date" class="form-control" id="tanggal_lahir_edit" name="tanggal_lahir_edit"
              placeholder="Masukan Tanggal Lahir" value="">
          </div>
        </div>

        <div class="form-group row">
          <label for="jenis_kelamin_edit" class="col-sm-5 col-form-label">Jenis Kelamin (*)</label>
          <div class="col-sm">
            <select type="text" class="form-control" id="jenis_kelamin_edit" name="jenis_kelamin_edit"
              placeholder="Masukan Jenis Kelamin" value="" required>
              <option selected hidden>Pilih Jenis Kelamin</option>
              <option value="L">Laki-Laki</option>
              <option value="P">Perempuan</option>
            </select>
          </div>
        </div>

        <div class="form-group row">
          <label for="hp_edit" class="col-sm-5 col-form-label">No Hp (*)</label>
          <div class="col-sm">
            <input type="text" class="form-control" id="hp_edit" name="hp_edit" placeholder="Masukan Nomor HP"
              value="" data-inputmask='"mask": "9999999999999"' data-mask required>
          </div>
        </div>

        <div class="form-group row">
          <label for="email_edit" class="col-sm-5 col-form-label">Email (*)</label>
          <div class="col-sm">
            <input type="text" class="form-control" id="email_edit" name="email_edit"
              placeholder="Masukan Alamat Email" value="" required>
          </div>
        </div>

        <div class="form-group row">
          <label for="pendidikan_edit" class="col-sm-5 col-form-label">Pendidikan Terakhir (*)</label>
          <div class="col-sm">
            <select type="text" class="form-control" id="pendidikan_edit" name="pendidikan_edit"
              placeholder="Masukan Pendidikan Terakhir" value="" required>
              <option hidden selected>Pilih Pendidikan Terakhir</option>
              <option value="Tidak / Belum Sekolah">Tidak / Belum Sekolah</option>
              <option value="Tidak Tamat SD / Sederajat">Tidak Tamat SD / Sederajat</option>
              <option value="SD / Sederajat">Tamat SD / Sederajat</option>
              <option value="SMP / Sederajat">SMP / Sederajat</option>
              <option value="SMA / Sederajat">SMA / Sederajat</option>
              <option value="Diploma 1/2">Diploma 1/2</option>
              <option value="Akademi / Diploma 3 / Sarjana Muda">Akademi / Diploma 3 / Sarjana Muda</option>
              <option value="Diploma 4 / Strata 1">Diploma 4 / Strata 1</option>
              <option value="Strata 2">Strata 2</option>
              <option value="Strata 3">Strata 3</option>
            </select>
          </div>
        </div>

        <div class="form-group row">
          <label for="pekerjaan_edit" class="col-sm-5 col-form-label">Pekerjaan(*)</label>
          <div class="col-sm">
            <input type="text" class="form-control" id="pekerjaan_edit" name="pekerjaan_edit"
              placeholder="Masukan Pekerjaan" value="" required>
          </div>
        </div>

        <div class="form-group row">
          <label for="baptis_selam_edit" class="col-sm-5 col-form-label">Dibaptis Selam</label>
          <div class="col-sm">
            <input type="date" class="form-control" id="baptis_selam_edit" name="baptis_selam_edit"
              placeholder="Masukan Tanggal Baptis" value="">
          </div>
        </div>

        <div class="form-group row">
          <label for="smk_edit" class="col-sm-5 col-form-label">Angkatan SMK</label>
          <div class="col-sm">
            <input type="text" class="form-control" id="smk_edit" name="smk_edit" placeholder="Masukan Angkatan SMK"
              value="">
          </div>
        </div>

        <div class="form-group row">
          <label for="kebun_edit" class="col-sm-5 col-form-label">Nama KeBun</label>
          <div class="col-sm">
            <input type="text" class="form-control" id="kebun_edit" name="kebun_edit" placeholder="Masukan KeBun"
              value="">
          </div>
        </div>

        <div class="form-group row">
          <label for="tanggal_nikah_edit" class="col-sm-5 col-form-label">Tanggal Menikah</label>
          <div class="col-sm">
            <input type="date" class="form-control" id="tanggal_nikah_edit" name="tanggal_nikah_edit"
              placeholder="Masukan Tanggal Menikah" value="">
          </div>
        </div>

        <div class="form-group row">
          <label for="diserahkan_anak_edit" class="col-sm-5 col-form-label">Diserahkan Anak</label>
          <div class="col-sm">
            <input type="date" class="form-control" id="diserahkan_anak_edit" name="diserahkan_anak_edit"
              placeholder="Masukan Tanggal Diserahkan Anak" value="">
          </div>
        </div>

        <input type="hidden" name="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">
        <input type="hidden" name="id_jemaat_edit" id="id_jemaat_edit" value="">
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </div>
    </form>
  </div>
  <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
$(function () {
$("#example1").DataTable({
  "responsive": true,
  "lengthChange": false,
  "autoWidth": false,
  "paging": true,
  // "sorting": false,
  // "buttons": ["copy", "csv", "excel", "pdf", "print"],
}).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

});

$('#modal-aktif-jemaat').on('show.bs.modal', function (event) {
var button = $(event.relatedTarget); // Button that triggered the modal
var recipient_c = button.data('c');

var recipient_v = button.data('v');

modal.find('.id_jemaat_aktif').val(recipient_c);
document.getElementById("id_jemaat_aktif").value = recipient_c;

document.getElementById("nama_jemaat_aktif").innerHTML = recipient_v;
})

$('#modal-nonaktif-jemaat').on('show.bs.modal', function (event) {
var button = $(event.relatedTarget); // Button that triggered the modal
var recipient_c = button.data('c');

var recipient_v = button.data('v');

// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
var modal = $(this);
modal.find('.id_jemaat_nonaktif').val(recipient_c);
document.getElementById("id_jemaat_nonaktif").value = recipient_c;

document.getElementById("nama_jemaat_nonaktif").innerHTML = recipient_v;
})

$('#modal-edit-jemaat').on('show.bs.modal', function (event) {
var button = $(event.relatedTarget); // Button that triggered the modal
var recipient_a = button.data('a');
var recipient_b = button.data('b');
var recipient_c = button.data('c');
var recipient_d = button.data('d');
var recipient_e = button.data('e');
var recipient_f = button.data('f');
var recipient_g = button.data('g');
var recipient_h = button.data('h');
var recipient_i = button.data('i');
var recipient_j = button.data('j');

var recipient_k = button.data('k');
var recipient_l = button.data('l');
var recipient_m = button.data('m');
var recipient_n = button.data('n');
var recipient_o = button.data('o');
var recipient_p = button.data('p');
// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
var modal = $(this);
document.getElementById("id_jemaat_edit").value = recipient_a;

document.getElementById("id_keluarga").value = recipient_b;
document.getElementById("nama_edit").value = recipient_c;

modal.find('.hubungan_kk_edit').val(recipient_p);
document.getElementById("hubungan_kk_edit").value = recipient_p;

document.getElementById("tempat_lahir_edit").value = recipient_d;

var dateX = new Date(recipient_e);
const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

dayX = ("0" + dateX.getDate()).slice(-2);
monthX = ("0" + (dateX.getMonth() + 1)).slice(-2);
yearX = dateX.getFullYear();
modal.find('.tanggal_lahir_edit').val(yearX + '-' + monthX + '-' + dayX);
$("#tanggal_lahir_edit").val(yearX + '-' + monthX + '-' + dayX);


if (recipient_f == "l") {
  modal.find('.jenis_kelamin_edit').val("L");
  document.getElementById("jenis_kelamin_edit").value = "L";
} else if (recipient_f == "p") {
  modal.find('.jenis_kelamin_edit').val("P");
  document.getElementById("jenis_kelamin_edit").value = "P";
} else {
  modal.find('.jenis_kelamin_edit').val(recipient_f);
  document.getElementById("jenis_kelamin_edit").value = recipient_f;
}

modal.find('.hp_edit').val(recipient_i);
document.getElementById("hp_edit").value = recipient_i;

modal.find('.email_edit').val(recipient_j);
document.getElementById("email_edit").value = recipient_j;

modal.find('.pendidikan_edit').val(recipient_g);
document.getElementById("pendidikan_edit").value = recipient_g;

modal.find('.pekerjaan_edit').val(recipient_h);
document.getElementById("pekerjaan_edit").value = recipient_h;

// -------------------------

var dateX1 = new Date(recipient_l);
const monthNames1 = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
dayX1 = ("0" + dateX1.getDate()).slice(-2);
monthX1 = ("0" + (dateX1.getMonth() + 1)).slice(-2);
yearX1 = dateX1.getFullYear();
modal.find('.baptis_selam_edit').val(yearX1 + '-' + monthX1 + '-' + dayX1);
$("#baptis_selam_edit").val(yearX1 + '-' + monthX1 + '-' + dayX1);


var dateX3 = new Date(recipient_k);
const monthNames3 = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
dayX3 = ("0" + dateX3.getDate()).slice(-2);
monthX3 = ("0" + (dateX3.getMonth() + 1)).slice(-2);
yearX3 = dateX3.getFullYear();
modal.find('.diserahkan_anak_edit').val(yearX3 + '-' + monthX3 + '-' + dayX3);
$("#diserahkan_anak_edit").val(yearX3 + '-' + monthX3 + '-' + dayX3);

var dateX2 = new Date(recipient_m);
const monthNames2 = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
dayX2 = ("0" + dateX2.getDate()).slice(-2);
monthX2 = ("0" + (dateX2.getMonth() + 1)).slice(-2);
yearX2 = dateX2.getFullYear();
modal.find('.tanggal_nikah_edit').val(yearX2 + '-' + monthX2 + '-' + dayX2);
$("#tanggal_nikah_edit").val(yearX2 + '-' + monthX2 + '-' + dayX2);

modal.find('.smk_edit').val(recipient_m);
document.getElementById("smk_edit").value = recipient_n;

modal.find('.kebun_edit').val(recipient_n);
document.getElementById("kebun_edit").value = recipient_o;
})

</script>