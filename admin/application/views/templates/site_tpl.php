<?php 
	if (!$this->session->has_userdata('pengguna')){
		redirect('site');
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>GPdI Bukit Hermon</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/fontawesome-free/css/all.min.css'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?php if (current_url()!=site_url('events/calendar')) { echo base_url('/assets/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css'); } ?>">

  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/icheck-bootstrap/icheck-bootstrap.min.css'); ?>">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/jqvmap/jqvmap.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/dist/css/adminlte.min.css'); ?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/overlayScrollbars/css/OverlayScrollbars.min.css'); ?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/daterangepicker/daterangepicker.css'); ?>">
  <!-- Color picker -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css'); ?>">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/summernote/summernote-bs4.min.css'); ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/datatables-bs4/css/dataTables.bootstrap4.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/datatables-responsive/css/responsive.bootstrap4.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/datatables-buttons/css/buttons.bootstrap4.min.css'); ?>">

  <link rel="shortcut icon" href="<?php echo base_url('../assets/images/gpdi.ico'); ?>">
  
  <!-- Ekko Lightbox -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/ekko-lightbox/ekko-lightbox.css'); ?>">

  <!-- fullCalendar -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/fullcalendar/main.css'); ?>">

  <script type="text/javascript" src="<?php echo base_url('/assets/sweetalert2/sweetalert2.all.min.js'); ?>"></script>
  <!-- jQuery -->
  <script src="<?php echo base_url('/assets/jquery/jquery.min.js'); ?>"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?php echo base_url('/assets/jquery-ui/jquery-ui.min.js'); ?>"></script>
  <link rel="stylesheet" href="<?php echo base_url('/assets/jquery-ui/jquery-ui.min.css'); ?>">
  
  <!-- JQUERY Image Uploader -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/jquery-imageuploader/jquery.imagesloader.css'); ?>">
  <script type="text/javascript" src="<?php echo base_url('/assets/jquery-imageuploader/jquery.imagesloader.js'); ?>"></script>
  
  <style>
        .ekko-lightbox iframe img {
          width: 100%;
        }
        .none {
            display: none;
        }

        .dtr-data {
            white-space: pre;
            display: block;
        }

        .dtr-data .btn {
            white-space: initial;
        }

        .xx:before {
            display: none !important;
        }
        table.dataTable.dtr-inline.collapsed>tbody>tr>td.dtr-control:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th.dtr-control:before{
            left: 45% !important;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed">

<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="<?php echo base_url('../assets/images/logo-admin.png'); ?>" alt="GPdILogo">
  </div>
  
          <!-- /sidebar menu -->
          
        </div>
      </div>

      <!-- top navigation -->
      <!-- /top navigation -->

      <!-- page content -->
      
      <div class="right_col" role="main">
        <div class="">
          
        </div>
      </div>
    <?php echo $this->load->view($left_content); ?>

    <div class="content-wrapper">
      <?php echo $this->load->view($content); ?>
    </div>

    <footer class="main-footer">
      <strong>Copyright &copy; 2022 <a href="https://gpdibukithermon.org/">GPdI Bukit Hermon Cimahi</a>.</strong>
      All rights reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo base_url('/assets/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
  <!-- ChartJS -->
  <script src="<?php echo base_url('/assets/chart.js/Chart.min.js'); ?>"></script>
  <!-- Sparkline -->
  <script src="<?php echo base_url('/assets/sparklines/sparkline.js'); ?>"></script>
  <!-- JQVMap -->
  <script src="<?php echo base_url('/assets/jqvmap/jquery.vmap.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/jqvmap/maps/jquery.vmap.usa.js'); ?>"></script>
  <!-- jQuery Knob Chart -->
  <script src="<?php echo base_url('/assets/jquery-knob/jquery.knob.min.js'); ?>"></script>
  <!-- daterangepicker -->
  <script src="<?php echo base_url('/assets/moment/moment.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/daterangepicker/daterangepicker.js'); ?>"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="<?php if (current_url()!=site_url('events/calendar')) { echo base_url('/assets/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js'); } ?>"></script>
  <!-- Summernote -->
  <script src="<?php echo base_url('/assets/summernote/summernote-bs4.min.js'); ?>"></script>
  <!-- overlayScrollbars -->
  <script src="<?php echo base_url('/assets/overlayScrollbars/js/jquery.overlayScrollbars.min.js'); ?>"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url('/assets/dist/js/adminlte.js'); ?>"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo base_url('/assets/dist/js/demo.js'); ?>"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="<?php echo base_url('/assets/dist/js/pages/dashboard.js'); ?>"></script>
  <!-- DataTables  & Plugins -->
  <script src="<?php echo base_url('/assets/datatables/jquery.dataTables.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/datatables-bs4/js/dataTables.bootstrap4.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/datatables-responsive/js/dataTables.responsive.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/datatables-responsive/js/responsive.bootstrap4.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/datatables-buttons/js/dataTables.buttons.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/datatables-buttons/js/buttons.bootstrap4.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/jszip/jszip.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/pdfmake/pdfmake.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/pdfmake/vfs_fonts.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/datatables-buttons/js/buttons.html5.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/datatables-buttons/js/buttons.print.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/datatables-buttons/js/buttons.colVis.min.js'); ?>"></script>

  <!-- Select2 -->
  <script src="<?php echo base_url('/assets/select2/js/select2.full.min.js'); ?>"></script>
  <!-- Bootstrap4 Duallistbox -->
  <script src="<?php echo base_url('/assets/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js'); ?>"></script>
  <!-- InputMask -->
  <script src="<?php echo base_url('/assets/moment/moment.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/inputmask/jquery.inputmask.min.js'); ?>"></script>


  <!-- fullCalendar 2.2.5 -->
  <script src="<?php echo base_url('/assets/moment/moment.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/fullcalendar/main.js'); ?>"></script>

  <!-- Color Picker -->
  <script src="<?php echo base_url('/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js'); ?>"></script>

  <!-- Ekko Lightbox -->
  <script src="<?php echo base_url('/assets/ekko-lightbox/ekko-lightbox.min.js'); ?>"></script>

<script>
    $(function () {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });

    if ($('.filter-container').length) {
      $('.filter-container').filterizr({gutterPixels: 3});
      $('.btn[data-filter]').on('click', function() {
        $('.btn[data-filter]').removeClass('active');
        $(this).addClass('active');
      });
    }

  })
  $( "#logout" ).click(function() {
    Swal.fire({
      title: 'Logout',
      text: "Are you sure want to logout?",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = '<?php echo base_url('site/logout'); ?>';
      }
    })
  });
</script>