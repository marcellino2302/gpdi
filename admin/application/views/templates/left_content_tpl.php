<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
    
  </ul>

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <!-- Notifications Dropdown Menu -->
    <li class="nav-item dropdown">
      <a class="nav-link" data-toggle="dropdown" href="#">
        <i class="fa fa-cog"></i>
      </a>
      <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        <span class="dropdown-item dropdown-header">Settings</span>
        <div class="dropdown-divider"></div>
        <a href="<?php echo site_url('changepass'); ?>" class="dropdown-item">
          <i class="nav-icon fas fa-lock mr-2"></i> Change Password
        </a>
        <div class="dropdown-divider"></div>
        <a href="javascript:void(0)" class="dropdown-item" id="logout" onclick="logoutsession()">
          <i class="nav-icon fas fa-power-off mr-2"></i> Logout
        </a>
      </div>
    </li>
    
  </ul>

</nav>
<!-- /.navbar -->

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="javascript:void(0)" class="brand-link">
    <img src="<?php echo base_url('../assets/images/gpdi.ico'); ?>" alt="GPdi Logo" class="brand-image elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">GPdI Bukit Hermon</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?php echo base_url('/assets/dist/img/user.jpg'); ?>" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="javascript:void(0)" class="d-block"><?php echo $this->session->userdata('pengguna')->nama_user;?></a>
      </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      
      <?php 
      $singleMenu = array("dashboard","give");
      $open = 0;
        for($i=0;$i<count($this->session->userdata('menu'));$i++){
          if($this->session->userdata('menu')[$i]->akses == '1'){
            if($this->session->userdata('menu')[$i]->level == 'main_menu'){
              // $kondisi = "";
              // $counter = 0;
              // for($j=$i;$j<count($this->session->userdata('menu'));$j++){
              //   if($this->session->userdata('menu')[$j+1]->level != 'sub_menu'){
              //     break;
              //   }
              //   if($counter == 0){
              //     $kondisi .= "current_url()==site_url('".$this->session->userdata('menu')[$j+1]->url."')";
              //   }else{
              //     if($this->session->userdata('menu')[$j+1]->level == 'sub_menu'){
              //       $kondisi .= " || current_url()==site_url('".$this->session->userdata('menu')[$j+1]->url."')";
              //     }
              //   }
              //   $counter++;
              // }
              if(count(explode('/',$this->session->userdata('menu')[$i]->url))==1 and 0>=count(array_intersect(array_map('strtolower', explode('/',$this->session->userdata('menu')[$i]->url)), $singleMenu))){

                if (strpos(current_url(),$this->session->userdata('menu')[$i]->url)){
                  $open = 1;
                }
                ?>

                <li class="nav-item <?=  $open == 1 ? 'menu-is-opening menu-open' : '' ?>">
                  <a href="#" class="nav-link">
                    <i class="nav-icon <?= $this->session->userdata('menu')[$i]->icon ?>"></i>
                    <p>
                      <?= $this->session->userdata('menu')[$i]->nama_menu ?>
                      <i class="fas fa-angle-left right"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                <?php
              }else{
                ?>
                <li class="nav-item">
                  <a href="<?php echo site_url($this->session->userdata('menu')[$i]->url); ?>" class="nav-link <?= current_url()==site_url($this->session->userdata('menu')[$i]->url) ? 'active' : '' ?>">
                    <i class="nav-icon <?= $this->session->userdata('menu')[$i]->icon ?>"></i>
                    <p>
                      <?= $this->session->userdata('menu')[$i]->nama_menu ?>
                    </p>
                  </a>
                </li>
                <?php
              }
            }else if($this->session->userdata('menu')[$i]->level == 'sub_menu'){
              ?>
              <li class="nav-item">
                <a href="<?php echo site_url($this->session->userdata('menu')[$i]->url); ?>" class="nav-link <?= current_url()==site_url($this->session->userdata('menu')[$i]->url) ? 'active' : '' ?>">
                  <i class="far fa-circle <?= $this->session->userdata('menu')[$i]->icon ?>"></i>
                  <p>&nbsp;<?= $this->session->userdata('menu')[$i]->nama_menu ?></p>
                </a>
              </li>
              <?php
              if(isset($this->session->userdata('menu')[$i+1]->level)){
                if($this->session->userdata('menu')[$i+1]->level != 'sub_menu'){
                  echo "</ul></li>";
                }
              }
            }else if($this->session->userdata('menu')[$i]->level == 'header_menu'){
              echo "<li class='nav-header'>".$this->session->userdata('menu')[$i]->nama_menu."</li>";
            }
            $open = 0;
          }else{
            if($this->session->userdata('menu')[$i]->level == 'header_menu'){
              echo "<li class='nav-header'>".$this->session->userdata('menu')[$i]->nama_menu."</li>";
            }
          }
        }
        

      ?>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>