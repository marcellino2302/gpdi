 <!-- Content Header (Page header) -->
 <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-12">
              <h1 class="m-0">Change Password
              </h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                
                <!-- /.card-header -->
                <div class="card-body">
                    <form class="form-horizontal" <?= site_url('/changepass/save'); ?>" method="post" id="frmchange">
                        <div class="form-group row">
                            <label for="oldpass" class="col-sm-2 col-form-label">Old Password (*)</label>
                            <div class="col-sm-10">
                                <div class="input-group" data-target-input="nearest">
                                    <input type="password" class="form-control" id="oldpass" name="oldpass">
                                    <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker" onclick="oldpassicon()">
                                        <div class="input-group-text"><i class="fa fa-eye-slash" id="oldpassicon"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="newpass" class="col-sm-2 col-form-label">New Password (*)</label>
                                <div class="col-sm-10">
                                    <div class="input-group" data-target-input="nearest">
                                        <input type="password" class="form-control" id="newpass" name="newpass">
                                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker" onclick="newpassicon()">
                                            <div class="input-group-text"><i class="fa fa-eye-slash" id="newpassicon"></i></div>
                                        </div>
                                    </div>
                                    <div id="popover-password">
                                        <p>Password Strength: <span id="result"> </span><input type="hidden" id="strength" name="strength" value="0"></p>
                                        <div class="progress" style="height: 5px;">
                                            <div id="password-strength" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                            </div>
                                        </div>
                                        <ul class="list-unstyled">
                                            <li class=""><span class="low-upper-case"><i class="fa fa-times" aria-hidden="true"></i></span>&nbsp; 1 lowercase &amp; 1 uppercase</li>
                                            <li class=""><span class="one-number"><i class="fa fa-times" aria-hidden="true"></i></span> &nbsp;1 number (0-9)</li>
                                            <li class=""><span class="one-special-char"><i class="fa fa-times" aria-hidden="true"></i></span> &nbsp;1 Special Character (!@#$%^&*).</li>
                                            <li class=""><span class="eight-character"><i class="fa fa-times" aria-hidden="true"></i></span>&nbsp; Atleast 8 Character</li>
                                        </ul>
                                    </div>


                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="confirmpass" class="col-sm-2 col-form-label">Confirm Password (*)</label>
                                <div class="col-sm-10">
                                    <div class="input-group" data-target-input="nearest">
                                        <input type="password" class="form-control" id="confirmpass" name="confirmpass">
                                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker" onclick="confirmpassicon()">
                                            <div class="input-group-text"><i class="fa fa-eye-slash" id="confirmpassicon"></i></div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <input type="hidden" name="id_user" id="id_user" value="<?=$this->session->userdata('pengguna')->id_user?>">

                        <div class="form-group row">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <button type="button" class="col-sm-2 btn btn-primary" onclick="validasinya()">Submit</button>
                        </div>
                    </div>
                </div>
                </form>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
          </div>
          <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->

<script>
    function validasinya(){
        var oldpass = $('#oldpass').val();
        var newpass = $('#newpass').val();
        var strength = $('#strength').val();
        var confirmpass = $('#confirmpass').val();

        if(oldpass == ""){
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Old Password Required',
                showConfirmButton: false,
                timer: 1500
            })
        }else{
            $.ajax({
                type: "POST",
                url: "<?= base_url('/changepass/cekpassword') ?>",
                data: {
                    'pass': $("#oldpass").val(),
                    'id_user': $("#id_user").val(),
                },
                dataType: "json",
                success: function (response) {
                    if(response == 0){
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Old Password Wrong',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }else{
                         if(newpass == ""){
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'New Password Required',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }else if(strength < 3){
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Password Strength Week',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }else if(confirmpass == ""){
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Confirm Password Required',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }else if(newpass != confirmpass){
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Recheck Confirm Password',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }else{
                            $.ajax({
                                type: "POST",
                                url: "<?= base_url('/changepass/save') ?>",
                                data: {
                                    'oldpass': $("#oldpass").val(),
                                    'newpass': $("#newpass").val(),
                                    'confirmpass': $("#confirmpass").val(),
                                    'id_user': $("#id_user").val(),
                                },
                                dataType: "json",
                                success: function (response) {
                                    location.reload();
                                }
                            });
                        }
                    }
                }
            });
        }
    }

    $('#newpass').keyup(function() {
        var password = $('#newpass').val();
        if (checkStrength(password) == false) {
            $('#sign-up').attr('disabled', true);
        }
    });

   function oldpassicon(){
        if($('#oldpassicon').attr('class') == 'fa fa-eye-slash'){
            $('#oldpassicon').removeClass('fa-eye-slash').addClass('fa-eye');
            $('#oldpass').attr('type','text');
        }else{
            $('#oldpassicon').removeClass('fa-eye').addClass('fa-eye-slash');
            $('#oldpass').attr('type','password');
        }
   }

   function newpassicon(){
        if($('#newpassicon').attr('class') == 'fa fa-eye-slash'){
            $('#newpassicon').removeClass('fa-eye-slash').addClass('fa-eye');
            $('#newpass').attr('type','text');
        }else{
            $('#newpassicon').removeClass('fa-eye').addClass('fa-eye-slash');
            $('#newpass').attr('type','password');
        }
   }

   function confirmpassicon(){
        if($('#confirmpassicon').attr('class') == 'fa fa-eye-slash'){
            $('#confirmpassicon').removeClass('fa-eye-slash').addClass('fa-eye');
            $('#confirmpass').attr('type','text');
        }else{
            $('#confirmpassicon').removeClass('fa-eye').addClass('fa-eye-slash');
            $('#confirmpass').attr('type','password');
        }
   }

    function checkStrength(password) {
        var strength = 0;
        //If password contains both lower and uppercase characters, increase strength value.
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
            strength += 1;
            $('.low-upper-case').addClass('text-success');
            $('.low-upper-case i').removeClass('fa-times').addClass('fa-check');
            $('#popover-password-top').addClass('hide');
        } else {
            $('.low-upper-case').removeClass('text-success');
            $('.low-upper-case i').addClass('fa-times').removeClass('fa-check');
            $('#popover-password-top').removeClass('hide');
        }

        //If it has numbers and characters, increase strength value.
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) {
            strength += 1;
            $('.one-number').addClass('text-success');
            $('.one-number i').removeClass('fa-times').addClass('fa-check');
            $('#popover-password-top').addClass('hide');

        } else {
            $('.one-number').removeClass('text-success');
            $('.one-number i').addClass('fa-times').removeClass('fa-check');
            $('#popover-password-top').removeClass('hide');
        }

        //If it has one special character, increase strength value.
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
            strength += 1;
            $('.one-special-char').addClass('text-success');
            $('.one-special-char i').removeClass('fa-times').addClass('fa-check');
            $('#popover-password-top').addClass('hide');

        } else {
            $('.one-special-char').removeClass('text-success');
            $('.one-special-char i').addClass('fa-times').removeClass('fa-check');
            $('#popover-password-top').removeClass('hide');
        }

        if (password.length > 7) {
            strength += 1;
            $('.eight-character').addClass('text-success');
            $('.eight-character i').removeClass('fa-times').addClass('fa-check');
            $('#popover-password-top').addClass('hide');

        } else {
            $('.eight-character').removeClass('text-success');
            $('.eight-character i').addClass('fa-times').removeClass('fa-check');
            $('#popover-password-top').removeClass('hide');
        }


        $("#strength").val(strength);


        // If value is less than 2

        if (strength < 2) {
            $('#result').removeClass()
            $('#password-strength').addClass('progress-bar-danger');
            $('#result').addClass('text-danger').text('Very Week');
            $('#password-strength').css('width', '10%');
        } else if (strength == 2) {
            $('#result').addClass('good');
            $('#password-strength').removeClass('progress-bar-danger');
            $('#password-strength').addClass('progress-bar-warning');
            $('#result').addClass('text-warning').text('Week')
            $('#password-strength').css('width', '60%');
            return 'Week'
        } else if (strength == 3 || strength == 4) {
            $('#result').removeClass()
            $('#result').addClass('strong');
            $('#password-strength').removeClass('progress-bar-warning');
            $('#password-strength').addClass('progress-bar-success');
            $('#result').addClass('text-success').text('Strength');
            $('#password-strength').css('width', '100%');

            return 'Strong'
        }


    }
</script>