<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_dashboard extends CI_Model {
    /*
	public	$propose   	  = '';
	public	$unpaid   	  = '';
	*/
	
	public function dashboardHome(){
		return $this->db->query("  SELECT (SELECT COUNT(*) FROM wb_jemaat) AS totaljemaat, 
		(SELECT COUNT(*) FROM wb_komsel WHERE deleted_at IS NULL AND id<>30) AS totalcommunity, 
		(SELECT COUNT(*) FROM wb_events WHERE deleted_at IS NULL) AS totalevents, 
		(SELECT (SELECT COUNT(*) FROM wb_penyerahananak WHERE deleted_at IS NULL 
		AND status_penyerahananak=0)+(SELECT COUNT(*) FROM wb_baptisanair 
		WHERE deleted_at IS NULL AND status_baptis=0)+(SELECT COUNT(*) FROM wb_pranikah 
		WHERE deleted_at IS NULL AND status_pranikah=0)+(SELECT COUNT(*) FROM wb_konseling 
		WHERE deleted_at IS NULL AND status_konseling=0)+(SELECT COUNT(*) FROM wb_visitasi 
		WHERE deleted_at IS NULL AND status_visitasi=0)+(SELECT COUNT(*) FROM wb_permohonandoa 
		WHERE deleted_at IS NULL AND status_permohonandoa=0)) AS totalcare ")->row();
	}

	public function get_index(){
		$date = date('Y-m-d');
		$result = $this->db->query("SELECT * FROM `wb_calendars` 
		WHERE deleted_at IS NULL and waktu_mulai >= '$date'
		ORDER BY `waktu_mulai` ASC
		Limit 5")->result();
		return $result;

	}

	public function get_jemaat(){
		$month = date('m');
		$result = $this->db->query("SELECT * FROM `wb_jemaat_fix` 
		WHERE `status`=0 AND MONTH(tgl_lahir) = $month or MONTH(tgl_lahir) = $month+1
		ORDER BY  MONTH(tgl_lahir) ASC,  DAY(tgl_lahir) ASC
		")->result();
		return $result;

	}
}

/* End of file Model_dashboard.php */
/* Location: ./application/models/Model_dashboard.php */