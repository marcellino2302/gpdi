<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelchangepass extends CI_Model {

	public function simpan($data){
		//array data
		$array = array(
			'password' 		    => md5($data['confirm']),
			'lastmodified' 		=> date("Y-m-d h:i:s")	,
			'user' 		        => $data['id_user'],
		);
		//set value
		$this->db->set($array);
		//where
		$this->db->where('id_user', $data['id_user']);
		//update	
		return $this->db->update('ad_user');
	}

	public function cekdata($pass, $id_user)
	{
		$result = $this->db->get_where('ad_user',array('deleted'=>'0', 'id_user'=>$id_user, 'password'=>md5($pass)))
						   ->num_rows();
		//return query result
		return $result;
	}
}