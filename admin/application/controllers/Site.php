<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends MX_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('cookie');
	}
	
	public function index(){
		$this->page->template('login_tpl');
		$this->page->view();
	}
	
	public function login(){
		$username = $this->input->post('username');
		$pass 	  = $this->input->post('password');
		$remember = $this->input->post('remember');
		$password = MD5($pass);
		
		$src = $this->db->get_where('ad_user', array (
			'username' 	=> $username,
			'password'	=> $password,
		));

		if($remember=='on'){
			$cookie = array(
				'name'   => 'cookie_username',
				'value'  => $username,                            
				'expire' => '3600',                                                                                   
				'secure' => FALSE 
			);
	   		set_cookie($cookie);   

			$cookie = array(
				'name'   => 'cookie_remember',
				'value'  => $remember,                            
				'expire' => '3600',                                                                                   
				'secure' => FALSE 
			);
	   		set_cookie($cookie);   
		}else{
			delete_cookie("cookie_username");
			delete_cookie("cookie_remember");
		}
		
		if ($src->num_rows() == 0) {
			$this->session->set_flashdata('status', 'failed');
			redirect(base_url());
		}
		else {
			$pengguna = $src->row();
			$this->session->set_userdata('pengguna', $pengguna);

			$menu = $this->db->query("SELECT mn.id_menu AS menu, mn.nama_menu, mn.url, mn.hide, mn.icon, mn.akses AS aksesmenu, mn.tambah AS tambahmenu, mn.edit AS editmenu, mn.post AS postmenu, mn.print AS printmenu, mn.hapus AS hapusmenu, mn.level, ak.* FROM wb_menu mn 
			LEFT JOIN wb_akses ak ON mn.id_menu=ak.id_menu AND ak.id_level = '".$pengguna->id_level."'
			WHERE mn.hide=0 AND mn.deleted_at IS NULL
			ORDER BY mn.no_urut ASC")->result();
			$this->session->set_userdata('menu', $menu);

			redirect(site_url('/dashboard'));
		}
	}
	
	public function logout(){
		unset($_SESSION['pengguna']);
		$this->session->sess_destroy();
		redirect(base_url());
	}
	
}

/* End of file site.php */
/* Location: ./application/controllers/site.php */