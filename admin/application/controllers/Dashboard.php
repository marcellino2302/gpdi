<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
	
	public function __construct(){
		parent::__construct();
		
		$this->page->use_directory();
		$this->load->model('Model_dashboard');
	}
	
	public function index(){	
		$this->page->view('templates/main_content',array(
			'grid'		=> $this->Model_dashboard->dashboardHome(),
			'events'	=> $this->Model_dashboard->get_index(),
			'jemaat'	=> $this->Model_dashboard->get_jemaat()
		));
	}
	
}

/* End of file dasbor.php */
/* Location: ./application/controllers/dasbor.php */