<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Give extends MX_Controller {
	
	public	 function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modelgive');
	}

    public function index(){
		//load view with get data
		$this->page->view('give_index', array (
			'grid'		=> $this->Modelgive->get_index(),
		));
    }

	public function save(){
		//array data from input post
		$data['nama'] 		= $this->input->post('nama_add_give');
		$data['urutan'] 	= $this->input->post('urutan_add_give');
		$data['foto'] 		= $_FILES['foto_user2']['name'];
		$data['id_user'] 	= $this->input->post('id_user');

		//save data
		$this->Modelgive->simpan($data);

		//upload image
		$config = array(
			'file_name' => $this->input->post('id_user')."-".str_replace(" ","",$_FILES['foto_user2']['name']),
			'upload_path' => "./image/give/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf|xls|xlsx",
			'overwrite' => TRUE,
		);
		$this->load->library('upload', $config);
		$this->upload->do_upload('foto_user2');

		//redirect page
		redirect($this->agent->referrer());
    }

	public function save_edit(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_give_edit');
		$data['nama'] 		= $this->input->post('nama_edit_give');
		$data['urutan'] 	= $this->input->post('urutan_edit_give');
		$data['id_user'] 	= $this->input->post('id_user');
		if($_FILES['foto_user']['name'] == '' ||  $_FILES['foto_user']['name'] == null){
			$data['foto'] 		= $this->input->post('fotoLama');
			$foto 				= $this->input->post('fotoLama');
		}else{
			$data['foto'] 		= $this->input->post('id_user')."-".str_replace(" ","",$_FILES['foto_user']['name']);
			$foto				= $this->input->post('id_user')."-".str_replace(" ","",$_FILES['foto_user']['name']);
		}

		//update data
		$this->Modelgive->simpan_edit($data);

		//upload image
		$config = array(
			'file_name' => $foto,
			'upload_path' => "./image/give/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf|xls|xlsx",
			'overwrite' => TRUE,
		);
		$this->load->library('upload', $config);
		if($_FILES['foto_user']['name'] == '' ||  $_FILES['foto_user']['name'] == null){
		}else{
			$this->upload->do_upload('foto_user');
		}

		//redirect page
		redirect($this->agent->referrer());
    }

	public function delete(){
		//array data from input post
		$data['id'] 		= $this->input->post('id_give_delete');
		$data['nama'] 		= $this->input->post('nama_give_delete');
		$data['id_user'] 	= $this->input->post('id_user');

		//delete data
		$this->Modelgive->hapus($data);

		//redirect page
		redirect($this->agent->referrer());
    }
}