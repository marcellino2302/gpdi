<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Changepass extends MX_Controller {
	
	public	 function __construct() {
		parent::__construct();
		$this->page->use_directory();
		//load model
		$this->load->model('Modelchangepass');
	}

    public function index(){
		//load view with get data
		$this->page->view('changepass_index', array(''));
    }

	public function save(){
		//array data from input post
		$data['old'] 		= $this->input->post('oldpass');
		$data['new'] 	    = $this->input->post('newpass');
		$data['confirm'] 	= $this->input->post('confirmpass');
		$data['id_user'] 	= $this->input->post('id_user');

		//save data
		$result = $this->Modelchangepass->simpan($data);

        //redirect page
		echo json_encode('0');
    }

	public function cekpassword(){
		$pass = $this->input->post('pass');
		$id_user = $this->input->post('id_user');
		
		$numrow = $this->Modelchangepass->cekdata($pass, $id_user);

		echo json_encode($numrow);
    }

}